Deployment of athena bot
========================

Sysem config
------------

**Install required software:**

* Python 3.3
* Python Virtualenv
* Python package that contains pip
* xml and xlst developement files

On recent debian systems it is done by:

.. code-block:: bash

    aptitude -t testing install python3.3 python-virtualenv \
        git virtualenvwrapper build-essential libxslt1-dev libxml2-dev

.. note::

    As of today you'll need to download **both** python3.3 and python-virtualenv
    from the testing repository.

**Create athena user**

.. code-block:: bash

    adduser athena --disabled-password

**Add proper ssh keys (athena can fetch)**

Add proper ssh keys, if you don't know what keys to add email me.

**Clone the repository**

.. code-block:: bash

    athena@dilf:~$ cd
    athena@dilf:~$ git clone git@bitbucket.org:silf/silf-backend-athena.git

**Install dependencies inside virtualenv**

.. code-block:: bash

    athena@dilf:~$ cd
    athena@dilf:~$ cd silf-backend-athena
    athena@dilf:~$ pip install -r REQUIREMENTS
    Successfully installed ...
    Cleaning up..


Sysem config
------------

Preffered metod of deployment is a supervisord,
example supervisord file is:

.. code-block:: ini

    autostart=true
    autorestart=true
    stderr_logfile=/var/log/athena.err.log
    stdout_logfile=/var/log/athena.out.log
    user=athena
    killasgroup=true
    stopasgroup=true
    stopsignal=KILL




