Athena Capabilities
===================


Capability mechanism allows to selectively enable or disable modules inside
athena. Capabilities are updated using config files see: :doc:`config_file.rst`.

There are following modules:

``reservation``

    Disabled ``reservation`` module does nothing.

    Enabled ``reservation`` module reads reservation information from SILF site
    and moderates room accordingly.

``read_experiments_from_remote_api``

    Disabled ``read_experiments_from_remote_api`` module does nothing.

    Enabled ``read_experiments_from_remote_api`` reads experiment list from SILF
    site and creates and manages experiment rooms for all experiments read
    from remote site.

``chatbot``

    Disabled ``chatbot`` module does nothing.

    Enabled ``chatbot`` module loads chatbot if chatbot is defined for this
    experiment.

``expstate``

    Disabled ``expstate`` module registeres important stanzas concerned with
    experiment flow (setting protocol version, mode and starting series)

    Enabled ``expstate`` additionaly sends state to users that join the experiment.


``create_additional_rooms``


    Disabled ``create_additional_rooms`` module does nothing.

    Enabled ``create_additional_rooms`` module creates rooms that are not
    bound to any experiment (for example General Chat) if these rooms are configured
    in config file.


``publish_results``


    Disabled ``publish_results`` module logs experiment results.

    Enabled ``publish_results`` module uses remote rest API to publish experiment results.


``monitoring``


    Disabled ``monitoring`` module logs experiment session events.

    Enabled ``monitoring`` module uses remote rest API to publish experiment session events.