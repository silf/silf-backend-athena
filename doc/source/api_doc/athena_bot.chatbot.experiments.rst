athena_bot.chatbot.experiments package
======================================

Subpackages
-----------

.. toctree::

    athena_bot.chatbot.experiments.attenuation

Submodules
----------

athena_bot.chatbot.experiments.example module
---------------------------------------------

.. automodule:: athena_bot.chatbot.experiments.example
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.chatbot.experiments.geiger module
--------------------------------------------

.. automodule:: athena_bot.chatbot.experiments.geiger
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: athena_bot.chatbot.experiments
    :members:
    :undoc-members:
    :show-inheritance:
