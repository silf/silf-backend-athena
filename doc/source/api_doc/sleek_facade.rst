sleek_facade package
====================

Submodules
----------

sleek_facade.facade module
--------------------------

.. automodule:: sleek_facade.facade
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: sleek_facade
    :members:
    :undoc-members:
    :show-inheritance:
