athena_bot.modules package
==========================

Submodules
----------

athena_bot.modules.api module
-----------------------------

.. automodule:: athena_bot.modules.api
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.capabilities module
--------------------------------------

.. automodule:: athena_bot.modules.capabilities
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.chatbot module
---------------------------------

.. automodule:: athena_bot.modules.chatbot
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.create_rooms module
--------------------------------------

.. automodule:: athena_bot.modules.create_rooms
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.experiment_state module
------------------------------------------

.. automodule:: athena_bot.modules.experiment_state
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.remote_conf module
-------------------------------------

.. automodule:: athena_bot.modules.remote_conf
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.reservation module
-------------------------------------

.. automodule:: athena_bot.modules.reservation
    :members:
    :undoc-members:
    :show-inheritance:

athena_bot.modules.result_module module
---------------------------------------

.. automodule:: athena_bot.modules.result_module
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: athena_bot.modules
    :members:
    :undoc-members:
    :show-inheritance:
