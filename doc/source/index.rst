.. Athena Bot documentation master file, created by
   sphinx-quickstart on Sat Jan 11 12:50:32 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _main-page:

Welcome to Athena Bot's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 1

   deployment_example
   config_file
   versions

Authors
-------

* Mikołaj Kubicki ``<TODO>``
* Jacek Bzdak ``jbzdak@gmail.com``


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

