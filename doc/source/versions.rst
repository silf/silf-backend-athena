Athena version tags
===================

``1.4.2``

Fix for SILF-910

``1.4.1``

Cleanup edition. Rename some methods in API. Remove unused config options.

``1.4``

Chatbot for attenuation and Black Body


``1.3``

Current production version (with modules and chatbot)

``1.2``

Previous production version (no modules, no chatbot)



