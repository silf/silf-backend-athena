Plan manualnych testów funkcjonalności Atheny
=============================================

Przygotowanie
-------------

1. Stworzenie użytkowników silf-api o nazwach:

 a. ``op``, ``op1`` --- Ci będą operatorami
 b.  ``obs``, ``obs1`` --- Ci będą obserwatorami

2. Ustawienie przykładowego eksperymentu.


Rezerwacje
----------

1. Zakladamy na przykładowy eksperyment godzinną rezerwację na użytkownika ``op``.

  a. Po wejściu użytkownika ``op`` do pokoju powinien on być operatorem
  b. ``obs`` nie powinien nim być
  c. Po restarcie Atheny ``op`` ciągle powinien być operatorem.

2. Kasujemy rezerwację godzinną, wyłączamy athenę, zakladamy 30 sekundową.

   Użytkownik ``op`` otrzyma operatora a potem go straci.

Experiment state
----------------

1. Zakładamy przykładowy eksperyment z godzinną rezerwacją na użykownika ``op``.

2. Wybieramy tryb za pomocą guzika

3.

