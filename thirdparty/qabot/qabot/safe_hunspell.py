# -*- coding: utf-8 -*-

import pathlib
import hunspell


class HunSpell(object):

    """
  Py hunspell wrapper that is safer for use.
  """

    def __init__(self, aff_file: pathlib.Path, dic_file: pathlib.Path):

        if not aff_file.suffix == ".aff":
            raise ValueError("Invalid aff file extension")
        if not dic_file.suffix == ".dic":
            raise ValueError("Invalid aff file extension")

        self.hs = hunspell.HunSpell(str(dic_file), str(aff_file))

    def __decode(self, val: bytes):
        return val.decode(self.hs.get_dic_encoding())

    def get_all_stems(self, word, safe=True):
        """

    :param safe: If True will return word in word is not in the dictionary.

    >>> dir = pathlib.Path('/usr/share/hunspell')
    >>> hs = HunSpell(dir / 'pl.dic', dir / 'pl.aff')
    Traceback (most recent call last):
    ValueError: Invalid aff file extension
    >>> hs = HunSpell(dir / 'pl.aff', dir / 'pl.dic')
    >>> hs.get_all_stems('miasta')
    ['miasta', 'miasto']
    >>> hs.get_best_stem('miasta')
    'miasta'
    >>> hs.get_best_stem("niematakiegosłwa") is None
    True
    """
        stems = [self.__decode(s) for s in self.hs.stem(word)]
        if len(stems) == 0 and safe:
            stems.append(word)

        return stems

    def get_best_stem(self, word):
        stems = self.get_all_stems(word, False)
        if len(stems) == 0:
            return None
        return stems[0]
