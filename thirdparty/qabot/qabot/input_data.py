# -*- coding: utf-8 -*-

import uuid

import os
import yaml
import pathlib
from qabot.safe_hunspell import HunSpell

DATA_DIR = pathlib.Path(__file__).parent / "data"

PATHS_FILE = DATA_DIR / "paths.yaml"
STOPWORDS_FILE = DATA_DIR / "stopwords.yaml"


class YamlSourceObject(object):

    """
  Abstract base class for all objects.
  """

    def __init__(self, hs: HunSpell, md: "FileMetadata"):
        self.hs = hs
        """
    HunSpell object (may be null)
    """
        self.md = md
        """
    :type:  FileMetadata
    """

    def parse_and_verify(self, data: dict) -> list:
        """
    :return: A list of strings containing parsing warnings.
    """
        return []

    def post_parse_verification(self) -> list:
        return []


class FileMetadata(YamlSourceObject):

    """
  Represents metadata for whole dataset.
  """

    def __init__(self, id: "QaBotData", stopwords_file: pathlib.Path):
        super().__init__(None, None)
        self.input_data = id

        self.lang = None
        """
    Language
    """
        self.allowed_operations = {}
        """
    Set of strings containing all allowed operatior
    """
        self.default_operation = None
        """
    Default operation used where operation is unspecified
    """
        self.default_pts = None
        """
    Default points for each word
    """
        self.default_min_pts = None
        """
    Default minimal points for each questions
    """
        self.stopwords_file = stopwords_file
        """
    Path containing stopwords for ``lang``
    """

    def parse_and_verify(self, data: dict) -> list:
        self.lang = data["lang"]
        with self.stopwords_file.open("r") as f:
            self.stopwords = set(yaml.load(f)[self.lang])
        self.allowed_operations = set(data["allowed_operations"])
        self.default_operation = data["default"]["operation"]
        if not self.default_operation in self.allowed_operations:
            raise ValueError("Default operations is not in allowed operations")
        self.default_pts = float(data["default"]["question_pts"])
        self.default_min_pts = float(data["default"]["min_pts"])
        return []


class WordObject(YamlSourceObject):
    def __init__(self, qest: "Question", hs: HunSpell, md: "FileMetadata"):
        super().__init__(hs, md)
        self.id = uuid.uuid4()
        self.quest = qest
        """
    :type: Question
    """
        self.words = []
        """
    List of words this word object represents
    """
        self.points = 0
        """
    Points for this word object
    """

    def parse_and_verify(self, data: dict) -> list:
        word = data["word"]
        if not isinstance(word, list):
            word = [word]
        warnings = []
        self.words = [self.__clean_word(w, warnings) for w in word]
        self.points = data.get("pts", self.md.default_pts)
        return warnings

    def __clean_word(self, word, warnings):
        stem = self.hs.get_best_stem(word)
        if word != stem or stem is None:
            warnings.append(
                "Word '{word}' in question {id} not in it's "
                "stemmed form: '{stem}'".format(word=word, id=self.quest.id, stem=stem)
            )

        if stem is None:
            stem = word

        if word in self.md.stopwords:
            warnings.append(
                "Word '{word}' in question {id} is a stopword".format(
                    word=word, id=self.quest.id, stem=stem
                )
            )
        return stem


class Operation(YamlSourceObject):
    def __init__(self, hs: HunSpell, md: "FileMetadata"):
        super().__init__(hs, md)
        self.operation_type = None
        """
    Operation name (must be in: metadata.allowed_operations)
    """
        self.operation_dict = None
        """
    Dictionaty with additional data
    """

    def parse_and_verify(self, data: object) -> list:

        if isinstance(data, str):
            self.operation_dict = {}
            self.operation_type = data
        else:
            self.operation_type = data.pop("type")
            self.operation_dict = data

        return []


class Question(YamlSourceObject):

    """
  Represents a single question
  """

    def __init__(self, id: "QaBotData", hs: HunSpell, md: "FileMetadata"):
        super().__init__(hs, md)
        self.input_data = id
        self.id = None
        """
    Question id
    """
        self.min_pts = None
        """
    Minimal number of points for this question to matched from user string
    """
        self.operation = None
        """
    Operations
    """
        self.question = None
        """
    Question string. This question is matched by word from ``self.words``
    """
        self.answer = None
        """
    Answer that will be displayed from this question
    """
        self.words = None
        """
    List of word objects.
    """
        self.related = None
        """
    List of related questions
    """
        self.wo_pts = None
        """
    Performance cache of points for each word.
    """

    def parse_and_verify(self, data: dict) -> list:

        warnings = []

        self.id = data["id"]
        question_metadata = data.get("metadata", {})
        self.min_pts = float(question_metadata.get("min-pts", self.md.default_min_pts))
        self.operation = Operation(self.hs, self.md)
        warnings.extend(
            self.operation.parse_and_verify(
                data.get("operation", self.md.default_operation)
            )
        )

        def get_and_clean(name):
            if name in data:
                try:
                    return data[name].strip()
                except Exception as e:
                    pass
                    raise
            return None

        self.question = get_and_clean("question")
        self.answer = get_and_clean("answer")

        self.words = []
        for w in data["words"]:
            wo = WordObject(self, self.hs, self.md)
            warnings.extend(wo.parse_and_verify(w))
            self.words.append(wo)

        self.related = data.get("related", tuple())

        self.wo_pts = {}
        for wo in self.words:
            self.wo_pts[wo.id] = wo.points

        return warnings

    def post_parse_verification(self) -> list:

        for r in self.related:
            if not isinstance(r, Question):
                yield "Error nonexistent related question {}".format(r)

    def __str__(self):
        return "<Question: id={}>".format(self.id)

    __repr__ = __str__


class QaBotData(object):

    """
  Whole dataset
  """

    def __init__(
        self, file: pathlib.Path, paths_file: pathlib.Path, stopwords_file: pathlib.Path
    ):
        self.file = file
        self.paths_file = paths_file
        self.stopword_file = stopwords_file
        self.hs = None
        """
    Hun spell object
    """
        self.md = None
        """
    File metadata
    """
        self.question_map = {}
        """
    Maps question id to question
    """

    @property
    def stopwords(self):
        return self.md.stopwords

    def parse(self):
        warnings = []

        with self.file.open("r") as f:
            data = yaml.load(f)

        self.md = FileMetadata(self, self.stopword_file)
        warnings.extend(self.md.parse_and_verify(data["metadata"]))

        with self.paths_file.open("r") as f:

            self.paths = yaml.load(f)["dict_paths"][self.md.lang]

        self.hs = HunSpell(
            pathlib.Path(self.paths["aff_file"]), pathlib.Path(self.paths["dic_file"])
        )

        questions = []
        for d in data["questions"]:
            q = Question(self, self.hs, self.md)
            warnings.extend(q.parse_and_verify(d))
            questions.append(q)

        self.question_map = {q.id: q for q in questions}

        for q in self.question_map.values():

            q.related = [self.question_map.get(rid, rid) for rid in q.related]

        for q in self.question_map.values():
            warnings.extend(q.post_parse_verification())

        return warnings
