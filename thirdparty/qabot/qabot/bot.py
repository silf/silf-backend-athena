# -*- coding: utf-8 -*-

import re

from collections import defaultdict, Counter
from copy import deepcopy
from qabot.input_data import Question, WordObject, QaBotData


class QaBot(object):
    """
  Implements qabot.

  Algorithm is as follows (and should work with minor tweaks for languages
  other than Polish)

  Preparatory phase:

  1. Tokenize question by space
  2. Remowe all non-word characters
  3. Lowercase whole question
  4. Stem all words
  5. Remove stopwords

  Proper phase

  * For each word in question (submitted by user):

    * Find questions that contain a word object containing this word,
      and mark this word object as 'selected'

  * For each question:

    * Assing for each question points, that is a sum of points from all selected
      word objects

  * Return all questions that got more than ``min_points`` sorted by number of points.
  """

    def __init__(self, data: QaBotData):
        self.data = data

        self.question_cleanup_re = re.compile("[^\w]", re.UNICODE)

        self.word_map = {}
        """
    Maps word string to a (question id , word object id ) tuple.
    """

    def initialize(self):
        wm = defaultdict(lambda: [])
        for q in self.data.question_map.values():
            assert isinstance(q, Question)
            for wo in q.words:
                assert isinstance(wo, WordObject)
                for w in wo.words:
                    wm[w].append((q.id, wo.id))
                    if w.lower() != w:
                        wm[w.lower()].append((q.id, wo.id))
        self.word_map = dict(wm)

    def apply_word_score(self, word: str, scoring_dict: dict):
        stems = self.data.hs.get_all_stems(word)
        for stem in stems:
            try:
                for qid, wid in self.word_map[stem]:
                    scoring_dict[qid].add(wid)
            except KeyError:
                pass

    def get_question_by_id(self, qid: str):
        q = self.data.question_map[qid]
        if q.operation.operation_type == "alias":
            return self.get_question_by_id(q.operation.operation_dict["target"])
        return q

    def clean_and_question(self, question: str):
        """
    >>> qbot = QaBot(None)
    >>> qbot.clean_and_question("Czy ala ma kota?")
    ['czy', 'ala', 'ma', 'kota']

    :param question:
    :return:
    """
        words = question.split()

        return [re.sub(self.question_cleanup_re, "", w.lower()) for w in words]

    def on_question(self, question: str) -> list:
        words = self.clean_and_question(question)
        scoring_dict = defaultdict(lambda: set())
        sw = self.data.stopwords
        for w in words:
            if w in sw:
                continue
            self.apply_word_score(w, scoring_dict)

        question_score = defaultdict(lambda: 0)

        for qid, woids in scoring_dict.items():
            q = self.data.question_map[qid]
            q.last_score = []
            for woid in woids:
                question_score[qid] += q.wo_pts[woid]

        ranking = sorted(question_score.items(), key=lambda x: x[1], reverse=True)
        ranking = [(score, self.get_question_by_id(qid)) for qid, score in ranking]
        ranking = list(filter(lambda x: x[0] >= x[1].min_pts, ranking))

        return ranking
