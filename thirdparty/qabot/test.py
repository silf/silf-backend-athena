# -*- coding: utf-8 -*-
import pathlib
from qabot.bot import QaBot
from qabot.input_data import QaBotData


id = QaBotData(
    pathlib.Path("/home/jb/programs/silf-qabot/data/input-file-example.yaml")
)


print("\n".join(id.parse()))

print(id.hs.get_best_stem("alpha"))


qabot = QaBot(id)

qabot.initialize()

# print(qabot.on_question("Czy ala ma kota"))

print(
    qabot.on_question(
        "Dlaczego promieniowanie jest osłabiane przy przejściu przez materię"
    )
)
print(qabot.on_question("Jak inaczej kwanty oddziałują z materią?"))
print(qabot.on_question("Jakie są rodzaje promieniowania?"))
print(qabot.on_question("Co to promieniowanie beta?"))
