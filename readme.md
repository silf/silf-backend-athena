Docker container requires that following enviorment variables exist: 

* `TIGASE_DOMAIN` Hostname of the XMPP server, a resolvable address to which Athena will
  connect, compare to `TIGASE_HOST`. 
* `ATHENA_TIGASE_PASSWORD` Password for the athena user on the server.
* `SILF_APP_API_URL` Root url for the `silf-app` api endpoint, example: `http://ilf.fizyka.pw.edu.pl/silf/api/`.
* `SILF_APP_ATHENA_API_TOKEN` API Token for athena to authorize to `silf-app`


Following settings have default values: 

* `TIGASE_HOST` host to which we make connection `TIGASE_DOMAIN`.
* `ATHENA_CAPABILITIES` defaults to whatever is in `docker.ini`. 

 
