import abc
# from copy import deepcopy
import json
import collections
import sleekxmpp
from sleekxmpp import Callback, MatchXMLMask
# from athena_bot.modules import AthenaCapabilities
# from athena_bot.modules.capabilities import CapabilityType
# from silf.backend.commons.util.config import open_configfile, configure_logging
from xml.etree import cElementTree as ET

from sleekxmpp.exceptions import IqError, IqTimeout
from sleekxmpp.jid import JID

from silf.backend.commons.util.config import open_configfile
from silf.backend.client.labdata import check_message_labdata, _extract_labdata
from silf.backend.client.api import CallbackStore

import warnings

import logging
LOGGER = logging.getLogger(__name__)


def jid_to_bare_str(jid):
    """
    Convert JID to bare string.
    If argument is not of JID type remove resource if present and make lower case.
    :param jid: JID or string.
    :return: Returns string which is bare part of jid, so only user@domain part
    :rtype: str
    """
    if isinstance(jid, JID):
        return jid.bare
    if isinstance(jid, str):
        jid = jid.lower()
        if '/' in jid:
            jid = jid.split('/')[0]
    return jid

LabdataMetadata = collections.namedtuple('LabdataMetadata', ['sender_jid', 'sender_nick'])


class BaseFacade(object, metaclass=abc.ABCMeta):

    def __init__(self, configData=None, configPath='config/default.ini', section='Xmpp', resource=None):

        conf = configData
        if configData is None:
            conf = open_configfile(configPath)
        conf.validate_mandatory_config_keys(section, ['botJid', 'password', 'botNick', 'host', 'port',
                                                      'mucHost', 'adminJid'])

        self.jid = self.__create_bot_jid(conf[section]['botJid'], resource)
        self.password = conf[section]['password']
        self.nick = conf[section]['botNick']
        self.host = conf[section]['host']
        self.port = conf[section]['port']
        self.mucHost = conf[section]['mucHost']
        self.adminJid = conf[section]['adminJid']

        self.rooms = {}
        self.client = None

        logging.getLogger('sleekxmpp').setLevel(logging.WARN)

    def __create_bot_jid(self, baseJid, rsc):
        if not rsc:
            return baseJid
        return baseJid.split('/')[0] + '/' + rsc

    @abc.abstractmethod
    def connect(self, block=True):
        pass

    def join_room(self, roomDesc):
        # enrich room description with additional data
        roomDesc._create_room_jid(self.mucHost)
        roomDesc.add_room_owner(self.adminJid)
        # store room description for connection
        self.rooms[roomDesc.room_jid] = roomDesc

    @abc.abstractmethod
    def send_labdata_msg_to_room(self, room_jid, labdata, nick=None):
        pass

    @abc.abstractmethod
    def send_chatmsg_to_room(self, roomjid, body, nick=None):
        pass

    @abc.abstractmethod
    def leave_room(self, roomDesc):
        pass

    @abc.abstractmethod
    def set_user_role_in_room(self, room_jid, user_jid, role, user_nick=None):
        pass

    @abc.abstractmethod
    def disconnect(self):
        pass


class XmppFacade(BaseFacade):

    def connect(self, block=True):
        self.client = sleekxmpp.ClientXMPP(self.jid, self.password)
        self.client.add_event_handler("session_start", self._start_session)
        self.client['feature_mechanisms'].unencrypted_plain = True
        self.client.use_ipv6 = False

        self.client.register_plugin('xep_0004')  # Data Forms
        self.client.register_plugin('xep_0045')  # Multi-User Chat
        # add below callback so that even messages with empty body
        # are handled by athena
        self.client.plugin['xep_0045'].xmpp.register_handler(
            Callback('MUCMessage',
                     MatchXMLMask("<message xmlns='%s' type='groupchat'></message>" % self.client.plugin['xep_0045'].xmpp.default_ns),
                     self.muc_message_callback))

        self.client.connect((self.host, self.port), reattempt=False, use_ssl=False, use_tls=False)
        self.client.use_cdata = False
        self.client.process(block=block)

    def _start_session(self, event):
        """
        Fired automatically after successful connection to XMPP server
        Join groupchat.
        """

        self.client.get_roster()
        self.client.send_presence()

        for roomDesc in self.rooms.values():
            self.client.add_event_handler("muc::{}::got_online".format(roomDesc.room_jid), self._muc_online)
            self.client.add_event_handler("muc::{}::got_offline".format(roomDesc.room_jid), self._muc_offline)
            self.client.add_event_handler("muc::{}::presence".format(roomDesc.room_jid), self._muc_presence)
            self.client.plugin['xep_0045'].joinMUC(roomDesc.room_jid, self.nick, wait=False)

    def muc_message_callback(self, msg):
        #for debug you can use: msg.keys() to see available data in msg
        #examples: msg['mucnick'] msg['from'] msg['body']
        roomJid = msg['from'].bare

        if self.nick == msg['from'].resource:
            return  # Don't launch callbacks on messages from myself

        self.rooms[roomJid].on_message(msg['body'], msg['mucnick'])

        # check for labdata is here cuz check methods are based on Message object
        # not body of msg which is passed to on_message
        if check_message_labdata(msg):
            self.rooms[roomJid]._on_labdata(_extract_labdata(msg), LabdataMetadata(msg['to'].bare, msg['mucnick']))

    def _muc_offline(self, presence):
        #check _muc_message to se how get data from presence object
        roomJid = presence['from'].bare
        self.rooms[roomJid].remove_user(presence['muc']['jid'])
        self.rooms[roomJid]._on_visitor_offline(presence['muc']['jid'], presence['muc']['nick'],
                                               presence['muc']['role'])

    def _muc_online(self, presence):
        ##check _muc_message to se how get data from presence object or check presence['muc'].keys()
        roomJid = presence['from'].bare

        codes = self._get_statu_code_from_muc_presence(presence)
        # code 110 - Inform user that presence refers to itself
        # code 201 - Inform user that a new room has been created
        if '201' in codes and '110' in codes:
            self._configure_room(self.rooms[roomJid])

        if '110' not in codes:
            self.rooms[roomJid].add_user_to_room(presence['muc']['jid'], presence['muc']['nick'])
            # fire event on new visitor
            # print(str(presence))
            self.rooms[roomJid]._on_visitor_online(presence['muc']['jid'], presence['muc']['nick'],
                                                   presence['muc']['role'])

    def _muc_presence(self, presence):
        roomJid = presence['from'].bare
        if roomJid in self.rooms:
            self.rooms[roomJid]._set_user_role(presence['muc']['nick'], presence['muc']['role'])

    def _configure_room(self, roomDesc):
        roomForm = self.client.plugin['xep_0045'].getRoomConfig(roomDesc.room_jid)
        #print('roomForm - getValues: ' + str(roomForm.getValues()))
        roomForm.setValues({'muc#roomconfig_roomdesc': roomDesc.description,
                            'muc#roomconfig_roomname': roomDesc.display_name,
                            'muc#roomconfig_moderatedroom': roomDesc.moderated,   # Whether to Make Room Moderated
                            'muc#roomconfig_changesubject': roomDesc.change_subject,   # Whether to Allow Occupants to Change Subject
                            'muc#roomconfig_persistentroom': roomDesc.persistent,
                            'muc#roomconfig_enablelogging': False,  # Whether to Enable Public Logging of Room Conversations
                            'muc#maxhistoryfetch': '0'  # Maximum Number of History Messages Returned by Room
                            })

        if self.client.plugin['xep_0045'].configureRoom(roomDesc.room_jid, roomForm):
            LOGGER.info('room %s was configured and is ready to use', roomDesc.room_jid)
            roomDesc.on_room_created()
        #TODO dodac elsa i wyjatek ze sie nie udalo pokoju skonfigurowac

        self.__add_affiliations(roomDesc.room_jid, 'owner', roomDesc.room_owners)  # affList
        self.__add_affiliations(roomDesc.room_jid, 'admin', roomDesc.room_admins)
        self.__add_affiliations(roomDesc.room_jid, 'member', roomDesc.room_members)

        LOGGER.info('Room %s was configured', roomDesc.room_jid)

    def __add_affiliations(self, roomJid, aff, jidList):
        for j in jidList:
            if not self.client.plugin['xep_0045'].setAffiliation(roomJid, j, affiliation=aff):
                LOGGER.warn('Did not set %s as %s for room: %s', j, aff, roomJid)
            else:
                LOGGER.info('Set %s as %s for room: %s', j, aff, roomJid)

    def _get_statu_code_from_muc_presence(self, presence):
        statusList = presence.findall('{http://jabber.org/protocol/muc#user}x/{http://jabber.org/protocol/muc#user}status')
        #print('_muc_online - xpath: ' + str(statusList))
        return {node.get('code') for node in statusList}

    def send_labdata_msg_to_room(self, room_jid, labdata, nick=None):
        self.__send_msg_to_room_impl(
            room_jid, labdata=labdata, nick=nick
        )

    def send_chatmsg_to_room(self, roomjid, body, nick=None):
        self.__send_msg_to_room_impl(
            roomjid, body=body, nick=nick
        )

    def __send_msg_to_room_impl(self, room_jid, *, labdata=None, body=None, nick=None):

        if nick is None:
            mtype = 'groupchat'
            to_jid = room_jid
        else:
            mtype = 'chat'  # private chat message to specific user
            to_jid = JID(room_jid)
            to_jid.resource = nick

        msg = self.client.make_message(
            mto=to_jid, mtype=mtype,
            mfrom=self.jid,
            mnick=self.nick
        )

        if labdata is not None:
            if body is None:
                suite_dump = ""
                if labdata.suite is not None:
                    suite_dump = json.dumps(labdata.suite.__getstate__())
                body = "{}{}".format(labdata.namespace, suite_dump)
            labdata.insert_into_message(msg)

        if body is not None:
            msg['body'] = body

        msg.send()

    #TODO wszystkie 4 metody ponizej sa do wywalenia
    #TODO przejzyj mock-i i testFacade z testowych modulow i tez usun z tamtad te metody jesli sa nadpisane
    def send_msg_to_room(self, room_jid, labdata):
        warnings.warn("Function `send_msg_to_room` is deprecated", DeprecationWarning)
        self.__send_msg_to_room_impl(room_jid, labdata=labdata)

    def send_private_msg_to_room(self, room_jid, nick, labdata):
        warnings.warn("Function `send_private_msg_to_room` is deprecated", DeprecationWarning)
        self.__send_msg_to_room_impl(room_jid, labdata=labdata, nick=nick)

    def leave_room(self, roomDesc):
        self.client.plugin['xep_0045'].leaveMUC(roomDesc.room_jid, self.nick)

        LOGGER.info('Leaving room %s', roomDesc.room_jid)
        self.client.remove_handler("muc::{}::got_online".format(roomDesc.room_jid))
        self.client.remove_handler("muc::{}::got_offline".format(roomDesc.room_jid))

    def disconnect(self):
        """
        Terminate connection to XMPP server.
        """
        #TODO opuszczenie wszystkich pokoi do ktorych jestesmy dolaczeni, ?zamkniecie eksperymentow?
        LOGGER.info('Disconecting from server')
        self.client.disconnect(reconnect=False, wait=True)

    def set_role(self, room, role, nick, ifrom=None):
        """
        # TODO: Maybe this should be private?
        :param sleekxmpp.jid.JID room: JID object for room adress
        :param str role: new role of the user
        :param str nick: nick of the user to change his role
        :param ifrom:
        :return:
        """
        if role not in ('visitor', 'participant', 'moderator', 'none') or not nick:
            raise TypeError
        query = ET.Element('{http://jabber.org/protocol/muc#admin}query')
        item = ET.Element('item', {'role': role, 'nick': nick})
        query.append(item)
        iq = self.client.plugin['xep_0045'].xmpp.makeIqSet(query)
        iq['to'] = room
        iq['from'] = ifrom
        # For now, swallow errors to preserve existing API
        try:
            result = iq.send()
        except IqError:
            LOGGER.error('Error while setting role {} for {}'.format(role, nick))
            return False
        except IqTimeout:
            return False
        return True

    def set_user_role_in_room(self, room_jid, user_jid, role, user_nick=None):
        """ Set user as experiment operator.

        # TODO: Nie wiem czy tego nie przenieść do Rezerwacji? oddzielnego modułu
        # TODO: Nie wiem czy tego nie przenieść do pokoju...

        :param sleekxmpp.jid.JID user_jid: JID object
        :param str user_nick: nick of the user. Can be None
        :return:
        """

        if not user_nick:
            room_desc = self.rooms[room_jid]
            user_nick = room_desc.get_user_nick(user_jid)
        # user_nick is None this means that user is nt currently present in the room
        if user_nick:
            LOGGER.debug('set {} for: {}'.format(role, user_nick))
            self.set_role(room_jid, role, user_nick)


class RoomDescription(object):

    def __init__(self, roomConfig):
        self._name = roomConfig['roomName']
        self.display_name = roomConfig.get('roomDisplayName', fallback=self._name)
        self.description = roomConfig.get('roomDescription', fallback=self._name)
        self.moderated = roomConfig.getboolean('isRoomModerated', fallback=False)
        self.persistent = roomConfig.getboolean('isRoomPersistent', fallback=False)
        self.change_subject = roomConfig.getboolean('isRoomSubjectChangeable', fallback=False)
        # all of below lists are string lists of jid values
        jidList = roomConfig.get('roomAdminList', fallback='')
        self.room_admins = jidList.split(',') if jidList else []
        jidList = roomConfig.get('roomOwnerList', fallback='')
        self.room_owners = jidList.split(',') if jidList else []
        jidList = roomConfig.get('roomMemberList', fallback='')
        self.room_members = jidList.split(',') if jidList else []

        self._room_jid = None
        # map of JID objects to corresponding nicks
        self._user_jid_2_nick = {}
        self._user_nick_to_jid = {}
        self._user_nick_to_role = {}
        self.labdata_callbacks = CallbackStore()
        self.visitor_online_callbacks = []
        self.visitor_offline_callbacks = []
        self.message_listeners = []

    @property
    def name(self):
        return self._name

    @property
    def room_jid(self):
        """
        Returns JID object.
        :return: Return JID object
        :rtype: sleekxmpp.jid.JID
        """
        return self._room_jid

    def add_user_to_room(self, user_jid, user_nick):
        """
        Add user info to room.
        :param sleekxmpp.jid.JID user_jid: jid of user joining the room
        :param str user_nick: nick of user in the room
        :return:
        """
        LOGGER.debug("New user in room: {}".format(user_jid.full))
        self._user_jid_2_nick[user_jid] = user_nick
        self._user_nick_to_jid[user_nick] = user_jid

    def _set_user_role(self, user_nick, user_role):
        print('_set_user_role in {} {}={}'.format(self.room_jid, user_nick, user_role))
        self._user_nick_to_role[user_nick] = user_role

    def add_message_listener(self, listener):
        """
        Adds message launched for every
        :param func listener:

            def func(user_jid, msg_body, user_nick);
                pass

        """
        self.message_listeners.append(listener)

    def remove_user(self, user_jid):
        """

        :param sleekxmpp.jid.JID user_jid: jid of user leaving the room
        :return:
        """
        user_nick = self._user_jid_2_nick.pop(user_jid, None)
        if user_nick is not None:
            self._user_nick_to_jid.pop(user_nick, None)
            self._user_nick_to_role.pop(user_nick, None)

    def add_room_owner(self, ownerJid):
        """
        owner is Affiliation type, maps to role moderator
        """
        self.room_owners.append(jid_to_bare_str(ownerJid))

    def add_room_admin(self, adminJid):
        """
        admin is Affiliation type, maps to role moderator
        """
        self.room_admins.append(jid_to_bare_str(adminJid))

    def _create_room_jid(self, mucHost):
        self._room_jid = JID(self.name + '@' + mucHost)

    def on_room_created(self):
        pass

    def _on_visitor_online(self, user_jid, user_nick, room_role):
        #print('nowy uzytkownik: ' + str(presence['muc']['jid']) + " nick: " + str(presence['muc']['nick']))
        # print('nowy uzytkownik: ' + str(user_jid) + " nick: " + str(user_nick) + " role: " + room_role)
        for c in self.visitor_online_callbacks:
            c(user_jid, user_nick, room_role)

    def register_visitor_online_listener(self, callback):
        """

        :param callback:

            def cb(user_jid, user_nick, room_role):
                pass
        :return:
        """
        self.visitor_online_callbacks.append(callback)

    def register_visitor_offline_listener(self, callback):
        """
        Register callback for user offline event. User can be of any type: visitor, operator, experiment, ..
        :param callback:

            def cb(user_jid, user_nick, room_role):
                pass
        :return:
        """
        self.visitor_offline_callbacks.append(callback)

    def _on_visitor_offline(self, user_jid, user_nick, room_role):
        # print('offline uzytkownik: ' + str(user_jid) + " nick: " + str(user_nick) + " role: " + room_role)
        for c in self.visitor_offline_callbacks:
            c(user_jid, user_nick, room_role)

    def on_message(self, msg_body, author_nick):
        # LOGGER.debug('on_message from {} body: {}'.format(author_nick, msg_body))

        for c in self.message_listeners:
            c(self._user_nick_to_jid.get(author_nick, None), msg_body, author_nick)

    def _on_labdata(self, labdata, labdata_metadata):
        """
        Event occures when labdata was present in recived message. Labdata is extracted and passed as arg in here
        :param labdata: Type is labdata as defined in silf.backend.client.labdata
        :return: None
        """
        LOGGER.debug('on_labdata type: {}  ns: {}'.format(labdata.type, labdata.namespace))
        self.labdata_callbacks.fire_callback(labdata, labdata_metadata)

    def get_user_nick(self, user_jid):
        """

        :param sleekxmpp.jid.JID user_jid: JID object, but can also be string representation of jid
        :return: None or corresponding nick for the user in room
        :rtype: str
        """
        return self._user_jid_2_nick.get(user_jid, None)

    def get_user_jid(self, user_nick):
        return self._user_nick_to_jid.get(user_nick, None)

    def register_labdata_listener(self, callback, namespace=None, type=None):
        """

        :param callback: Expected callback signature is: callback(labdata, labdata_metadata)
        :param namespace:
        :param type:
        :return:
        """
        self.labdata_callbacks.add_callback(callback, namespace=namespace, type=type)

    def can_write_in_room(self, user_nick):
        """
        Returns true if user with given nick has rights to write in room
        :param user_nick:
        :return:
        """
        return user_nick in self._user_nick_to_role and self._user_nick_to_role[user_nick] != 'visitor'

    def can_write_in_room_by_jid(self, user_jid):
        """
        Returns true if user with given JID has rights to write in room
        :param user_jid:
        :return:
        """
        return user_jid in self._user_jid_2_nick and self.can_write_in_room(self._user_jid_2_nick[user_jid])