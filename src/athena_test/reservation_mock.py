# codeing=UTF-8

import datetime
from athena_bot.modules.reservation import BaseEnabledReservationRepository, SilfReservation, utcnow
from athena_bot.modules.capabilities import AthenaCapabilities, CapabilityType


@AthenaCapabilities.register_capability("reservation", CapabilityType.Enabled)
class ReservationRepoStub(BaseEnabledReservationRepository):

    def _update_reservation_list(self):
        """
        Create 2 nearby fake reservations for u_jid
        :return:
        """
        if self._reservation_list is None or len(self._reservation_list) == 0:
            self._reservation_list = []
            u_jid = 'new@khirgal'
            nn = utcnow()
            length = 5
            self._reservation_list.append(self._create_reservation(nn + datetime.timedelta(minutes=1), length, u_jid))
            self._reservation_list.append(self._create_reservation(nn + datetime.timedelta(minutes=(length + 2)), length, u_jid))
        else:
            self._reservation_list = [x for x in self._reservation_list if not x.is_finished()]

        print("after update, len={}".format(len(self._reservation_list)))

    def _create_reservation(self, start_time, duration, u_jid):
        return SilfReservation(start_time, start_time + datetime.timedelta(minutes=duration), u_jid)
