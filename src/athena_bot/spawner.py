from athena_bot.athena import Athena
from athena_bot.modules import AthenaCapabilities
from silf.backend.commons.util.config import open_configfile, configure_logging
from multiprocessing import Process, Event

# If you uncomment next line we will launch all athenas in a single process,
# as a separate threads --- which might slow things down.
# During developement it makes debugging easier --- as you don't deal
# with multiprocessing.
#TODO if there is single experiment room then do not create separate process - use this one
# from multiprocessing.dummy import Process

import os
import time
import atexit


class AthenaSpawner(object):
    """
    Object that spawns multiple copies of athena server each in own process,
    it also kills them all when this process ends.
    """

    def __init__(self, config_path='config/default.ini'):
        self.conf = open_configfile(config_path)
        self.conf_paths = [config_path]
        self.bot_list = []
        self.exit_event = Event()
        self.section = ""

        self.capabilities = []
        self.watched_experimets = set()
        self.remote_config = None
        self.create_additional_rooms = None
        atexit.register(self.on_exit)

    def spawn(self, section='Athena'):

        self.capabilities = AthenaCapabilities.from_config(self.conf)

        self.section = section

        self.watched_experimets = set()

        section = self.conf[section]['experimentList'].strip()

        # Explicitly allow empty list
        if len(section) > 0:
            self.watched_experimets.update({x.strip() for x in section.split(',')})

        remote_config_module = self.capabilities.load_capability_type("read_experiments_from_remote_api")

        self.remote_config = remote_config_module(self)
        conf = self.remote_config.generate_conf()
        self.watched_experimets.update(conf['experiments'])
        self.conf_paths.extend(conf['paths'])

        create_additional_rooms_module = self.capabilities.load_capability_type("create_additional_rooms")

        self.create_additional_rooms = create_additional_rooms_module(self)
        self.create_additional_rooms.create_additional_rooms()

        for e in sorted(self.watched_experimets):
            self._fork_child(e)

        try:
            self.recreate_child_loop()
        except KeyboardInterrupt:
            # catch this exception so that no stack trace will be printed to console
            print('KeyboardInterrupt received - closing')
        # below call is not needed since method on_exit is registered and will be called
        #self.kill_all_children()

    def _fork_child(self, experiment_name):
        print('Creating process for experiment: {}'.format(experiment_name))
        p = Process(name=experiment_name, target=self.run_subprocess, args=(experiment_name, ))
        p.start()
        self.bot_list.append(p)

    def recreate_child_loop(self):
        while 1:
            time.sleep(10)
            for p in list(self.bot_list):
                if not p.is_alive:
                    self.bot_list.remove(p)
                    self._fork_child(p.name)

    def kill_all_children(self):
        print('Closing child process')
        self.exit_event.set()
        # wait for children to finish
        time.sleep(5)
        # kill all bad children which are not finished yet
        for p in self.bot_list:
            if p.is_alive:
                print('terminate: {}'.format(p.pid))
                p.terminate()

    def run_subprocess(self, exp_name):
        #a = AthenaMockProcess(expName, configPath)
        print('running subProcess for: {}'.format(exp_name))
        a = Athena(exp_name, self.conf_paths, self.exit_event, self.capabilities)
        a.run()

    def on_exit(self):
        print('spawner - on exit')
        self.kill_all_children()


class AthenaMockProcess(object):

    def __init__(self, experiment, config_path='config/default.ini'):
        self.conf = open_configfile(config_path)
        self.experiment = experiment

    def run(self):
        print('Running {} pid={}'.format(self.experiment, os.getpid()))
        time.sleep(2)
        print('for exp {}'.format(self.conf[self.experiment]['expServerJid']))
