__author__ = 'silf'


class AthenaUrlconf(object):

    def __init__(self, root_url):
        self._root_url = root_url
        self._reservation_url = root_url + 'reservation/experiment/{codename}/recent.json'
        self._monitoring_url = root_url + 'monitoring/create/experiment/'
        self._session_url = root_url + 'experiment_results/session'
        self._series_url = root_url + 'experiment_results/series'
        self._experiment_list_url = root_url + 'experiments'


    def reservation_url_for(self, experiment):
        return self._reservation_url.format(codename=experiment)

    def monitoring_url(self, experiment):
        return self._monitoring_url + experiment

    @property
    def experiment_list_url(self):
        return self._experiment_list_url

    def exp_session_url(self, data_series=None):
        if data_series is None:
            return self._session_url
        return self._session_url + "/" + str(data_series)

    def data_series_url(self, data_series=None):
        if data_series is None:
            return self._series_url
        return self._series_url + "/" + str(data_series)