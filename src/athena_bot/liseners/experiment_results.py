from collections import OrderedDict

from athena_bot.aggregator import aggregate_results
import json
from abc import ABCMeta, abstractmethod

from athena_bot.modules.capabilities import CapabilityType, AthenaCapabilities
from athena_bot.liseners import base

import logging
LOGGER = logging.getLogger(__name__)


class ExperimentResults(base.ExperimentListenerBase):

    def __init__(self, athena):
        self.online_users = set()
        self.athena = athena
        self.clear_experiment()
        self.data = {}
        self.session_users = set()
        self._register_to_athena_room_desc(athena.roomDesc)
        LOGGER.info('Experiment results created')

    def _register_to_athena_room_desc(self, room_desc):
        """
        :param room_desc:
        :type room_desc: sleek_facade.facade.RoomDescription
        """
        room_desc.register_labdata_listener(self.start_session, type="result", namespace="silf:mode:set")
        room_desc.register_labdata_listener(self.end_experiment, type="result", namespace="silf:experiment:stop")
        room_desc.register_labdata_listener(self.start_series, type="result", namespace="silf:series:start")
        # user stops series
        room_desc.register_labdata_listener(self.end_series, type="result", namespace="silf:series:stop")
        # experiment stops series
        room_desc.register_labdata_listener(self.end_series, type="data", namespace="silf:series:stop")
        room_desc.register_labdata_listener(self.get_results, type="data", namespace="silf:results")
        room_desc.register_visitor_online_listener(self.add_user)
        room_desc.register_visitor_offline_listener(self.remove_user)

    @abstractmethod
    def _process_start_session_data(self, data):
        """

        :param data: data for start_session event
        :return:
        """

    @abstractmethod
    def _process_end_experiment_data(self, data):
        """

        :param data: data for end_experiment event
        :return:
        """

    @abstractmethod
    def _process_end_series_data(self, data):
        """

        :param data: data for end_series event
        :return:
        """

    def clear_series(self):
        super().clear_series()
        self.data = {}

    def start_session(self, item, metadata):
        self.clear_experiment()
        self._experiment_started(item.content.experimentId)
        self.session_users.update(self.online_users)
        # TODO: Rename name to experiment_name
        data = {
            'experiment_session_id': self.current_experiment_uuid,
            'experiment': self.athena.roomDesc.name,
            'participants': [str(u) for u in self.session_users],
            'name': self.athena.expName
        }
        self._process_start_session_data(data)

    def end_experiment(self, item, metadata):
        if self.series_is_running:
            self.end_series(item, metadata)
        data = {
            'participants': [str(u) for u in self.session_users],
            'experiment_session_id': self.current_experiment_uuid,
        }
        self._process_end_experiment_data(data)
        self.clear_experiment()

    def start_series(self, item, metadata):
        #TODO: Maybe extract this to supertype
        self._series_started(
            item.content.seriesId,
            item.content.label
        )
        # raise ValueError(item.content.label)

    def get_results(self, item, metadata):
        content = item.content.__getstate__()
        if self.series_is_running:
            self.data = aggregate_results(self.data, content)

    def end_series(self, item, metadata):
        # TODO: rename name to current_series_name
        data = {
            'series_id': self.current_series_uuid,
            'experiment_session_id': self.current_experiment_uuid,
            'name': self.current_series_label,
            'data': json.dumps(self.data)
        }
        self._process_end_series_data(data)
        self.clear_series()

    def add_user(self, user_jid, user_nick, room_role):
        self.online_users.add(user_jid)
        if self.experiment_is_running and user_jid not in self.session_users:
            self.session_users.add(user_jid)

    def remove_user(self, user_jid, user_nick, room_role):
        if user_jid in self.online_users:
            self.online_users.remove(user_jid)


@AthenaCapabilities.register_capability("publish_results", CapabilityType.Disabled)
class LogExperimentResults(ExperimentResults):

    def _process_start_session_data(self, data):
        self.__log_data('start session', data)

    def _process_end_experiment_data(self, data):
        self.__log_data('end experiment', data)

    def _process_end_series_data(self, data):
        self.__log_data('end series', data)

    def __log_data(self, event, data):
        LOGGER.debug("exp result event: {} data: {}".format(event, data))


@AthenaCapabilities.register_capability("publish_results", CapabilityType.Enabled)
class RemoteExperimentResults(ExperimentResults):

    def __log_response(self, function_name, data, response):
        LOGGER.debug("{} {}".format(
            function_name,
            json.dumps(OrderedDict((
                ('data', data),
                ('response_status', response.status_code),
                ('response_text', response.text),
            )))
        ))

    def __init__(self, athena):
        super().__init__(athena)
        self.urlconf = athena.urlconf

    def _process_start_session_data(self, data):
        headers = {'Connection': 'close'}
        url = self.urlconf.exp_session_url()
        rsp = self.athena.create_authorized_http_session().post(
            url, data=data, headers=headers)

        self.__log_response("start_session", data, rsp)

    def _process_end_experiment_data(self, data):
        headers = {'Connection': 'close'}
        url = self.urlconf.exp_session_url(data['experiment_session_id'])

        rsp = self.athena.create_authorized_http_session().patch(
            url, data=data, headers=headers)

        self.__log_response("end_experiment", data, rsp)

    def _process_end_series_data(self, data):
        headers = {'Connection': 'close'}
        url = self.urlconf.data_series_url()

        rsp = self.athena.create_authorized_http_session().post(
            url, data=data, headers=headers)

        self.__log_response("end_series", data, rsp)
