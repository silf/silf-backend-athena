import datetime

from athena_bot.liseners import base
from athena_bot.modules.capabilities import CapabilityType, AthenaCapabilities

import logging
LOGGER = logging.getLogger(__name__)

EVENT_TYPES = {
    'silf:mode:set': 'exp_start',
    'silf:experiment:stop': 'exp_end',
    'silf:series:start': 'series_start',
    'silf:series:stop': 'series_end'
}


@AthenaCapabilities.register_capability("monitoring", CapabilityType.Disabled)
class Monitoring(base.ExperimentListenerBase):
    # TODO: Adding reasons for end of reservation and timeout.

    def __init__(self, athena):
        self.athena = athena
        self.clear_experiment()
        self._register_to_athena_room_desc(athena.exp_room)
        LOGGER.info("Monitoring started")

    def now(self):
        """
        Created for test purpose, as it was hard to mock datetime.utcnow
        TODO: should be removed
        :return:
        """
        return datetime.datetime.utcnow()

    def on_mode_set(self, item, metadata):

        self._experiment_started(item.content.experimentId)

        self.post_event("exp_start")

    def on_series_start(self, item, metadata):
        self._series_started(item.content.seriesId)

        self.post_event('series_start')

    def on_series_end(self, item, metadata):
        reason = "user_ended"           # "Ended by user"
        if metadata.sender_jid == self.athena.roomDesc.expJid:
            reason = "exp_ended"        # "Ended by experiment"

        self.post_event('series_end', additional_data=reason)
        self.clear_series()

    def on_experiment_stop(self, item, metadata):
        # TODO:Nie wiem czy ten if ma sens.
        if not self.experiment_is_running:
            # If this is false it means that either athena was just turned on
            # (and we don't know the state) or (more probably) self.experiment_is_running
            # was set to False by Experiment exiting the room in that case
            # we got notified.
            return

        if metadata.sender_jid == self.athena.roomDesc.expJid:
            reason = "sess_ended_exp"       # "Experiment closed session"
        elif metadata.sender_jid == self.athena.athena_jid:
            reason = "sess_ended_ath"       # "Athena closed session"
        else:
            reason = "sess_ended_usr"       # "User closed session"

        self.clear_series()
        self.post_event("exp_end", reason)
        self.clear_experiment()

    def add_in_event(self, user_jid, user_nick, room_role):
        """
        Executes for online presence.
        """
        if user_jid.bare == self.athena.roomDesc.expJid:
            self.post_event("server_in")

    def add_out_event(self, user_jid, user_nick, room_role):
        """
        Executes for offline presence.
        """
        if user_jid.bare == self.athena.roomDesc.expJid:
            if self.series_is_running:
                self.post_event("series_end", "experiment_disconnected")
                self.clear_series()
            if self.experiment_is_running:
                self.post_event("exp_end", "experiment_disconnected")
            self.post_event("server_out")
            self.clear_experiment()

    def post_event(self, event_type, additional_data=None):
        """
        Log given event. This method should be overwritten to achieve different ways
        of storing information about happening events.
        """
        LOGGER.info("post event: {}".format(event_type))

    def _register_to_athena_room_desc(self, room_desc):
        """
        :param room_desc:
        :type room_desc: sleek_facade.facade.RoomDescription
        """
        room_desc.register_labdata_listener(self.on_mode_set, type="result", namespace="silf:mode:set")
        room_desc.register_labdata_listener(self.on_experiment_stop, type="result", namespace="silf:experiment:stop")
        room_desc.register_labdata_listener(self.on_series_start, type="result", namespace="silf:series:start")
        # when user ends series
        room_desc.register_labdata_listener(self.on_series_end, type="result", namespace="silf:series:stop")
        # when experiment ends series
        room_desc.register_labdata_listener(self.on_series_end, type="data", namespace="silf:series:stop")
        room_desc.register_visitor_offline_listener(self.add_out_event)
        room_desc.register_visitor_online_listener(self.add_in_event)
        # room_desc.register_labdata_listener(self.set_reason, type="query", namespace="silf:experiment:stop")
        # room_desc.register_labdata_listener(self.set_reason, type="query", namespace="silf:series:stop")


@AthenaCapabilities.register_capability("monitoring", CapabilityType.Enabled)
class RemoteMonitoring(Monitoring):

    def __init__(self, athena):
        super().__init__(athena)
        self.urlconf = self.athena.urlconf

    def post_event(self, event_type, additional_data=None):
        """
        Posts appropriate monitoring event to silf-app API REST.
        """
        url = self.urlconf.monitoring_url(self.athena.roomDesc.experiment_codename)
        data = {
            #TODO: Move this to settings
            "date": self.now().strftime('%Y-%m-%dT%H:%M:%S%z'),
            "event_type": event_type,
            "additional_data": additional_data,
            "session_uuid": self.current_experiment_uuid,
            "series_uuid": self.current_series_uuid
        }
        # print(data)
        headers = {'Connection': 'close'}
        self.athena.create_authorized_http_session().post(url, data=data, headers=headers)
