# -*- coding: utf-8 -*-


class ExperimentListenerBase(object):

    @property
    def current_series_uuid(self):
        return self._current_series_id

    @property
    def current_experiment_uuid(self):
        return self._current_experiment_id

    @property
    def current_series_label(self):
        return self._series_label

    def clear_series(self):
        self.series_is_running = False
        self._current_series_id = None
        self._series_label = None

    def clear_experiment(self):
        self.clear_series()
        self.experiment_is_running = False
        self._current_experiment_id = None

    def _series_started(self, id, label=None):
        self.series_is_running = True
        self._current_series_id = id
        self._series_label = label

    def _experiment_started(self, id):
        self.experiment_is_running = True
        self._current_experiment_id = id
