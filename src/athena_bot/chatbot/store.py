from athena_bot.chatbot.experiments.attenuation import AttenuationBot
from athena_bot.chatbot.experiments.blackbody.bot import BlackbodyBot
from athena_bot.chatbot.experiments.centrifugal.api import CFBot
from athena_bot.chatbot.experiments.diode.bot import DiodeBot
from athena_bot.chatbot.experiments.doppler.api import DopplerBot
from athena_bot.chatbot.experiments.fotoefekt.bot import PhotoelectricBot
from athena_bot.chatbot.experiments.geiger import GeigerBot
from athena_bot.chatbot.experiments.example import ExampleBot
from athena_bot.chatbot.experiments.hall.bot import HallBot
from athena_bot.chatbot.experiments.maxwell.bot import MaxweBot
from athena_bot.chatbot.experiments.michelson.bot import MichelsonBot
from athena_bot.chatbot.experiments.ohm.bot import OhmBot
from athena_bot.chatbot.experiments.snellius.bot import SnelliusBot


class ChatBotStore(dict):

    def __init__(self):
        super().__init__()
        self['example'] = ExampleBot
        self['geiger'] = GeigerBot
        self['attenuation'] = AttenuationBot
        self['blackbody'] = BlackbodyBot
        self['diode'] = DiodeBot
        self['michelson'] = MichelsonBot
        self['centrifugal-force'] = CFBot
        self['doppler'] = DopplerBot
        self['ohm'] = OhmBot
        self['photoelectric'] = PhotoelectricBot
        self['snellius'] = SnelliusBot
        self['maxwell'] = MaxweBot
        self['hall'] = HallBot
        self['snellius'] = SnelliusBot