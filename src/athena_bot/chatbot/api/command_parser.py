# -*- coding: utf-8 -*-

import abc
import re
from itertools import chain

from .jinjaenv import ENVIORMENT


class CommandParser(object, metaclass=abc.ABCMeta):
    """
    Parser that parses a single command.
    """

    REQUIRES_OP = True
    """
    If True this command can be executed by the operator.
    """

    def __init__(self, chat_bot, bot_state=None):
        self.chat_bot = chat_bot
        self.bot_state = bot_state
        """
        Optionally bot_state may be attached to the comamnd parser.
        """

    def send_chat_message(self, message_text: str):
        """
        Sends message to room.
        """
        self.chat_bot.send_msg_to_room(message_text)

    @abc.abstractmethod
    def matches_command(self, command_text: str, bot_name: str) -> bool:
        """

        Checks if this command matches user input.

        :param str command_text: Command text without bot name. If user
                                 entered: "\czesio something else" only
                                 "something else" will be passed here
        :param str bot_name: Name of the bot. If user
                                 entered: "\czesio something else" only
                                 "czesio" will be passed here

        :return: True if this `command_match` matches this command.
        """
        pass

    @abc.abstractmethod
    def get_command_info(self) -> str:
        """
        :return: Returns a short human-readable description of commands
                 userd by this bot.
        """
        pass

    @abc.abstractmethod
    def execute_command(
            self, command_text: str, bot_name: str, user_nick: str,
            user_is_op: bool):
        """

        :param str command_text: Command text without bot name. If user
                                 entered: "\czesio something else" only
                                 "something else" will be passed here
        :param str bot_name: Name of the bot. If user
                                 entered: "\czesio something else" only
                                 "czesio" will be passed here
        :param str user_nick: User nickname in the room
        :param bool user_is_op: True if user is a room operator.

        :return: New state for parser or None if there is no need to change state
        """
        pass



class SimpleCommandParser(CommandParser):
    COMMAND = None
    COMMAND_DESCRIPTION = ""

    def __init__(self, chat_bot):
        super().__init__(chat_bot)

    def command(self):
        if isinstance(self.COMMAND, (list, tuple)):
            return " ".join(self.COMMAND)
        return self.COMMAND

    def command_as_regex(self):
        command = self.COMMAND
        if isinstance(self.COMMAND, str):
            command = [command]
        return re.compile(".*{}.*".format("\s+".join(command)))

    def get_description(self):
        try:
            return self.chat_bot.get_value_from_kvstore(self.COMMAND_DESCRIPTION)
        except (KeyError, ValueError) as e:
            return self.COMMAND_DESCRIPTION

    def get_command_info(self):
        tmpl = ENVIORMENT.get_template('command_description.md')
        return tmpl.render(
            bot_name=self.chat_bot.BOT_NAME,
            command=self.command(),
            description=self.get_description()
        )

    def matches_command(self, command_str, bot_name):
        return re.match(self.command_as_regex(), command_str) is not None


class StartSeriesCommand(SimpleCommandParser):

    """
    This is an abstract class, user should override SERIES_STATE attribute.
    """

    SERIES_STATE = None

    def execute_command(self, *args, **kwargs):
        return self.SERIES_STATE


class StopSeriesCommand(SimpleCommandParser):

    COMMAND = ['seria', 'stop']

    COMMAND_DESCRIPTION = "base.commands.stop_series.description"

    def execute_command(self, command_text, bot_name, user_nick, user_is_op):
        if not self.chat_bot.is_executing_series():
            self.chat_bot.send_private_msg_to_room(
                user_nick,
                "command.stop_series.series_not_started"
            )
            return
        return self.chat_bot.IDLE_STATE

class ListQuestions(SimpleCommandParser):

    COMMAND =["lista", "pytań"]
    REQUIRES_OP = False
    COMMAND_DESCRIPTION = """base.commands.list_all_questions.description""".strip()

    def get_response(self) -> str:

        question_list = list(self.chat_bot.qabot.data.question_map.values())
        question_list.sort(key=lambda q: q.question)

        tmpl = ENVIORMENT.get_template('question_list.md')
        return tmpl.render(
            questions=question_list
        )

    def execute_command(self, command_text: str, bot_name: str, user_nick: str,
                        user_is_op: bool):

        self.chat_bot.send_msg_to_room(
            self.get_response()
        )

class ListAllCommandsCommand(SimpleCommandParser):

    """
    This command lists avilable commands and sends them to users.

    If user is an ``operator`` he gets all commands if user is a normal user
    he only gets commands he can execute.
    """

    COMMAND = "info"
    REQUIRES_OP = False

    COMMAND_DESCRIPTION = """base.commands.list_all_commands.description""".strip()

    def execute_command(self, command_text, bot_name, user_nick, user_is_op):

        commands = [
            p.get_command_info()
            for p in chain(self.chat_bot.state.parsers, self.chat_bot.parsers)
            if not isinstance(p, FallbackHandler)
        ]

        tmpl = ENVIORMENT.get_template('list_commands_response.md')

        rendered = tmpl.render(
            commands=commands,
            user_is_op=user_is_op
        )

        self.chat_bot.send_private_msg_to_room(user_nick, rendered)


class FallbackHandler(CommandParser):

    REQUIRES_OP = False

    def matches_command(self, *args, **kwargs):
        return True

    def get_command_info(self):
        return None

    def execute_command(self, command_text, bot_name, user_nick, user_is_op):
        self.chat_bot.send_private_msg_to_room(user_nick, "Nieznane polecenie {}".format(command_text))
