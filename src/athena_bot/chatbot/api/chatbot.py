# -*- coding: utf-8 -*-

import abc
from itertools import chain
import pathlib
import logging
import re
import uuid
import itertools

from athena_bot.chatbot.api.command_parser import FallbackHandler, ListQuestions
from athena_bot.chatbot.api.command_parser import ListAllCommandsCommand
from silf.backend.client.labdata import LabdataBase, format_labdata
import yaml

from athena_bot.chatbot.api.state import BotIdle, BotState
from qabot.bot import QaBot
from qabot.input_data import Question, QaBotData
from silf.backend.commons.api import SeriesStartedStanza, SettingSuite, Setting, \
    ResultSuite, SeriesStopped
from silf.backend.commons.api.stanza_content._misc import ModeSelected

from sleek_facade.facade import RoomDescription

from .jinjaenv import ENVIORMENT

LOGGER = logging.getLogger("chatbot")

# Points to /athena_bot/chatbot/data
DATA_PATH = pathlib.Path(__file__).parents[2] / 'chatbot' / 'data'

DESCRIPTION_KEY_RE = "^[\w\d\.]+$"

class ChatBot(object, metaclass=abc.ABCMeta):
    """
    This implements a chatbot.
    To implement it you need to:
    override ``INITIAL_STATE`` and ``COMMAND_CLASSES`` chatbot.

    Optionally you might override:

    * ``KV_STORE_PATH``
    * ``QABOT_DATA_PATH``

    It works as following:

    1. Wherever we reclieve a chat message it is passed through
       instances of :class:`CommandParser` defined in ``COMMAND_CLASSES``,
       first CommandParser that signifies that it can handle the comand
       is executed, rest is omited.

       Command can change chatbot state.

    2. Whenever we reclieve a chat message with labadata it is passed
       handled by  :attr:`state`.

       Labdata can change bot state.
    """

    BOT_NAME = "czesio"

    BOT_COMMAND_PATTERN = r"\s*(\\%s)\s+(.*)"
    """
    Bot name will be injected into this regexp using python percent format,
    then it will match ``\\botname command``, where comamnd is anything.
    """

    MESSAGE_FOR_NEW_USER = """
    Czesc jestem bot {bot}! Żeby wysłać do mnie wiadomość musisz
    napisać \{bot} <polecenie>. By zobaczyć listę poleceń musisz wysłać
    \{bot} info
    """

    NOT_ENOUGH_RIGHT = """
        By wykonać tą operację musisz posiadać uprawnienia operatora
    """

    KV_STORE_PATH = None
    """
    Should point to yaml file that will be loaded and passed to states.

    :type: pathlib.Path
    """

    __BASE_KV_STORE_PATH = DATA_PATH / 'base.yaml'

    QABOT_DATA_PATH = None
    """
    Data for qabot
    """

    IDLE_STATE = BotIdle

    @classmethod
    def render_question(cls, q: Question):
        tmpl = ENVIORMENT.get_template('qabot_response.rst')
        return tmpl.render(q=q)

    @abc.abstractmethod
    def get_command_classes(self):
        """
        Returns a list of :class:`CommandParsers` this bot uses. It should
        return types (not instances).

        Additionally two default commands: ListAllCommandsCommand and FallbackHandler
        ale automatically added to the list.
        """
        return []

    def __init__(self, athena):
        super().__init__()
        self.athena = athena
        self.athena.results.add_callback(lambda x: self.state.on_new_results(x))
        self._state = None
        self.facade = athena.facade
        self.experiment = athena.expName
        self.bot_name = self.BOT_NAME
        self.chat_room = athena.chat_room
        self.control_room = athena.exp_room
        self.state_attached_to_experiment_run = {}
        """
        This attribute facilitated inter-state communication, for example if
        one state needs to send a fit, with data gathered from previous
        series.

        This should be cleared between experiment runs TODO: implement clearing
        """
        self.jid_role_map = {}
        self._kv_store = None
        self._qabot = None

    @property
    def kv_store(self) -> dict:
        """
        Returns a key-value store associated with the bot, it is used to
        store various metadata related to the experiment: from message
        texts to various experiment settings.
        """
        if self._kv_store is None:
            with self.__BASE_KV_STORE_PATH.open('r') as f:
                self._kv_store = yaml.load(f)
            if self.KV_STORE_PATH is not None:
                with self.KV_STORE_PATH.open('r') as f:
                    self._kv_store.update(yaml.load(f))
        return self._kv_store

    def get_value_from_kvstore(self, dotted_key:str):
        """
        :param dotted_key: A dot delimined path that returns an object stored
                           inside kvstore.

                           If kvstore contains a following structure:

                           {
                                'foo': {'bar': 42},
                                'baz': 12
                           }

                           get_value_from_kvstore('foo.bar') returns 42

        """
        data = self.kv_store

        # Sanity check
        if not re.match(DESCRIPTION_KEY_RE, dotted_key):
            raise ValueError("Invalid key format")

        for part in dotted_key.split("."):
            data = data[part]

        return data


    @property
    def qabot(self):
        if self._qabot is None:
            if self.QABOT_DATA_PATH is None:
                return None
            data = QaBotData(
                self.QABOT_DATA_PATH,
                DATA_PATH / 'paths.yaml',
                DATA_PATH / 'stopwords.yaml'
            )
            data.parse()
            self._qabot = QaBot(data)
            self._qabot.initialize()

        return self._qabot

    def initialize(self):
        """
        Initializes the bot
        """
        assert isinstance(self.chat_room, RoomDescription)
        self.state = self.IDLE_STATE
        self.parsers = [
            cls(self) for cls in itertools.chain(
                self.get_command_classes(), self.default_command_classes())]
        self.chat_room.add_message_listener(self._process_message)
        self.chat_room.add_message_listener(self._qabot_process_message)

        self.control_room.register_labdata_listener(self.process_labdata)
        self.control_room.register_visitor_online_listener(
            self._on_visitor_online
        )
        self.control_room.register_visitor_offline_listener(
            self._on_visitor_offilne
        )

    def is_executing_series(self) -> bool:
        return not isinstance(self.state, self.IDLE_STATE)

    @property
    def state(self) -> 'chatbot_state.BotState':
        """
        Returns current bot state
        """
        return self._state

    @state.setter
    def state(self, val):
        """
        Sets new state by:

        1. Instantiating the new state.
        1. Calling :meth:`BotState.on_state_exit`

        :param val: Type representing the new state
        :type val: Type being a subclass of BotState.

        """
        # TODO: Przenieść tworzenie stanu w sensowne miejsce
        val = val(self)
        # TODO: Jeśli będzie stan który ma self.parsers to trzeba tutaj mówić
        # że w nowym stanie są nowe polecenia.
        if self._state is not None:
            self._state.on_state_exit()
        self._state = val
        self._state.on_state_enter()

    def _on_visitor_online(self, user_jid, user_nick, room_role):

        if user_nick == self.facade.nick:
            return

        self.send_private_msg_to_room(
            user_nick,
            self.MESSAGE_FOR_NEW_USER.format(bot=self.BOT_NAME)
        )
        self.jid_role_map[str(user_nick)] = room_role

    def _on_visitor_offilne(self, user_jid, user_nick, room_role):
        if str(user_nick) in self.jid_role_map:
            del self.jid_role_map[str(user_nick)]

    def _qabot_process_message(self, user_jid, msg_body, user_nick):
        if user_nick == self.facade.nick:
            return
        qabot = self.qabot
        if qabot is None:
            return
        questions = qabot.on_question(msg_body)
        if len(questions) == 0:
            return
        pts, q = questions[0]
        self.send_msg_to_room(
            self.render_question(q)
        )

    def _process_message(self, user_jid, msg_body, user_nick):

        if user_nick == self.facade.nick:
            return

        match = re.match(self.command_pattern, msg_body)
        if not match:
            return

        try:
            for p in chain(self.state.parsers, self.parsers):
                if p.matches_command(match.group(2), self.BOT_NAME):
                    user_is_op = self.user_is_op_in_control(user_nick)
                    if p.REQUIRES_OP and not user_is_op:
                        self.send_private_msg_to_room(user_nick, self.NOT_ENOUGH_RIGHT)
                        return
                    new_state_maybe = p.execute_command(match.group(2), self.BOT_NAME, user_nick, user_is_op)
                    if new_state_maybe is not None:
                        self.state = new_state_maybe
                    return
        except:
            self.send_msg_to_room("Podczas przetwarzania żądania wystąpił błąd")
            raise

    def user_is_op_in_control(self, user_nick) -> bool:
        """
        Checks if user (defined by user_nick) is an operator in control room.
        """
        was_initially_participant = self.jid_role_map.get(str(user_nick), None) in {'participant', 'moderator'}
        owner_jid = str(self.control_room.get_user_jid(user_nick))
        has_reservation = self.athena.reservation_repo.is_reservation_in_progress(owner_jid)
        return was_initially_participant or has_reservation

    def process_labdata(self, labdata, labdata_metadata, *args, **largs):
        """
        Processes labdata by passing it to the state.
        """
        if labdata.namespace == 'silf:experiment:stop' and labdata.type == 'result':
            self.state_attached_to_experiment_run = {}

        new_state_maybe = self.state.on_labdata(labdata)
        if new_state_maybe is not None:
            self.state = new_state_maybe

    def send_labdata(self, labdata: LabdataBase):
        """
        Sends labdata object to the room
        """
        self.facade.send_msg_to_room(
            self.control_room.room_jid,
            labdata
        )

    def send_msg_to_room(self, message: str):
        """
        Sends chatmessage to the room

        :param message: String in a markdown format.
        """
        self.facade.send_chatmsg_to_room(self.chat_room.room_jid, message.strip())

    def send_private_msg_to_room(self, nick, message):
        """
        Sends private chatmessage to the room to user defined by nick.

        :param message: String in a markdown format.
        """
        self.facade.send_chatmsg_to_room(self.chat_room.room_jid, message.strip(), nick=nick)

    @property
    def logger(self):
        return logging.getLogger("bot={}".format(self.experiment))

    @property
    def bot_name(self):
        return self._bot_name

    @property
    def command_pattern(self):
        """

        Regex pattern used to do preliminary check to detect if user input
        is a command (without checking if any particular command matches it).

        >>> cb = ChatBot("foo", None, None)
        >>> cp = cb.command_pattern
        >>> match1 = re.match(cp, "\czesio cześć")
        >>> match1 is not None
        True
        >>> match1.group(0) == "\czesio cześć"
        True
        >>> match1.group(2) == "cześć"
        True
        """
        return self._bot_pattern

    @bot_name.setter
    def bot_name(self, val):
        self._bot_name = val
        self._bot_pattern = re.compile(self.BOT_COMMAND_PATTERN % val)

    def default_command_classes(self):
        result  = [
            ListAllCommandsCommand,
            FallbackHandler
        ]
        if self.qabot is not None:
            result.insert(0, ListQuestions)
        return result

    def send_new_series(
            self, series_label: str, series_data: list,
            series_settings: dict=None):
        """
        This method is used to whole series to client --- it can be used
        (for example to send fits).

        :param str series_label: Series name sent to the user.
        :param series_data: Iterable of dictionaries containing results as
                       silf.backend.commons.api.stanza_content._misc.Result`.
        :param series_settings: Optional settings that are send to users as
                                part of `silf:series:start` stanza.

        """
        if series_settings is None:
            series_settings = {}

        # TODO If athena enters the room after version has ben established
        # this will raise an error. . . Hovever version should be checked
        # and I'm not comforablle with
        # ``self.athena.exp_state.protocol_version is None or ....``
        # This is a problem during developement (when you restart athena all
        # the time). During production should not occour.
        # if self.athena.exp_state.protocol_version != '1.0.0':
        #     raise ValueError("Not ready for this version")

        series_uuid = str(uuid.uuid4())
        stanza = SeriesStartedStanza(
            seriesId=series_uuid,
            initialSettings=SettingSuite(**{
                k: Setting(v) for k, v in series_settings.items()
            })
        )
        stanza.label = series_label
        self.athena.facade.send_labdata_msg_to_room(
            self.control_room.room_jid,
            labdata=format_labdata(
                "silf:series:start",
                lid=str(uuid.uuid4()), ltype='result', suite=stanza)
        )
        if isinstance(series_data, dict):
            series_data = [series_data]
        for sd in series_data:
            stanza = ResultSuite(**sd)
            self.athena.facade.send_labdata_msg_to_room(
                self.control_room.room_jid,
                labdata=format_labdata(
                    "silf:results",
                    lid=str(uuid.uuid4()), ltype='data', suite=stanza)
            )
        stanza = SeriesStopped(seriesId=series_uuid)
        self.athena.facade.send_labdata_msg_to_room(
            self.control_room.room_jid,
            labdata=format_labdata(
                "silf:series:stop",
                lid=str(uuid.uuid4()), ltype='data', suite=stanza)
        )

