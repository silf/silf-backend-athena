# -*- coding: utf-8 -*-

import abc
import uuid
import time
from silf.backend.client.labdata import format_labdata
from athena_bot.modules.result_module import AggregateResultsModule
from silf.backend.commons.api import SeriesStopped, ProtocolVersion, \
    ModeSelection, Setting, SettingSuite
from silf.backend.commons.api.stanza_content._misc import ModeSelected, \
    SeriesStartedStanza

class BotState(object, metaclass=abc.ABCMeta):
    """
    Encapsulates a bot state. Bot state is something that is responsible
    for parsing labdatas.

    State can also overrde commands.
    """

    STATE_PARSERS = []

    def __init__(self, chat_bot):
        """

        :param ChatBot chat_bot:
        :return:
        """
        self.chat_bot = chat_bot
        """
        :type: athena_bot.chatbot.api.chatbot.ChatBot
        """
        self.kv_store = chat_bot.kv_store
        self.parsers = []
        """
        parsers
        """

    @abc.abstractmethod
    def on_labdata(self, labdata):
        """

        :param  labdata: Labdata recloeved
        :type labdata: silf.backend.client.labdata.LabdataBase
        :return: Either another BotState (type not instance) or None (if state should not be changed).
        """
        self.chat_bot.logger.debug("Reclieved labdata %s for state %s.".format(labdata, type(self).__name__))

    @abc.abstractmethod
    def on_new_results(self, aggregate: AggregateResultsModule):
        """
        Callback function launched when we reclieve new results.

        :param aggregate: Aggregated resultrs
        """
        pass

    def on_state_enter(self):
        """
        Callback executed when this state is set on a bot.
        """
        self.parsers = [
            c(self.chat_bot, self) for c in self.STATE_PARSERS
        ]

    def on_state_exit(self):
        """
        Callback executed when new state is set on a bot.

        Typically this will close experiment session.

        """
        pass

class BotIdle(BotState):
    """
    State in which bot does noting (default state)
    """

    def on_labdata(self, labdata):
        super()

    def on_new_results(self, aggregate):
        super().on_new_results(aggregate)

class SeriesState(BotState):

    """
    State that captures a series.

    This is an abstract class.
    """

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.results = {}
        self.seen_result_ids = set()
        self.got_series_done = False
        self.get_modes_id = None

    def on_state_enter(self):
        self.get_modes_id = None
        self.send_get_modes()

    def on_state_exit(self):
        # TODO: It should store seriesId and resend it on series:stop
        if not self.got_series_done:
            self.got_series_done = True
            self.chat_bot.send_msg_to_room(
                self.chat_bot.get_value_from_kvstore(
                    "base.commands.series_state.on_state_exit"
                )
            )
            self.chat_bot.send_labdata(format_labdata(
                'silf:series:stop',
                ltype='query',
                suite=SeriesStopped(
                    seriesId=None
                )
            ))

    def send_get_modes(self):
        self.get_modes_id = str(uuid.uuid4())
        self.chat_bot.send_labdata(format_labdata(
            "silf:protocol:version:set",
            ltype='query',
            suite=ProtocolVersion(version="1.0.0"),

        ))
        # self.chat_bot.send_labdata(format_labdata(
        #     'silf:experiment:stop',
        #     ltype='query'
        # ))
        self.chat_bot.send_labdata(format_labdata(
            'silf:mode:get',
            ltype='query',
            lid=self.get_modes_id
        ))

    def select_mode(self, mode_suite):
        """

        :param ModeSuite mode_suite:
        :return:
        """

        modes = mode_suite.modes
        if len(modes) == 1:
            for m in modes.keys():
                return m
        raise ValueError("If there are more than one mode you need to select one explicitly")

    def _do_mode_selection(self, labdata):
        suite = labdata.suite

        # This forces us to respond only if we initiated qet:mode
        # but it messes with the UI.
        # See: SILF-891
        # if self.get_modes_id is None:

        #     return
        # if labdata['id'] != self.get_modes_id:
        #     return

        new_mode = self.select_mode(suite)

        if new_mode not in suite.modes.keys():
            raise ValueError(
                "Invalid mode {}, avilable modes are {}".format(
                    new_mode, suite.modes.keys()))

        self.chat_bot.send_labdata(
            format_labdata(
                "silf:mode:set",
                "query",
                suite=ModeSelection(mode=new_mode)
            )
        )

    @abc.abstractmethod
    def get_series_settings(self, mode_selected_suite:ModeSelected):
        """
        :return: Return value should be compatible with ``start_series`` method.
        """
        pass

    @abc.abstractmethod
    def series_started_callback(self, series_started_suite: SeriesStartedStanza):
        """
        :return: Either none or a new state
        """
        pass

    @abc.abstractmethod
    def series_stopped_callback(self, series_stopped_suite: SeriesStopped) -> BotState:
        """
        :return: Either none or a new state
        """
        pass

    @abc.abstractmethod
    def experiment_stopped_callback(self) -> BotState:
        """
        :return: Either none or a new state
        """
        pass

    def _do_series_started(self, labdata) -> BotState:
        self.series_started_callback(labdata.suite)

    def _do_send_settings(self, labdata) -> BotState:

        suite = labdata.suite

        settings = self.get_series_settings(suite)

        return self.start_series(settings)

    def start_series(self, settings: dict) -> BotState:
        """
        :param settings: Either a SettingSuite or a dictionary containing
                         mapping from setting name to setting value.
        """

        if isinstance(settings, dict):
            settings = SettingSuite(**{k: Setting(value=v) for (k, v) in settings.items()})

        # TODO: Validate settings with Control Suite

        self.chat_bot.send_labdata(
            format_labdata(
                "silf:series:start",
                "query",
                suite=settings
            )
        )

    def _do_series_finish(self, labdata) -> BotState:
        return self.series_stopped_callback(labdata.suite)

    def _do_experiment_finish(self, labdata) -> BotState:
        return self.experiment_stopped_callback()

    def on_labdata(self, labdata):
        super().on_labdata(labdata)

        if labdata.type not in {'result', 'data'}:
            return None
        if labdata.namespace == 'silf:mode:get':
            return self._do_mode_selection(labdata)
        if labdata.namespace == 'silf:mode:set':
            return self._do_send_settings(labdata)
        if labdata.namespace == 'silf:series:start':
            return self._do_series_started(labdata)
        if labdata.namespace == 'silf:series:stop':
            return self._do_series_finish(labdata)
        if labdata.namespace == 'silf:experiment:stop':
            result_state = self._do_experiment_finish(labdata)
            if result_state is None:
                return self.chat_bot.IDLE_STATE

class ManySeriesState(SeriesState):

    """
    Normal bot state (for example: :type:`SeriesState`) performs single series
    and then is replaced by ``bot.IDLE_STATE``.

    This state performs many series in sequence, each series is identfied by
    ``series_number`` property.
    """

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.series_number=0
        """
        Identified by current series.
        """
        self.killed_by_command = False
        """
        If true it means that this state was killed by a stop:experiment
        command, which means it will shut down and won't start new series.
        """

    def on_state_exit(self):
        super().on_state_exit()
        self.killed_by_command = True

    @abc.abstractmethod
    def _on_series_stopped(self, series_stopped_suite:SeriesStopped):
        pass

    @property
    @abc.abstractmethod
    def has_next_series(self):
        pass

    def get_series_settings(self, mode_selected_suite:ModeSelected):
        return self._get_series_settings()

    @abc.abstractmethod
    def _get_series_settings(self) -> dict:
        pass

    def series_stopped_callback(self, series_stopped_suite):
        if self.killed_by_command:
            return
        self._on_series_stopped(series_stopped_suite)

        if self.has_next_series:
            self.series_number+=1
            self.start_series(self._get_series_settings())

