# -*- coding: utf-8 -*-

import pathlib
import uuid
import time
import numpy as np
from silf.backend.client.labdata import format_labdata

from athena_bot.chatbot.api import ChatBot, SimpleCommandParser, \
    StopSeriesCommand
from athena_bot.chatbot.api.command_parser import CommandParser
from athena_bot.chatbot.experiments.blackbody.bot import SingleSeriesCommand
from athena_bot.chatbot.experiments.michelson.lambda_calculation import calculate_lambda, \
    CalculationError
from athena_bot.chatbot.experiments.michelson.states import \
    PerformMichelsonState
from silf.backend.commons.api.stanza_content._misc import ModeSelected, \
    OutputFieldSuite, ChartField, Result
from silf.backend.commons.api.stanza_content.control._suite import ControlSuite


class CommentaryOn(SimpleCommandParser):

    COMMAND = ['komentarz', 'start']

    COMMAND_DESCRIPTION = """
    Włącza komenarz do ekspoerymentu
    """

    def execute_command(self, command_text: str, bot_name: str, user_nick: str,
                        user_is_op: bool):
        self.chat_bot.commentary = True
        self.send_chat_message(self.chat_bot.kv_store['command']['commentary']['on'])


class CommentaryOff(SimpleCommandParser):

    COMMAND = ['komentarz', 'stop']

    COMMAND_DESCRIPTION = """
    Wyłącza komenarz do ekspoerymentu
    """

    def execute_command(self, command_text: str, bot_name: str, user_nick: str,
                        user_is_op: bool):
        self.chat_bot.commentary = False
        self.send_chat_message(self.chat_bot.kv_store['command']['commentary']['off'])

class SeriesStart(SingleSeriesCommand):

    COMMAND = ['seria', 'start']

    COMMAND_DESCRIPTION = """
    Włącza serię pomiarową
    """

    SERIES_STATE = PerformMichelsonState


class ShowLambda(SimpleCommandParser):

    COMMAND = ['wyznacz', 'lambda']

    COMMAND_DESCRIPTION = """
    Pokazuje jak wyznaczyć długość fali. Uwaga polecenie to skasuje aktualny wykres.
    """

    def execute_command(self, command_text: str, bot_name: str, user_nick: str,
                        user_is_op: bool):
        exp_state = self.chat_bot.athena.exp_state
        if exp_state.is_series_running:
            self.send_chat_message(
                self.chat_bot.kv_store['state']['show_labda']['series_is_running']
            )
            return
        if not self.chat_bot.current_results.get('chart', None):
            self.send_chat_message(
                self.chat_bot.kv_store['state']['show_labda']['no_series_running']
            )
            return
        error_strings = self.chat_bot.kv_store['command']['lambda_calculation']['exception']
        try:
            self.__calculate_lambda()
        except CalculationError as e:
            self.send_chat_message(
                error_strings[e.args[0]]
            )
        except Exception:
            self.send_chat_message(
                error_strings['misc_error']
            )
            raise


    def __calculate_lambda(self):
        data = np.asarray(self.chat_bot.current_results['chart'])
        strings = self.chat_bot.kv_store['command']['lambda_calculation']
        result = calculate_lambda(data)
        # TODO Synthetic results for developement
        # result = {'fitted': np.array([  10.70175439,   18.61919505,   26.53663571,   34.45407637,
        #  42.37151703,   50.28895769,   58.20639835,   66.12383901,
        #  74.04127967,   81.95872033,   89.87616099,   97.79360165,
        # 105.71104231,  113.62848297,  121.54592363,  129.46336429,
        # 137.38080495,  145.29824561]), 'lambda_mm': 31.669762641898867, 'points': np.array([  11.,   18.,   26.,   34.,   43.,   50.,   59.,   66.,   75.,
        #  82.,   90.,   98.,  105.,  114.,  121.,  130.,  136.,  146.]), 'x_values': np.array([  1.,   2.,   3.,   4.,   5.,   6.,   7.,   8.,   9.,  10.,  11.,
        # 12.,  13.,  14.,  15.,  16.,  17.,  18.])}

        self.send_chat_message(strings['phase1'])

        self.chat_bot.athena.facade.send_labdata_msg_to_room(
            self.chat_bot.control_room.room_jid,
            labdata=format_labdata(
                "silf:mode:set",
                lid=str(uuid.uuid4()), ltype='result', suite=ModeSelected(
                    "bot-mode", str(uuid.uuid4()),
                    settings = ControlSuite(),
                    resultDescription= OutputFieldSuite(
                        chart=ChartField(
                            'chart', 'chart',
                            strings['chart']['label'],
                            strings['chart']['x_label'],
                            strings['chart']['y_label']
                    )
                )))
        )

        # TODO: Sleeps here are only for "dramatic" purposes (so not all data
        # is shown at the same moment).
        time.sleep(2)

        self.send_chat_message(strings['phase2'])
        self.chat_bot.send_new_series(
            strings['labels']['extrema'],
            series_data = {
                'chart': Result(
                    value=list(zip(result['x_values'], result['points'])),
                    pragma='replace'
                )
            }
        )
        time.sleep(2)
        self.send_chat_message(strings['phase3'])
        self.chat_bot.send_new_series(
            strings['labels']['fit'],
            series_data = {
                'chart': Result(
                    value=list(zip(result['x_values'], result['fitted'])),
                    pragma='replace'
                )
            }
        )
        time.sleep(2)
        lambda_val = result['lambda_mm']
        lambda_expected = self.chat_bot.kv_store['data']['expected_lambda_mm']
        format = {
            'lambda': lambda_val,
            'expected': lambda_expected
        }
        self.send_chat_message(strings['phase4']['begin'].format(**format))
        if abs(lambda_val - lambda_expected) < 2:
            self.send_chat_message(strings['phase4']['result_ok'].format(**format))
        else:
            self.send_chat_message(strings['phase4']['result_err'].format(**format))


class MichelsonBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa.yaml'

    def __init__(self, athena):
        super().__init__(athena)
        self.current_results = {}

    # QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa-cdc.yaml'

    def get_command_classes(self):
        return [
            SeriesStart,
            # CommentaryOn,
            # CommentaryOff,
            ShowLambda,
            StopSeriesCommand,

        ]

    def __on_results(self, *largs, **kwargs):
        self.current_results = self.athena.results.values_dict

    def initialize(self):
        super().initialize()
        self.athena.results.add_callback(
            self.__on_results
        )



