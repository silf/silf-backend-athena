# -*- coding: utf-8 -*-
import enum
from athena_bot.chatbot.api.state import SeriesState
from silf.backend.commons.api import ModeSelected, SeriesStopped, \
    SeriesStartedStanza


class SeriesLength(enum.Enum):

    SHORT = 1
    LONG = 2

class PerformMichelsonState(SeriesState):

    SETTINGS = {
        "end_pos": 240,
        "start_pos": 0,
        "step": 1
    }

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.parsers = [
            # TODO: Add fit state
        ]
        self.data = None

    def get_series_settings(self, mode_selected_suite:ModeSelected):
        return self.SETTINGS

    def series_stopped_callback(self, series_stopped_suite: SeriesStopped):
        super().series_stopped_callback(series_stopped_suite)

    def series_started_callback(self, series_started_suite: SeriesStartedStanza):
        super().series_started_callback(series_started_suite)

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()











