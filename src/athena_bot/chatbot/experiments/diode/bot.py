# -*- coding: utf-8 -*-
import time
from silf.backend.client.labdata import LabdataBase
from athena_bot.chatbot.api import *
import pathlib
from athena_bot.chatbot.experiments.diode.states import ShortForward, \
    StateBackwards


class StartShortCommand(StartSeriesCommand):


    COMMAND = ["start forward"]
    COMMAND_DESCRIPTION = "state.diodes.description"

    SERIES_STATE = ShortForward

class StartBackwardCommand(StartSeriesCommand):


    COMMAND = ["start backward"]
    COMMAND_DESCRIPTION = "state.backward.description"

    SERIES_STATE = StateBackwards



class DiodeBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa.yaml'

    def get_command_classes(self):
        return [
            StartShortCommand,
            StartBackwardCommand,
            StopSeriesCommand
        ]

    def __init__(self, *largs, **kwargs):
        super().__init__(*largs, **kwargs)

    def send_labdata(self, labdata: LabdataBase):
        # FIXME: This is a fix for a race condition in Diode Chatbot
        # If there is no sleep here I could block Diode with 100%
        # efficency
        time.sleep(2)
        super().send_labdata(labdata)

