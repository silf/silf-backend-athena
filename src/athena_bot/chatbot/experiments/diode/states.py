# -*- coding: utf-8 -*-
import enum
from sleekxmpp.plugins.xep_0202 import time
from athena_bot.chatbot.api import *
from athena_bot.chatbot.experiments.diode.const import Diode, ALL_LEDS_BACKWARD
from athena_bot.modules.result_module import AggregateResultsModule

import time

from .const import ALL_LEDS
from silf.backend.commons.api import SeriesStopped, SeriesStartedStanza


class ShortForward(ManySeriesState):

    SELECTED_LED = None

    SETTINGS = {
        "i_from": 0,
        "i_step": 0.75,
        "i_to": 10
    }

    def __init__(self, chat_bot):
        super().__init__(chat_bot)

        if self.SELECTED_LED is None:
            self.perform_for_leds = ALL_LEDS
        else:
            self.perform_for_leds = [self.SELECTED_LED]

        assert isinstance(self.chat_bot, ChatBot)
        self.chat_bot.send_msg_to_room(
            self.kv_store['state']['forward']['start']
        )

    def select_mode(self, mode_suite):
        return 'forward'

    def _get_series_settings(self):
        # time.sleep(1)
        settings = dict(**self.SETTINGS)
        diode = self.perform_for_leds[self.series_number]
        settings['diode'] = str(int(diode))
        diodes_desc = self.kv_store['state']['diodes']
        name = diode.name.lower()
        if name in diodes_desc:
            self.chat_bot.send_msg_to_room(
                diodes_desc[name]
            )
        return settings

    @property
    def has_next_series(self):
        return len(self.perform_for_leds) > self.series_number

    def _on_series_stopped(self, series_stopped_suite:SeriesStopped):
        pass

    def experiment_stopped_callback(self):
        # time.sleep(1)
        pass

    def on_state_exit(self):
        # time.sleep(1)
        super().on_state_exit()

    def series_stopped_callback(self, series_stopped_suite):
        super().series_stopped_callback(series_stopped_suite)
        # time.sleep(1)

    def series_started_callback(self,
                                series_started_suite: SeriesStartedStanza):
        super().series_started_callback(series_started_suite)

    def on_new_results(self, aggregate: AggregateResultsModule):
        super().on_new_results(aggregate)




class StateBackwards(ManySeriesState):



    # {"u_from": {"current": false, "value": 0},
    # "u_to": {"current": false, "value": -5},
    # "diode": {"current": false, "value": "2"},
    # "u_step": {"current": false, "value": 0.2}}

    SELECTED_LED = None

    SETTINGS = {
        "u_from": 0,
        "u_step": 0.25,
        "u_to": -5
    }

    def __init__(self, chat_bot):
        super().__init__(chat_bot)

        self.commmentary_state = self.CommentaryState.INITIAL

        if self.SELECTED_LED is None:
            self.perform_for_leds = ALL_LEDS_BACKWARD
        else:
            self.perform_for_leds = [self.SELECTED_LED]

        assert isinstance(self.chat_bot, ChatBot)
        self.chat_bot.send_msg_to_room(
            self.kv_store['state']['backward']['start']
        )

    def _get_series_settings(self):
        # time.sleep(1)
        settings = dict(**self.SETTINGS)
        diode = self.perform_for_leds[self.series_number]
        settings['diode'] = str(int(diode))
        diodes_desc = self.kv_store['state']['diodes']
        name = diode.name.lower()
        if name in diodes_desc:
            self.chat_bot.send_msg_to_room(
                diodes_desc[name]
            )
        return settings

    @property
    def has_next_series(self):
        return len(self.perform_for_leds)-1 > self.series_number


    def _on_series_stopped(self, series_stopped_suite:SeriesStopped):
        pass

    def experiment_stopped_callback(self):
        # time.sleep(1)
        pass

    def on_state_exit(self):
        # time.sleep(1)
        super().on_state_exit()

    def series_stopped_callback(self, series_stopped_suite):
        super().series_stopped_callback(series_stopped_suite)
        # time.sleep(1)

    def series_started_callback(self, series_started_suite: SeriesStartedStanza):
        # time.sleep(1)
        super().series_started_callback(series_started_suite)

    def select_mode(self, mode_suite):
        return 'reverse'

    def on_new_results(self, aggregate: AggregateResultsModule):
        diode = self.perform_for_leds[self.series_number]

        if Diode.ZENER == diode:
            voltages  = self.kv_store['state']['backward']['shotky_voltages']
            if 'chart' in aggregate.values_dict:
                last_result = aggregate.values_dict['chart'][-1]
                if self.commmentary_state == self.CommentaryState.INITIAL:
                    if last_result[1] < voltages['breakdown']:
                        self.chat_bot.send_msg_to_room(
                            self.kv_store['state']['backward']['breakdown']
                        )
                        self.commmentary_state = self.CommentaryState.BREAKDOWN
                elif self.commmentary_state == self.CommentaryState.BREAKDOWN:
                    if last_result[1] < voltages['over_range']:
                        self.chat_bot.send_msg_to_room(
                            self.kv_store['state']['backward']['over_range']
                        )
                        self.commmentary_state = self.CommentaryState.OVER_RANGE


    class CommentaryState(enum.Enum):

        INITIAL = 0
        BREAKDOWN = 1
        OVER_RANGE = 2