# -*- coding: utf-8 -*-

import enum

class Diode(enum.IntEnum):
    LED = 0
    SCHOTTKY = 1
    ZENER = 2
    GERMAN_HF = 3


ALL_LEDS = [
    Diode.SCHOTTKY,
    Diode.ZENER,
    Diode.LED
]

ALL_LEDS_BACKWARD = [
    Diode.SCHOTTKY,
    Diode.LED,
    Diode.ZENER,
]



