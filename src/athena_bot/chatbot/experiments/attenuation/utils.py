# -*- coding: utf-8 -*-

import numpy as np
from scipy.optimize import curve_fit

# [[2, 132.0], [5, 149.0], [7, 123.0], [10, 98.0], [15, 92.0], [20, 65.0]]


def fit_func(x, I0, mu):
    return I0 * np.exp(-mu * x)

__data = np.asanyarray([[2, 132.0], [5, 149.0], [7, 123.0], [10, 98.0], [15, 92.0], [20, 65.0]])


def do_att_fit(data):

    data = np.asanyarray(data)
    x = data[:, 0]
    y = data[:, 1]

    params, stddev = curve_fit(fit_func, x, y, [y[0], 0.1], maxfev=10000)

    return params

print(do_att_fit(__data))