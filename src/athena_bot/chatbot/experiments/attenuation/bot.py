# -*- coding: utf-8 -*-
import pathlib
from athena_bot.chatbot.api import StopSeriesCommand, StartSeriesCommand
from athena_bot.chatbot.api import ChatBot
from athena_bot.chatbot.experiments.attenuation.states import CheckLeadState, \
    CheckBackgroundState, CheckCooperState, CheckAluminiumState


class CheckBacgroundCommand(StartSeriesCommand):

    COMMAND = ['seria', 'tło', 'start']

    COMMAND_DESCRIPTION = """
    state.background.description
    """

    SERIES_STATE = CheckBackgroundState


class CheckLeadCommand(StartSeriesCommand):

    COMMAND = ['seria', 'ołów']

    COMMAND_DESCRIPTION = """
    state.lead.description
    """.strip()

    SERIES_STATE = CheckLeadState


class CheckCooperCommand(StartSeriesCommand):

    COMMAND = ['seria', 'miedź']

    COMMAND_DESCRIPTION = """
    state.cooper.description
    """.strip()

    SERIES_STATE = CheckCooperState


class CheckAluminiumCommand (StartSeriesCommand):

    COMMAND = ['seria', 'glin']

    COMMAND_DESCRIPTION = """
    state.aluminium.description
    """.strip()

    SERIES_STATE = CheckAluminiumState


class AttenuationBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'
    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'input-file-example.yaml'

    def get_command_classes(self):
        return [
            CheckBacgroundCommand,
            CheckLeadCommand,
            CheckAluminiumCommand,
            CheckCooperCommand,
            # StartGaussSeries,
            StopSeriesCommand,
        ]

    def __init__(self, *largs, **kwargs):
        super().__init__(*largs, **kwargs)