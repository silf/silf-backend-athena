# -*- coding: utf-8 -*-

import time
import numpy as np

import enum
import math
from athena_bot.chatbot.api import SeriesState, ChatBot
from silf.backend.commons.api.stanza_content._misc import Result

from .utils import do_att_fit, fit_func


class CheckBackgroundState(SeriesState):

    SERIES_TIME_MIN = 6

    DESIRED_ERROR_PERCENT = 3

    class CommentaryState(enum.Enum):
        INITIAL = 0
        DONE = 1

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.msgs = self.kv_store['state']['background']


    def select_mode(self, mode_suite):
        self.results_commentary = self.CommentaryState.INITIAL
        return "background"

    def on_state_enter(self):
        super().on_state_enter()
        assert isinstance(self.chat_bot, ChatBot)
        self.results_commentary = self.CommentaryState.INITIAL
        self.chat_bot.send_msg_to_room(
            self.msgs['experiment_start'].format(
                time=self.SERIES_TIME_MIN, error=self.DESIRED_ERROR_PERCENT)
        )

    def on_new_results(self, aggregated_results):
        if 'particle_count' not in aggregated_results.values_dict:
            return

        if 'chart_background' not in aggregated_results.values_dict:
            return

        point_count = aggregated_results.values_dict['particle_count']

        last_count = point_count[-1]

        if (math.sqrt(last_count) / last_count) * 100 < self.DESIRED_ERROR_PERCENT:
            chart = aggregated_results.values_dict['chart_background']
            self.chat_bot.state_attached_to_experiment_run['background'] = chart[-1][1]
            if self.results_commentary == self.CommentaryState.INITIAL:
                self.chat_bot.send_msg_to_room(
                    self.msgs['uncert_ok'].format(self.DESIRED_ERROR_PERCENT, chart[-1][1])
                )
                self.results_commentary = self.CommentaryState.DONE

    def get_series_settings(self, mode_selected_suite):
        return {
            'acquisition_time': int(self.SERIES_TIME_MIN*60),
            'light': False
        }

    def series_stopped_callback(self, series_stopped_suite):
        super().series_started_callback()

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)


class CheckMaterialState(SeriesState):

    SERIES_TIME_MIN = 3
    MATERIAL = None

    class CommentaryState(enum.Enum):

        INITIAL = 0
        WAITING_ON_SECOND_POINT = 2
        DONE = 3

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.msgs = self.kv_store['state']['material']
        self.gen = self.kv_store['declination']['material']['genetivus']
        self.nom = self.kv_store['declination']['material']['nominativus']

    def select_mode(self, mode_suite):
        return 'material'

    def get_possible_materials(self, mode_selected_suite):
        return set(mode_selected_suite.settings.controls \
            ['material_type']['metadata']['choices'].keys())

    def get_series_settings(self, mode_selected_suite):

        if mode_selected_suite is not None:
            self.possible_materials = self.get_possible_materials(mode_selected_suite)

        return {
            'acquisition_time': int(self.SERIES_TIME_MIN * 60),
            'material_type': self.MATERIAL,
            "background_value": 0
        }

    def on_state_enter(self):
        super().on_state_enter()
        assert isinstance(self.chat_bot, ChatBot)
        self.results_commentary = self.CommentaryState.INITIAL

        if 'background' not in self.chat_bot.state_attached_to_experiment_run:
            self.chat_bot.send_msg_to_room(self.msgs['no_background_fit'])

        genetivus = self.gen[self.MATERIAL]

        self.chat_bot.send_msg_to_room(
                self.msgs['experiment_start'].format(
                    material=genetivus, time=self.SERIES_TIME_MIN))

    def on_new_results(self, aggregate):
        data = self.chat_bot.athena.results.values_dict.get('chart_material', [])

        if self.results_commentary == self.CommentaryState.INITIAL:
            if len(data) > 0:
                self.chat_bot.send_msg_to_room(
                    self.msgs['first_point'].format(time=self.SERIES_TIME_MIN))
                self.results_commentary = self.CommentaryState.WAITING_ON_SECOND_POINT
        elif self.results_commentary == self.CommentaryState.WAITING_ON_SECOND_POINT:
            if len(data) > 2:
                first = data[0]
                second = data[1]
                if second[1] > first[1]:
                    self.chat_bot.send_msg_to_room(self.msgs['second_bigger_than_first'])
                else:
                    self.chat_bot.send_msg_to_room(self.msgs['second_ok'])

                self.results_commentary = self.CommentaryState.DONE

    def __do_fit(self, data):
        fit_data = np.copy(data)
        bck = self.chat_bot.state_attached_to_experiment_run.get('background', None)
        if bck:
            fit_data[1, :] -= self.SERIES_TIME_MIN*60*bck
        try:
            I0, mu = do_att_fit(data)
            self.fit_params = [I0, mu]
        except RuntimeError:
            self.chat_bot.send_msg_to_room(
                self.msgs['fit_invalid']
            )
            return False

        self.chat_bot.state_attached_to_experiment_run[self.MATERIAL] = {
            'I0': I0,
            'mu': mu,
        }
        if 'background' in self.chat_bot.state_attached_to_experiment_run:
            exp_back = self.chat_bot.state_attached_to_experiment_run['background']
        else:
            exp_back = self.msgs['background_not_measured']

        self.chat_bot.send_msg_to_room(
            self.msgs['fit_this_material'].format(
                I0=I0, mu=mu, exp_back=exp_back
            ))

        return True

    def __send_fitted_func(self, data):
        x = data[:, 0]

        fit_x = np.linspace(np.min(x), np.max(x), 100)
        fit_y = fit_func(fit_x, *self.fit_params)

        fit_x = fit_x.reshape(10, 10)
        fit_y = fit_y.reshape(10, 10)
        results = []

        for part_x, part_y in zip(fit_x, fit_y):
            results.append({
                'chart_material': Result(value=[[x, y] for x, y in zip(part_x, part_y)], pragma='append')
            })

        self.chat_bot.send_new_series(
            "Dopasowanie do materiału {mat} czas pomiaru {time}".format(
                mat=self.MATERIAL,
                time=int(self.SERIES_TIME_MIN * 60)
            ), results,
            series_settings=self.get_series_settings(None)
        )

    def __comment_results(self):

        other_materisls = self.possible_materials - {self.MATERIAL}

        executed_modes = set(self.chat_bot.state_attached_to_experiment_run.keys())

        materials_to_commenot = other_materisls & executed_modes

        if materials_to_commenot:
            for mat in sorted(materials_to_commenot):

                seq = list(self.kv_store['material_mu_sequence'])

                this_index = seq.index(self.MATERIAL)
                other_index = seq.index(mat)

                this_mu = self.chat_bot.state_attached_to_experiment_run[self.MATERIAL]['mu']
                other_mu = self.chat_bot.state_attached_to_experiment_run[mat]['mu']

                this_expects_bigger_mu = other_index - this_index < 0
                this_measured_bigger_mu = other_mu - this_mu < 0

                err = this_expects_bigger_mu ^ this_measured_bigger_mu

                expected_key = 'this_has_bigger_mu' if this_expects_bigger_mu else 'this_has_lesser_mu'
                ok_key = 'measurements_err' if err else 'measurements_ok'

                msg = '\n\n'.join([
                    self.msgs[expected_key], self.msgs['measured_mu'],
                    self.msgs[ok_key]
                ]).format(
                    this_mu=this_mu, other_mu=other_mu,
                    this_gen=self.gen[self.MATERIAL], other_gen=self.gen[mat],
                    this_nom=self.nom[self.MATERIAL], other_nom=self.nom[mat])

                self.chat_bot.send_msg_to_room(msg)


    def series_stopped_callback(self, series_stopped_suite):
        data = self.chat_bot.athena.results.values_dict.get('chart_material', [])
        # TODO: SILF-896 (poprawne sprawdzanie kto zakończył serię)

        if not data:
            return

        data = np.asanyarray(data)

        if not self.__do_fit(data):
            return

        self.__send_fitted_func(data)
        self.__comment_results()


    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)


class CheckLeadState(CheckMaterialState):
    MATERIAL = 'lead'


class CheckCooperState(CheckMaterialState):
    MATERIAL = 'cooper'


class CheckAluminiumState(CheckMaterialState):
    MATERIAL = 'aluminium'