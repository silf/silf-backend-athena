from silf.backend.commons.api.stanza_content._misc import Setting
from silf.backend.client.labdata import  format_labdata
from athena_bot.chatbot.api import *


class AverageSeriesStart(SeriesState):

    def on_state_exit(self):
        super().on_state_exit()

    def on_state_enter(self):
        assert isinstance(self.chat_bot, ChatBot)
        self.results_commentary = 0
        self.chat_bot.send_msg_to_room("Rozpoczynam serię pomiarową: 'Średnia'")
        self.send_get_modes()

    def select_mode(self, mode_suite):
        self.chat_bot.send_msg_to_room(
            "Uruchamiam teraz eksperyment w trybie średnia. "
            "Generator liczb losowych generuje liczby w danym zakresie. "
            "A na wykres trafia średnia ze wszystkich wygenerowanych liczb. "
            "Średnia ta zbiega do wartości oczekiwanej (co możecie zaobserwować). "
        )
        return "average"

    def on_new_results(self, aggregated_results):
        chart_results = aggregated_results['chart']['value']
        last_results = chart_results[-1]
        diff  = abs(self.avg - last_results[1])/self.avg
        if diff < 0.01 and len(chart_results) > 15:
          if self.results_commentary == 0:
                self.results_commentary+=1
                self.chat_bot.send_msg_to_room(
                    "Wyniki zaczynają zbiegać do wartości oczekiwanej."
                )
        if len(chart_results) >= 300 and self.results_commentary==1:
            self.results_commentary+=1
            self.chat_bot.send_msg_to_room(
                "Zebraliśmy już dość dużo pomiarów, możesz wyłączyć serię"
            )

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)

    def get_series_settings(self, mode_selected_suite):
        num_from = random.randint(0, 6)
        num_to = random.randint(num_from+1, 10)

        self.chat_bot.send_msg_to_room(
            "Generator liczb losowych będzie losował liczby z zakresu: "
            "{} do {}. ".format(num_from, num_to) +
            "Wyniki powinny zbiegać do średniej wynoszącej {}.".format((num_to + num_from)/2)
        )
        self.num_from = num_from
        self.num_to = num_to
        self.avg = (num_to + num_from)/2
        return SettingSuite(
            num_from = Setting(value=num_from),
            num_to = Setting(value=num_to)
        )

    def series_stopped_callback(self, series_stopped_suite):
        super().series_stopped_callback(series_stopped_suite)

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

class GaussSeriesStart(SeriesState):

    BIN_COUNT=100
    SUM_NUMBERS=50
    NUM_FROM=0
    NUM_TO=10

    def on_state_enter(self):
        super().on_state_enter()
        self.resuls_count = 10


    def select_mode(self, mode_suite):
        self.chat_bot.send_msg_to_room(
            """
            Uruchamiam eksperyment w trybie gauss, w trybie tym
            eksperyment sumuje {sum_numbers} liczb losowanych z
            rozkładu jednorodnego z zakresu od {num_from} do {num_to}.

            Następnie dane umieszczam na histogramie, czyli jeśli
            suma liczb wyniesie 55 odnajduje na wykresie zakres od 50
            do 60 i doda do niego jeden.
            """.format(sum_numbers=self.SUM_NUMBERS, num_from=self.NUM_FROM, num_to=self.SUM_NUMBERS)
        )
        return "gauss"

    def get_series_settings(self, mode_selected_suite):
        return SettingSuite(
            num_from = Setting(value=self.NUM_FROM),
            num_to = Setting(value=self.NUM_TO),
            sum_numbers = Setting(self.SUM_NUMBERS),
            bin_count = Setting(self.BIN_COUNT)
        )

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)

    def series_stopped_callback(self, series_stopped_suite):
        super().series_stopped_callback(series_stopped_suite)


class StartAverageSeries(StartSeriesCommand):

    COMMAND = ['series',  'start', 'average']

    COMMAND_DESCRIPTION = """
        Start average series
    """

    SERIES_STATE = AverageSeriesStart


class StartGaussSeries(StartSeriesCommand):

    COMMAND = ['series',  'start', 'gauss']

    COMMAND_DESCRIPTION = """
        Start Gauss series
    """

    SERIES_STATE = GaussSeriesStart


class ExampleBot(ChatBot):

    INITIAL_STATE = BotIdle

    def __init__(self, *largs, **kwargs):
        super().__init__(*largs, **kwargs)

    def get_command_classes(self):
        return [
            StartAverageSeries,
            StartGaussSeries,
            StopSeriesCommand
        ]
