# -*- coding: utf-8 -*-
import pathlib
from athena_bot.chatbot.api import ChatBot


class PhotoelectricBot(ChatBot):

    BOT_NAME = 'Maria'

    # KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa.yaml'

    def get_command_classes(self):
        return []
