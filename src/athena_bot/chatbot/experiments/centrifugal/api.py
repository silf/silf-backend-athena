# -*- coding: utf-8 -*-
import pathlib
from athena_bot.modules.result_module import AggregateResultsModule
from silf.backend.client.labdata import LabdataBase
from athena_bot.chatbot.api import StartSeriesCommand, ChatBot

from athena_bot.chatbot.api.state import SeriesState, BotState
from silf.backend.commons.api import ModeSelected, SeriesStartedStanza
from silf.backend.commons.api import SeriesStopped



class CentrifugalState(SeriesState):

    def get_series_settings(self, mode_selected_suite:ModeSelected):
        return {
            "start_velocity": 50,
            "end_velocity": 200,
            "point_count": 30}

    def series_stopped_callback(self, series_stopped_suite: SeriesStopped) -> BotState:
        return super().series_stopped_callback(series_stopped_suite)

    def series_started_callback(self, series_started_suite: SeriesStartedStanza):
        self.chat_bot.send_msg_to_room(
            self.chat_bot.get_value_from_kvstore('messages.series_commentary'))

    def on_new_results(self, aggregate: AggregateResultsModule):
        super().on_new_results(aggregate)

    def experiment_stopped_callback(self) -> BotState:
        return super().experiment_stopped_callback()


class SingleSeriesCommand(StartSeriesCommand):

    COMMAND = ['seria', 'start']

    SERIES_STATE = CentrifugalState

    COMMAND_DESCRIPTION = "state.single_series.description"



class CFBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa.yaml'

    def get_command_classes(self):
        return [
            SingleSeriesCommand
        ]

    def __init__(self, *largs, **kwargs):
        super().__init__(*largs, **kwargs)

