# -*- coding: utf-8 -*-
import pathlib
from athena_bot.chatbot.api import ChatBot
from athena_bot.chatbot.experiments.doppler.api import SingleSeriesCommand

from .position_state import StartSeriesMeasurement, DoExampleMeasurementState, \
    ManySeriesMeasurementAndAnaysis


class SeriesStart(SingleSeriesCommand):

    COMMAND = ['seria', 'wiele']

    COMMAND_DESCRIPTION = """
    Wykonuje automatycznie wiele serii pomiarowych,
    ale nie wyznacza oporów
    """

    SERIES_STATE = StartSeriesMeasurement

class SeriesStartOhm(SingleSeriesCommand):

    COMMAND = ['seria', 'opory']

    COMMAND_DESCRIPTION = """
    Wykonuje automatycznie wiele serii pomiarowych,
    i wyznacza opory
    """

    SERIES_STATE = ManySeriesMeasurementAndAnaysis



class CalculateOhms(SingleSeriesCommand):

    COMMAND = ['seria', 'jeden']

    COMMAND_DESCRIPTION = """
    Pomiar oporu dla jednego położenia
    """

    SERIES_STATE = DoExampleMeasurementState




class OhmBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa.yaml'

    def __init__(self, athena):
        super().__init__(athena)
        self.current_results = {}


    def get_command_classes(self):
        return [
            SeriesStart,
            CalculateOhms,
            SeriesStartOhm
            # CommentaryOn,
            # CommentaryOff,
            # ShowLambda,
            # StopSeriesCommand,

        ]

    def __on_results(self, *largs, **kwargs):
        self.current_results = self.athena.results.values_dict

    def initialize(self):
        super().initialize()
        self.athena.results.add_callback(
            self.__on_results
        )



