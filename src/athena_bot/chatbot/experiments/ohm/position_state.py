# -*- coding: utf-8 -*-
import enum
import random
import time
import uuid
import numpy as np
from athena_bot.chatbot.api.state import ManySeriesState, BotState, SeriesState
from athena_bot.modules.result_module import AggregateResultsModule
from silf.backend.client.labdata import format_labdata
from silf.backend.commons.api import SeriesStopped, SeriesStartedStanza, \
    ModeSelected, ControlSuite, OutputFieldSuite, ChartField
from silf.backend.commons.api.stanza_content._misc import Result
from silf.backend.commons.api.stanza_content.control._controls import \
    IntegerControl


def data_analysis(data_series) -> tuple:
    """

    :param data_series:
    :return: tuple containing (resistance_ohm:float, fit_params, is_series_ok:bool)
    """

    selector = (data_series[:, 0] > 2) & (data_series[:, 0] <= 8)
    dataset = data_series[selector, :]
    results = np.polyfit(dataset[:, 0], dataset[:, 1], 1)
    ohm = 1/results[0]
    numerator = np.where(data_series[:, 1]!=0, data_series[:, 1], np.min(data_series[data_series[:,1]!=0, 1]))
    chisq = np.sum(((data_series[:, 1] - np.polyval(results, data_series[:, 0]))/(numerator))**2)/len(data_series)

    return ohm, results,  chisq < 10



class StartSeriesMeasurement(ManySeriesState):

    MATERIAL_LENGTHS = list(range(1, 10, 1))

    # MATERIAL_LENGTHS = [2, 5]

    SERIES_SETTINGS = {
        "u_to": 10,
        "u_step": 1,
        "pos": 3,
        "u_from": 0
    }

    def __init__(self, chat_bot):
        super().__init__(chat_bot)

        self.data = {}

    def select_mode(self, mode_suite):
        """

        :param ModeSuite mode_suite:
        :return:
        """

        return "Resistor"

    def on_state_enter(self):
        super().on_state_enter()
        self.chat_bot.send_msg_to_room(
            self.chat_bot.get_value_from_kvstore('commands.many_points.start_measurement')
        )

    @property
    def has_next_series(self):
        return self.series_number < (len(self.MATERIAL_LENGTHS)-1)

    def _get_series_settings(self):
        settings = dict(self.SERIES_SETTINGS)
        self.pos = self.MATERIAL_LENGTHS[self.series_number]
        settings['pos'] = self.MATERIAL_LENGTHS[self.series_number]
        return settings

    def _on_series_stopped(self, series_stopped_suite:SeriesStopped):
        pass

    def on_new_results(self, aggregate: AggregateResultsModule):
        super().on_new_results(aggregate)
        self.data[self.pos] = aggregate.values_dict.get('chart', None)

    def series_started_callback(self,
                                series_started_suite: SeriesStartedStanza):
        super().series_started_callback(series_started_suite)

        if self.series_number > 0:
            self.chat_bot.send_msg_to_room(
                self.chat_bot.get_value_from_kvstore('commands.many_points.next_point')
            )



    def experiment_stopped_callback(self) -> BotState:
        print(self.data)
        if not self.has_next_series:
            self.chat_bot.send_msg_to_room(
                self.chat_bot.get_value_from_kvstore('commands.many_points.measurements_done')
            )
        return super().experiment_stopped_callback()


class ManySeriesMeasurementAndAnaysis(StartSeriesMeasurement):

    def __send_new_chart(self):
        title = self.chat_bot.get_value_from_kvstore("commands.example_measurement.chart.title")
        label_x = self.chat_bot.get_value_from_kvstore("commands.example_measurement.chart.axis_x")
        label_y = self.chat_bot.get_value_from_kvstore("commands.example_measurement.chart.axis_y")

        self.chat_bot.athena.facade.send_labdata_msg_to_room(
            self.chat_bot.control_room.room_jid,
            labdata=format_labdata(
                "silf:mode:set",
                lid=str(uuid.uuid4()), ltype='result', suite=ModeSelected(
                    "Resistor", str(uuid.uuid4()),
                    settings = ControlSuite(),
                    resultDescription= OutputFieldSuite(
                        chart=ChartField(
                            'chart', 'chart',
                            title, axis_x_label=label_x, axis_y_label=label_y
                    )
                )))
        )


    def __send_pos_to_resistance(self):
        pos_to_ohms = []
        for pos, data in self.data.items():
            data = np.asanyarray(data)
            ohm, __, __ = data_analysis(data)
            pos_to_ohms.append([pos, ohm])
        pos_to_ohms = sorted(pos_to_ohms)

        self.chat_bot.send_msg_to_room(
            self.chat_bot.get_value_from_kvstore('commands.many_points_analysis.measurements_done')
        )

        time.sleep(2)

        self.__send_new_chart()

        self.chat_bot.send_new_series(
            self.chat_bot.get_value_from_kvstore("commands.example_measurement.chart.series"),
            series_data = [{
                'chart': Result(
                    value=pos_to_ohms,
                    pragma='replace'
                )
            }]
        )


    def _on_series_stopped(self, series_stopped_suite:SeriesStopped):
        print(self.data)
        if not self.has_next_series:
            # Measurement is finished
            self.__send_pos_to_resistance()


class DoExampleMeasurementState(SeriesState):


    SERIES_SETTINGS = {
        "u_to": 10,
        "u_step": .2,
        "pos": 6,
        "u_from": 0
    }

    def select_mode(self, mode_suite):
        """

        :param ModeSuite mode_suite:
        :return:
        """
        return "Resistor"

    def on_new_results(self, aggregate: AggregateResultsModule):
        self.results = aggregate.values_dict.get('chart', None)

    def series_started_callback(self, series_started_suite: SeriesStartedStanza):
        pass

    def series_stopped_callback(self, series_stopped_suite: SeriesStopped) -> BotState:
        print(self.results)
        send_key = lambda key: self.chat_bot.send_msg_to_room(self.chat_bot.get_value_from_kvstore(key))
        results = np.asanyarray(self.results)
        ohms, fit_params, is_ok = data_analysis(results)
        send_key("commands.example_measurement.on_series_stop.initial")
        time.sleep(2)
        if is_ok:
            send_key("commands.example_measurement.on_series_stop.fit_ok")
        else:
            send_key("commands.example_measurement.on_series_stop.fit_bad")

        # TODO: Sleeps here are only for "dramatic" purposes (so not all data
        # is shown at the same moment).

        time.sleep(2)

        fitted_x = np.linspace(np.min(results[:, 0]), np.max(results[:, 0]), 100)
        fitted_y = np.polyval(fit_params, fitted_x)
        points = list(zip(fitted_x, fitted_y))

        self.chat_bot.send_new_series(
            "Dopasowanie",
            [{
                'chart': Result(value=points, pragma="replace")
            }]
        )

        msg = self.chat_bot.get_value_from_kvstore("commands.example_measurement.on_series_stop.fit_results")
        self.chat_bot.send_msg_to_room(msg.format(
            result=ohms,
            a=fit_params[0],
            b=fit_params[1]
        ))

        return super().series_stopped_callback(series_stopped_suite)


    def get_series_settings(self, mode_selected_suite:ModeSelected):
        self.chat_bot.send_msg_to_room(
            self.chat_bot.get_value_from_kvstore("commands.example_measurement.on_series_start")
        )
        res =  dict(self.SERIES_SETTINGS)
        res['pos'] = random.randint(3, 8)
        return res

    def experiment_stopped_callback(self) -> BotState:
        return super().experiment_stopped_callback()