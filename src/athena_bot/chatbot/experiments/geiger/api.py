# -*- coding: utf-8 -*-

import enum
from athena_bot.chatbot.api import *
import pathlib

import numpy as np


class ShortScanSeriesStart(SeriesState):

    ACQUISITION_TIME = 5
    STEPS = 8
    START_VOLTAGE = 300
    END_VOLTAGE = 600

    class CommentaryState(enum.Enum):
        INITIAL = 0
        ERROR = -1
        WAITING_FOR_COUNTS = 2
        WAITING_FOR_PLATEAOU = 3
        WAITING_FOR_CASCDE = 4
        DONE = 5

    def on_state_enter(self):
        assert isinstance(self.chat_bot, ChatBot)
        self.results_commentary = self.CommentaryState.INITIAL
        self.chat_bot.send_msg_to_room("Rozpoczynam serię pomiarową, w ramach tej serii dokonamy mało dokładnego skanu całej charakerystyki'")
        self.send_get_modes()
        self.voltage_step = (self.END_VOLTAGE-self.START_VOLTAGE)/self.STEPS

    def __update_results(self, aggregated_results):
        if 'chart' not in aggregated_results.values_dict:
            self.has_results = False
            return
        else:
            self.has_results = True
            chart_results = aggregated_results.values_dict['chart']

        self.last_results = chart_results[-1]

        self.chart_results = np.asarray(chart_results)

        self.chart_voltages = self.chart_results[:, 0]
        self.chart_counts = self.chart_results[:, 1]

        self.counts_diff = np.diff(self.chart_counts)/self.voltage_step/self.ACQUISITION_TIME

    def __get_plateou_start_voltage(self):
        """
        I assume that plateou starts 50V after we start getting some counts
        """
        where = np.where(self.chart_counts > 0)[0]
        if len(where) == 0:
            return None
        first = where[0]
        first_voltage = self.chart_voltages[first]
        return first_voltage + 50

    def __get_plateou_end_voltage(self):
        """
        I assume that plateaou ends 50V before counts start raising faster
        than 5 counts per 1V (assuming one secodn measurement)
        """
        where = np.where(self.counts_diff > 5)[0]
        if len(where) == 0:
            return None
        last = where[0]
        last_voltage = self.chart_voltages[last] - 50
        return last_voltage

    @property
    def last_voltage(self):
        if self.last_results is None:
            return None
        return self.last_results[0]

    @property
    def last_counts(self):
        if self.last_results is None:
            return None
        return self.last_results[1]

    def on_new_results(self, result_module):

        self.__update_results(result_module)

        if self.results_commentary == self.CommentaryState.INITIAL:
            if self.has_results:
                if self.last_counts == 0:
                    self.results_commentary = self.CommentaryState.WAITING_FOR_COUNTS
                    self.chat_bot.send_msg_to_room(
                        "Napięcie między anodą a katodą licznika jest zbyt małe by wywołać reakcję kaskadową, "
                        "na razie licznik nie rejestruje zliczeń"
                    )
                    return
                else:
                    self.chat_bot.send_msg_to_room(
                        "Na liczniku nie powinny teraz pojawiać się zliczenia jest to prawdodobnie problem uszkodzenia sprzętu. "
                        "Wyłączam serię pomiarową, pnieważ jako bot, nie jestem przygotowany na taką sytuację."
                    )
                    return self.chat_bot.IDLE_STATE

        if self.results_commentary == self.CommentaryState.WAITING_FOR_COUNTS:
            if self.last_counts > 0:
                self.results_commentary = self.CommentaryState.WAITING_FOR_PLATEAOU
                self.chat_bot.send_msg_to_room(
                    "Napięcie między anodą a katodą wynosi {:.0f}V i zaczęło być wystarczające by wywołać reakcję kaskadową, "
                    "zaczynają pojawiać się zliczenia. ".format(self.last_voltage)
                )
                return
        if self.results_commentary == self.CommentaryState.WAITING_FOR_PLATEAOU:
            sv = self.__get_plateou_start_voltage()
            if sv is not None and self.last_results[0] > sv:
                self.results_commentary = self.CommentaryState.WAITING_FOR_CASCDE
                self.chat_bot.send_msg_to_room(
                    "Wchodzimy w obszar plateaou (ok {:.0f}V), jest to obszar charakterystki "
                    "licznika G-M na którym liczba zliczeń się stabilizuje. ".format(self.last_voltage)
                )
                return
        if self.results_commentary == self.CommentaryState.WAITING_FOR_CASCDE:
            ev = self.__get_plateou_end_voltage()
            if ev is not None and self.last_results[0] > ev:
                self.results_commentary = self.CommentaryState.DONE
                self.chat_bot.send_msg_to_room(
                    "Plateou się kończy (ok {:.0f}V). Wchodzimy teraz na obszar charakterystyki "
                    "w którym napięcie między anodą a katodą jest na tyle duże "
                    "by kaskady powstawały spontanicznie".format(self.last_voltage)
                )

    def get_series_settings(self, mode_selected_suite):
        return {
            'acquisition_time': self.ACQUISITION_TIME,
            'point_count': self.STEPS,
            'start_voltage': self.START_VOLTAGE,
            'end_voltage': self.END_VOLTAGE
        }

    def series_stopped_callback(self, series_stopped_suite):
        self.chat_bot.send_msg_to_room(
            "Seria pomiarowa się skończyla."
        )

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)


class LongScanSeriesStart(ShortScanSeriesStart):

    ACQUISITION_TIME = 20
    STEPS = 45


class StartScanSeries(StartSeriesCommand):

    COMMAND = ['seria', 'skan', 'start', 'szybki']

    COMMAND_DESCRIPTION = """
    Szybki skan przez cały zakres charakterystyki licznika G-M
    """

    SERIES_STATE = ShortScanSeriesStart


class StartScanSeriesLong(StartSeriesCommand):

    COMMAND = ['seria', 'skan', 'start']

    COMMAND_DESCRIPTION = """
    Długi skan przez cały zakres charakterystyki licznika G-M
    """

    SERIES_STATE = LongScanSeriesStart


class GeigerBot(ChatBot):

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa-geiger.yaml'

    # KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    def get_command_classes(self):
        return [
            StartScanSeries,
            StartScanSeriesLong,
            # StartGaussSeries,
            StopSeriesCommand,
        ]