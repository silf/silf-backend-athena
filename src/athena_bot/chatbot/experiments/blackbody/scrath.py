# -*- coding: utf-8 -*-

import yaml
import pathlib

file = pathlib.Path(__file__).parent / 'messages.yaml'

with file.open() as f:
    print(yaml.load(f))