# -*- coding: utf-8 -*-


import numpy as np
from scipy.optimize import curve_fit


def fit_func(x, I0, T, offset):

    h = 6.62E-34
    c = 3E8
    k = 1.380E-23

    x = np.copy(x * 1E-9)

    exponent = (h * c) / (k * x * T)

    first = 2 * I0 * c**2 * h * np.power(x, -5)
    second = np.power(np.exp(exponent)-1, -1)

    return offset + first*second
#
# bb_data_1 = np.asarray([[370.0, -0.142], [710.0, -0.067], [1050.0, -0.003], [1390.0, -0.027], [1730.0, -0.052], [2070.0, -0.068], [2410.0, -0.083], [2750.0, -0.097], [3090.0, -0.107], [3430.0, -0.115]])


def do_bb_fit(data):

    data = np.asanyarray(data)
    x = data[:, 0]
    y = data[:, 1]

    I0 = np.max(y) - np.min(y)
    offset = np.min(y)

    params, stddev = curve_fit(fit_func, x, y, [I0, 2000, offset], maxfev=10000)

    return params

# print(do_bb_fit(bb_data_1))
