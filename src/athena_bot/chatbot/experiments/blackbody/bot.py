import pathlib

import re

from athena_bot.chatbot.api import StopSeriesCommand, StartSeriesCommand
from athena_bot.chatbot.api import ChatBot
from athena_bot.chatbot.experiments.blackbody.states import \
    AbstractBackbodyState, SingleSeriesState, ManySeriesState


class SingleSeriesCommand(StartSeriesCommand):

    COMMAND = ['seria', 'start', '[temperatura]']

    SERIES_STATE = SingleSeriesState

    COMMAND_DESCRIPTION = "state.single_series.description"

    def command_as_regex(self):
        return ".*seria\s+start\s+(?P<temp>\d+).*"

    def execute_command(self, command_text, *args, **kwargs):
        match = re.match(self.command_as_regex(), command_text)
        temp = int(match.group('temp'))
        if not AbstractBackbodyState.validate_temperature(self.chat_bot, temp):
            self.chat_bot.send_msg_to_room(
                self.chat_bot.kv_store['messages']['command']['invalid_temperature'])
            return

        class TemperatureState(self.SERIES_STATE):

            TEMPERATURE = temp

        return TemperatureState


class ManySeriesCommand(StartSeriesCommand):

    COMMAND = ['seria', 'przegląd', 'start']

    COMMAND_DESCRIPTION = "state.many_series.description"

    SERIES_STATE = ManySeriesState


class BlackbodyBot(ChatBot):

    KV_STORE_PATH = pathlib.Path(__file__).parent / 'messages.yaml'

    QABOT_DATA_PATH = pathlib.Path(__file__).parent / 'qa-cdc.yaml'

    def get_command_classes(self):
        return [
            SingleSeriesCommand,
            ManySeriesCommand,
            StopSeriesCommand,
        ]

    def __init__(self, *largs, **kwargs):
        super().__init__(*largs, **kwargs)