# -*- coding: utf-8 -*-

from athena_bot.chatbot.api import SeriesState, ChatBot

import numpy as np

from . import fits
from athena_bot.chatbot.experiments.blackbody.fits import fit_func
from silf.backend.commons.api import Result


class FitInvalidException(Exception):
    pass


class NoResultsException(Exception):
    pass


class AbstractBackbodyState(SeriesState):

    @classmethod
    def validate_temperature(cls, chat_bot, temp):
        min_temp = chat_bot.kv_store['validations']['temperature'][0]
        max_temp = chat_bot.kv_store['validations']['temperature'][1]

        if temp < min_temp:
            return False

        return temp < max_temp

    def _create_settings(self, temerature, resolution):
        return {
            'fidelity': resolution,
            'temperature': temerature,
            'begin_wavelength': self.kv_store['series_settigns']['wavelength'][0],
            'end_wavelength': self.kv_store['series_settigns']['wavelength'][1]
        }

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.__msgs = self.chat_bot.kv_store['messages']['abstract']

    def _initial_commentary(self):
        self.chat_bot.send_msg_to_room(
            self.__msgs['initial_commentary'])

    def _post_fit_commentary(self, temperature):
        self.chat_bot.send_msg_to_room(
            self.__msgs['post_fit_commentary'].format(
                temp_set=temperature,
                temp_fit=self.temp
            )
        )

    def __send_fits(self, set_temp):

        fit_x = np.linspace(
            self.kv_store['series_settigns']['wavelength'][0],
            self.kv_store['series_settigns']['wavelength'][1],
            100
        )

        fit_y = fit_func(fit_x, *[self.I0, self.temp, self.offset])

        fit_x = fit_x.reshape(10, 10)
        fit_y = fit_y.reshape(10, 10)

        results = []

        for part_x, part_y in zip(fit_x, fit_y):
            results.append({
                'result_chart': Result(value=[[x, y] for x, y in zip(part_x, part_y)], pragma='append')
            })

        self.chat_bot.send_new_series(
            "Dopasowanie do temperatury {set_temp:.2f}".format(
                set_temp=set_temp
            ), results,
            series_settings=self._create_settings(set_temp, 1)
        )

    def _do_fit(self, set_temp):

        all_results = self.chat_bot.athena.results.values_dict

        if 'result_chart' not in all_results:
            raise NoResultsException()

        data = np.asarray(all_results['result_chart'])

        try:
            self.I0, self.temp, self.offset = fits.do_bb_fit(data)
        except RuntimeError as e:
            raise FitInvalidException from e

        self.chat_bot.state_attached_to_experiment_run[set_temp] = {
            'I0': self.I0,
            'temp': self.temp,
            'offset': self.offset
        }

        self.__send_fits(set_temp)

    def on_new_results(self, aggregate):
        super().on_new_results(aggregate)


class SingleSeriesState(AbstractBackbodyState):

    TEMPERATURE = 2500
    RESOLUTION = 3

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.msgs = self.chat_bot.kv_store['messages']['single_temperature']
        self.series_started = False

    def get_series_settings(self, mode_selected_suite):
        self.series_started = True
        return self._create_settings(self.TEMPERATURE, self.RESOLUTION)

    def series_stopped_callback(self, series_stopped_suite):
        assert isinstance(self.chat_bot, ChatBot)
        if self.series_started:
            self.series_started = False
            self.chat_bot.send_msg_to_room(self.msgs['series_end'])

            self._initial_commentary()
            self._do_fit(self.TEMPERATURE)
            self._post_fit_commentary(self.TEMPERATURE)

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)


class ManySeriesState(AbstractBackbodyState):

    def __init__(self, chat_bot):
        super().__init__(chat_bot)
        self.series_id=0
        self.temperatures = self.kv_store['series_settigns']['temperature_series']
        self.killed_by_command = False

    @property
    def current_temp(self):
        return self.temperatures[self.series_id]

    def on_state_exit(self):
        super().on_state_exit()
        self.killed_by_command = True

    def series_stopped_callback(self, series_stopped_suite):
        if self.killed_by_command:
            return
        self._initial_commentary()
        self._do_fit(self.current_temp)
        self._post_fit_commentary(self.current_temp)

        if self.series_id < len(self.temperatures)-1:
            self.series_id+=1
            self.start_series(self._create_settings(self.current_temp, 1))
            if self.series_id == 2:
                self.chat_bot.send_msg_to_room(
                    self.kv_store['messages']['many_temperatures']['commentary'])

    def get_series_settings(self, mode_selected_suite):
        return self._create_settings(self.current_temp, 1)

    def series_started_callback(self, series_started_suite):
        super().series_started_callback(series_started_suite)

    def experiment_stopped_callback(self):
        super().experiment_stopped_callback()

