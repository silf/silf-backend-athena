
Wydaje mi się że zadałeś pytanie na które znam odpowiedź.

**Moje pytanie**: {{q.question}}

**Moja odpowiedź**:

{{q.answer}}

{% if q.related %}
**Powiązane pytania**:

{% for q in q.related %}- {{q.question}}

{% endfor %}
{% endif %}