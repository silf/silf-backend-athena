Bot wspiera {{ commands|length }} komend:

{% for c in commands %}
{{ c }}
    
{% endfor %}

{% if not user_is_op %}
**Kiedy zostaniesz operatorem mogą pojawić się nowe polecenia.**
{% endif %}