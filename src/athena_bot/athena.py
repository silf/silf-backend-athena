import atexit
import configparser
import os
from athena_bot.modules import AthenaCapabilities

import requests
from athena_bot.utils import AthenaUrlconf

from sleek_facade.facade import XmppFacade, RoomDescription
from silf.backend.commons.util.config import open_configfile, \
    ConfigValidationError
from athena_bot.experiment_room import ExperimentRoom

import logging
LOGGER = logging.getLogger(__name__)


class Athena(object):

    """
    Management Bot that manages single XMPP room (and hence a single exmperiment)
    """

    def __init__(self, experimentName, configPaths=('config/default.ini', ),
                 exitEvent=None, capabilities=None, FacadeType=XmppFacade):
        """
        :param str experimentName: section name from configuration describing given experiment
        :param list configPaths: list of paths in file system to configuration file. This file must contain sections:
        Athena and sections for each experiment room. Order of the paths in list is important, later configurations
        will override data from previous configurations.
        :param exitEvent Can be None, if present this process blocks on this event. When event
        occures process is closed
        :type exitEvent: :class:`multiprocessing.Event`
        """

        if capabilities is None:
            capabilities = AthenaCapabilities.default()

        self.capabilities = capabilities

        self.conf = self._open_configfile(configPaths)
        self.urlconf = AthenaUrlconf(self.conf.get('Athena', 'api_url'))
        self.expName = experimentName

        self.facade = FacadeType(self.conf, resource=experimentName)
        self.exp_state = None
        #TODO zrob jedna zmienna - anie dwie dublujace sie
        self.exp_room = self.roomDesc = None
        self.exit_event = exitEvent
        self.reservation_repo = None

        self.monitoring = None
        self.exp_results = None

        atexit.register(self.on_exit)

    @staticmethod
    def _open_configfile(config_paths):
        if isinstance(config_paths, str):
            config_paths = [config_paths]
        conf = open_configfile(config_paths[0])
        conf.read(config_paths[1:])
        additional_files = conf.get("Config", "AdditionalConfigFiles", fallback=None)
        if additional_files is not None:
            for file in additional_files.split(";"):
                if not os.path.exists(file):
                    raise ConfigValidationError("Nonexistent eupplementary config file {}".format(file))
                conf.read(file)
        return conf

    def configure_bot_room(self, roomName):

        LOGGER.info('configuring room for: %s', roomName)
        roomDesc = ExperimentRoom(self.conf[roomName])
        roomDesc.register_visitor_online_listener(self._on_new_user_online)
        self.facade.join_room(roomDesc)

        return roomDesc

    def configure_chat_room(self, botRoomDesc):
        LOGGER.info('configuring chat room')
        chatRoom = self._create_chat_room_desc(botRoomDesc)
        self.facade.join_room(chatRoom)
        botRoomDesc.chat_room = chatRoom
        return chatRoom

    def _create_chat_room_desc(self, expRoom):
        chatConf = configparser.ConfigParser()
        chatConf['chatRoom'] = {'roomName': expRoom.name + '-chat',
                                'roomDisplayName': 'Chat ' + expRoom.display_name,
                                'roomDescription': 'Chat for experiment ' + expRoom.display_name,
                                'isRoomModerated': False,
                                'isRoomPersistent': True}
        roomDesc = RoomDescription(chatConf['chatRoom'])
        # wyjdz z pokoju po jego stworzeniu
        roomDesc.on_room_created = lambda: self.facade.leave_room(roomDesc)

        return roomDesc

    def on_exit(self):
        LOGGER.info('Athena closing')
        self.reservation_repo.close()
        self.facade.disconnect()

    def _on_new_user_online(self, user_jid, user_nick, room_role):
        """

        :param sleekxmpp.jid.JID user_jid: JID object
        :param str user_nick: nick for the given jid in the room
        :param str room_role: current role in the room
        :return:
        """
        # send current state of experiment
        self.exp_state.on_user_join(user_jid, user_nick, room_role)

        if self.reservation_repo and self.reservation_repo.is_reservation_in_progress(user_jid.bare) \
                and room_role != 'participant':
            self.set_user_as_operator(user_jid, user_nick)
        elif self.roomDesc.is_visitor(user_jid.bare) and room_role != 'visitor':
            self.set_user_as_visitor(user_jid, user_nick)
        # send experiment old data - measurements which were finished during this session
        self._send_experiment_data()

    def _send_experiment_data(self):
        """
        Send historic data to user. According to specification this can
        be done after user is given observer or operator status.
        :return:
        """
        print('TODO: wyslanie danych do nowego uzytkownika')

    def set_user_as_operator(self, user_jid, user_nick=None):
        """ Set user as experiment operator.

        :param sleekxmpp.jid.JID user_jid: JID object
        :param str user_nick: nick of the user. Can be None
        :return:
        """
        self.facade.set_user_role_in_room(
            self.exp_room.room_jid,
            user_jid,
            'participant',
            user_nick
        )

    def set_user_as_visitor(self, user_jid, user_nick=None):
        """ Set user as experiment visitor.

        :param sleekxmpp.jid.JID user_jid: JID object
        :param str user_nick: nick of the user, can be None
        :return:
        """
        self.facade.set_user_role_in_room(
            self.exp_room.room_jid,
            user_jid,
            'visitor',
            user_nick
        )

    def run(self):
        """
            Method blocks current thread, so should be executed in new process or separated thread.
        """
        self.exp_room = self.roomDesc = self.configure_bot_room(self.expName)
        self.chat_room = self.configure_chat_room(self.roomDesc)

        self.reservation_repo = self.capabilities.instantiate_athena_module("reservation", self)
        self.exp_state = self.capabilities.instantiate_athena_module('expstate', self)
        self.results = self.capabilities.instantiate_athena_module("aggregate_results", self)
        self.bot = self.capabilities.instantiate_athena_module("chatbot", self)
        self.exp_results = self.capabilities.instantiate_athena_module("publish_results", self)
        self.monitoring = self.capabilities.instantiate_athena_module("monitoring", self)

        if self.exit_event is None:
            self.facade.connect(block=True)
        else:
            self.facade.connect(block=False)
            try:
                # self._tmp_send_exp_close()
                self.exit_event.wait()
            except KeyboardInterrupt:
                # this exception is propagated from spawner to subprocess, so catch it and close athena properly
                LOGGER.debug('KeyboardInterrupt - in ATHENA')
                self.on_exit()

    def create_authorized_http_session(self):
        session = requests.Session()
        session.headers.update({'Authorization': 'Token {}'.format(self.conf.get('Athena', 'silf_api_token'))})
        return session

    @property
    def athena_jid(self):
        return self.facade.jid
