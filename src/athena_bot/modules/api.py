# -*- coding: utf-8 -*-

class AthenaModule(object):

    def __init__(self, athena):
        self.athena = athena

    @property
    def exp_name(self):
        return self.athena.roomDesc.name

    @property
    def xmpp_facade(self):
        return self.athena.facade

    @property
    def exp_room(self):
        return self.athena.roomDesc
