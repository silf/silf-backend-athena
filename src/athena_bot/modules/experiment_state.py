# codeing=UTF-8
import abc

from silf.backend.client.labdata import format_labdata, LabdataExperimentStop
from silf.backend.client.const import SILF_EXPERIMENT_STOP

from uuid import uuid4
import logging
LOGGER = logging.getLogger(__name__)

# labdata.py has format_labdata which takes as last argument suite.
# Suite are small classes from _misc.py which in constructor take
# kwargs argument which is dictionary of values defined in DEFAULTS

from .api import AthenaModule
from .capabilities import AthenaCapabilities, CapabilityType


class ExpStateModule(AthenaModule):

    @property
    @abc.abstractmethod
    def labdata_map(self):
        return {}

    @abc.abstractmethod
    def stop_experiment(self):
        pass

    @abc.abstractmethod
    def on_user_join(self, user_jid, user_nick, room_role):
        """
        :param user_jid: jid of new user
        :param user_nick:  nick of new user
        :return:
        """
        pass

    @property
    @abc.abstractmethod
    def is_series_running(self):
        pass


@AthenaCapabilities.register_capability("expstate", CapabilityType.Disabled)
class DisabledExpState(ExpStateModule):

    def __init__(self, athena):
        super().__init__(athena)

        self._labdata_map = {}

        self.exp_room.register_labdata_listener(self.__set_labdata, 'silf:protocol:version:set', 'result')
        # self.exp_room.register_labdata_listener(self.__set_labdata, 'silf:mode:get', 'result')
        self.exp_room.register_labdata_listener(self.__on_mode_set, 'silf:mode:set', 'result')
        self.exp_room.register_labdata_listener(self.__set_labdata, 'silf:series:start', 'result')
        # when user stops series
        self.exp_room.register_labdata_listener(self.__on_series_stop, 'silf:series:stop', 'result')
        # when experiment stops series
        self.exp_room.register_labdata_listener(self.__on_series_stop, 'silf:series:stop', 'data')
        self.exp_room.register_labdata_listener(self.__on_experiment_stop, 'silf:experiment:stop')
        self.exp_room.register_visitor_offline_listener(self._on_user_offline)

    def __set_labdata(self, labdata, labdata_metadata):
        self._labdata_map[labdata.namespace] = labdata

    def __on_mode_set(self, labdata, labdata_metadata):
        """
        Store mode:set labdata. Additionally clear series labdata if present, when switching between modes.
        :param labdata:
        :param labdata_metadata:
        :return:
        """
        self.__set_labdata(labdata, labdata_metadata)
        if 'silf:series:start' in self._labdata_map:
            del self._labdata_map['silf:series:start']

    def _on_user_offline(self, user_jid, user_nick, room_role):
        """
        If experiment server losses connection stop the experiment and clear all data.
        :param user_jid:
        :param user_nick:
        :param room_role:
        :return:
        """
        # LOGGER.info("user offline: {}".format(user_jid))
        if user_jid.bare == self.exp_room.expJid:
            self.stop_experiment()
            # experiment is down, there will be no result send back for silf:experiment:stop, exec callback for this
            self.__on_experiment_stop(None, None)


    def __on_experiment_stop(self, labdata, labdata_metadata):
        """
        Handle experiment stop event - meaning that either reservation is finished or operator stopped the whole exp
        :param labdata:
        :return:
        """
        LOGGER.debug("ExpState: on_experiment_stop")
        self._labdata_map.clear()

    def __on_series_stop(self, labdata, metadata):
        if 'silf:series:start' in self._labdata_map:
            del self._labdata_map['silf:series:start']

    @property
    def labdata_map(self):
        return dict(self._labdata_map)

    @property
    def protocol_version(self):
        if 'silf:protocol:version:set' not in self._labdata_map:
            return None
        labdata = self._labdata_map['silf:protocol:version:set']
        return labdata.suite.version

    def stop_experiment(self):
        # TODO Not shure whether this shouldn't be in ExperimentRoom
        if 'silf:protocol:version:set' in self.labdata_map:
            stop_labdata = format_labdata(SILF_EXPERIMENT_STOP, 'query', uuid4().urn, None, None)
            self.xmpp_facade.send_labdata_msg_to_room(self.exp_room.room_jid, stop_labdata)


@AthenaCapabilities.register_capability("expstate", CapabilityType.Enabled)
class ExpState(DisabledExpState):

    NAMESPACES_TO_BE_SENT_TO_NEW_USER = [
        'silf:protocol:version:set', 'silf:mode:set', 'silf:series:start'
    ]

    def __init__(self, athena):
        super().__init__(athena)

    def on_user_join(self, user_jid, user_nick, room_role):
        """
        For each new user:
          - send version of labdata protocol
          - send mode of current experiment
          - send settings of current mesurement

        :param user_jid: jid of new user
        :param user_nick:  nick of new user
        :return:
        """
        LOGGER.debug("ExpState: on new user online")

        for ns in self.NAMESPACES_TO_BE_SENT_TO_NEW_USER:
            if ns in self._labdata_map:
                self.xmpp_facade.send_labdata_msg_to_room(self.exp_room.room_jid, nick=user_nick, labdata=self.labdata_map[ns])
