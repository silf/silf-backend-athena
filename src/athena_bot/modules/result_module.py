# -*- coding: utf-8 -*-

import abc
import collections
from athena_bot.aggregator import aggregate_results
from athena_bot.modules import AthenaCapabilities
from athena_bot.modules.api import AthenaModule
from athena_bot.modules.capabilities import CapabilityType

import logging
LOGGER = logging.getLogger(__name__)


class AggregateResultsModule(AthenaModule, metaclass=abc.ABCMeta):


    @abc.abstractmethod
    def add_callback(self, callback):
        """
        :param callable callback:
        Callable with signature:

            def func(agg):
                '''
                :param AggregateResultsModule agg:
                '''

        """
        pass

    @property
    @abc.abstractmethod
    def result_dict(self):
        """
        Dictionary of all resulrs in json format as got from server
        """
        pass

    @property
    @abc.abstractmethod
    def values_dict(self):
        """
        Dictionaty that contains result values, if these values are filled.

        If some result stanza has empty "value".
        """
        pass


@AthenaCapabilities.register_capability("aggregate_results", CapabilityType.Disabled)
class DisabledAggregateResults(AggregateResultsModule):

    def __init__(self, athena):
        super().__init__(athena)
        self.reset()
        self.initialize()
        self.callbacks = []

    def initialize(self):
        self.exp_room.register_labdata_listener(
            self.on_labdata_series_start,
            "silf:series:start", "result")

        self.exp_room.register_labdata_listener(
            self.on_labdata_series_stop,
            "silf:series:stop", "result"
        )

        self.exp_room.register_labdata_listener(
            self.on_labdata_series_stop,
            "silf:series:stop", "data"
        )

        self.exp_room.register_labdata_listener(
            self.on_labdata_results,
            "silf:results"
        )

    def add_callback(self, callback):
        self.callbacks.append(callback)

    @property
    def result_dict(self):
        return dict(self.results)

    @property
    def values_dict(self):
        def __convert_entry(entry):
            if 'value' not in entry:
                return None
            data = entry['value']
            if not isinstance(data, collections.Iterable):
                return data
            data = list(data)
            if len(data) == 0:
                return None
            return data

        return {
            k : __convert_entry(v) for k, v in self.results.items() if __convert_entry(v) is not None
        }

    def reset(self):
        self.results = {}
        self.seen_result_ids = set()
        self.series_running = False

    def on_labdata_series_start(self, labdata, labdata_metadata):
        self.reset()
        self.series_running = True

    def on_labdata_results(self, labdata, labdata_metadata):
        LOGGER.debug('labdata results: {}'.format(labdata))
        if labdata['id'] not in self.seen_result_ids:
            self.seen_result_ids.add(labdata['id'])
            self.results = aggregate_results(self.results, labdata.suite.__getstate__())
            for c in self.callbacks:
                c(self)

    def on_labdata_series_stop(self, labdata, labdata_metadata):
        self.series_running = False


@AthenaCapabilities.register_capability("aggregate_results", CapabilityType.Enabled)
class EnabledAggregateResults(DisabledAggregateResults):
    pass

    # TODO: Maybe this should send new results