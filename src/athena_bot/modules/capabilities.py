import enum
from collections import defaultdict


class CapabilityType(enum.IntEnum):
    Enabled = 1
    Disabled = 2


class AthenaCapabilities(object):

    DEFAULT_CAPABILITIES = {
        'reservation',
        'expstate'
    }

    KNOWN_CAPABILITIES = {
        'reservation',  # Reservation handling
        'read_experiments_from_remote_api',  # Creation of experiment rooms from silf API
        'chatbot',  # Deploy chatbot for all experiments where one is defined
        'expstate',  # Athena sends room state to clients (this setting implies: expstate_watch)
        'create_additional_rooms',
        'aggregate_results',
        'monitoring',   # publish events taking place in experiment process
        'publish_results'   # publish results gathered during experiment
    }

    CAPABILITIES_REGISTRY = defaultdict(lambda: {})

    @classmethod
    def register_capability(cls, name, type):
        if name not in cls.KNOWN_CAPABILITIES:
            raise ValueError("Unknown capability: {}".format(name))

        def decorator(Type):
            cls.CAPABILITIES_REGISTRY[name][type] = Type
            return Type

        return decorator

    @classmethod
    def default(cls):
        return cls(cls.DEFAULT_CAPABILITIES)

    @classmethod
    def from_config(cls, conf, section='Athena', key='capabilities'):
        capabilities = conf.get(section, key, fallback=None)

        if capabilities is None:
            capabilities = cls.DEFAULT_CAPABILITIES
        else:
            capabilities = {x.strip() for x in capabilities.split(",")}

        return cls(capabilities)

    def __init__(self, capabilities):
        self.capabilities = capabilities

        if len(self.capabilities - self.KNOWN_CAPABILITIES) > 0:
            raise ValueError("Unknown capability: {}".format(self.capabilities - self.KNOWN_CAPABILITIES))

    def __get_capabiity_type(self, capability):
        if capability in self.capabilities:
            return CapabilityType.Enabled
        return CapabilityType.Disabled

    def _get_capability(self, capability, type):
        """
        We guarantee that this is the only way in which this type loads
        enabled capabilities --- if you need to patch capability generation
        patch this method.
        """
        return self.CAPABILITIES_REGISTRY[capability][type]

    def load_capability_type(self, capability):
        capability_enabled = self.__get_capabiity_type(capability)
        try:
            capability_type = self._get_capability(capability, capability_enabled)
        except KeyError as e:
            raise KeyError("Couldn't load capability {} type {}".format(capability, capability_enabled.name))

        return capability_type

    def instantiate_athena_module(self, capability, athena, **kwargs):
        type = self.load_capability_type(capability)
        return type(athena, **kwargs)