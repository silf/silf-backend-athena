# -*- coding: utf-8 -*-
from athena_bot.modules.api import AthenaModule

from .capabilities import CapabilityType, AthenaCapabilities


class ChatBotModule(AthenaModule):
    pass


@AthenaCapabilities.register_capability("chatbot", CapabilityType.Disabled)
class DisabledChatBotModule(ChatBotModule):
    pass


@AthenaCapabilities.register_capability("chatbot", CapabilityType.Enabled)
class EnabledChatBotModule(ChatBotModule):
    def __init__(self, athena):
        super().__init__(athena)
        from athena_bot.chatbot.store import ChatBotStore
        store = ChatBotStore()
        if self.athena.expName in store:
            self.chatbot = store[self.athena.expName](athena)
            self.chatbot.initialize()