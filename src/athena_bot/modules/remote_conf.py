# codeing=UTF-8


import os

import configparser
import tempfile
import requests
from requests.exceptions import Timeout, ConnectionError
import logging
from athena_bot.modules.capabilities import AthenaCapabilities, CapabilityType
from athena_bot.utils import AthenaUrlconf

LOGGER = logging.getLogger(__name__)


class RemoteConfigModule(object):

    def __init__(self, spawner):
        self.spawner = spawner
        self.conf = spawner.conf
        self.urlconf = AthenaUrlconf(self.conf.get('Athena', 'api_url'))
        pass

    def generate_conf(self):
        return {'experiments': [], 'paths': []}


@AthenaCapabilities.register_capability("read_experiments_from_remote_api", CapabilityType.Disabled)
class DisabledRemoteConfig(RemoteConfigModule):
    pass


@AthenaCapabilities.register_capability("read_experiments_from_remote_api", CapabilityType.Enabled)
class EnabledRemoteConfig(RemoteConfigModule):

    def __init__(self, spawner):
        super().__init__(spawner)
        self.experiments_url = self.urlconf.experiment_list_url

    def _get_remote_conf(self):
        """
        Fetch experiment list from given url. In case of any errors empty list
        is returned.
        :return: List of available experiments.
        """
        exp_list = []
        try:
            r = requests.get(self.experiments_url)
            exp_list = r.json()
        except ConnectionError:
            LOGGER.exception('Error while trying to get experiments list from: %s', self.experiments_url)
        except Timeout:
            LOGGER.exception('Timeout while trying to get experiments list from: %s', self.experiments_url)

        return exp_list

    def _generate_conf_for_exp(self, exp_desc, conf):
        """
        Generates configuration for given exp_desc into conf.
        :param exp_desc: Data fetched from remote location about available experiments
        :param conf: Dictionary in which configuration for given experiment is generated.
        :return:
        """
        codename = exp_desc['codename']
        conf['expServerJid'] = codename + "-exp@${Xmpp:domain}"
        conf['roomName'] = codename
        conf['roomDisplayName'] = exp_desc['display_name']
        conf['roomDescription'] = exp_desc['display_name']
        conf['isRoomModerated'] = "True"
        conf['isRoomPersistent'] = "True"
        conf['isRoomSubjectChangeable'] = "False"

    def generate_conf(self):
        """
        Fetch configuration from remote location and generate configuration in to files listed under 'paths'
        key in returned map. Under key 'experiments' list of available experiments is generated.
        :return: Map with keys: 'experiments' and 'paths'
        """
        conf = {'experiments': [], 'paths': []}
        exp_list = self._get_remote_conf()
        if len(exp_list) > 0:
            config = configparser.ConfigParser()
            for ex in exp_list:
                codename = ex['codename']
                if codename in self.spawner.watched_experimets:
                    continue # This experiment is already in config, don't override settingss
                config[codename] = {}
                self._generate_conf_for_exp(ex, config[codename])
                conf['experiments'].append(codename)
            conf_file = os.path.join(
                tempfile.gettempdir(), "remoteExperiments.ini")
            fd = os.open(conf_file, os.O_WRONLY | os.O_CREAT | os.O_TRUNC, 0o600)
            with os.fdopen(fd, mode='w') as configfile:
                config.write(configfile)
                conf['paths'].append(conf_file)

        return conf