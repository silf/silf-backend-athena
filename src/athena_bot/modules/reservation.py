# codeing=UTF-8

from abc import ABCMeta, abstractmethod

from datetime import datetime
from threading import Thread, Lock, RLock
from copy import deepcopy, copy
import sched
import time
import abc
import requests
from requests.exceptions import Timeout, ConnectionError

import pytz

import logging
from athena_bot.modules.api import AthenaModule
from athena_bot.modules.capabilities import AthenaCapabilities
from athena_bot.modules.capabilities import CapabilityType

LOGGER = logging.getLogger(__name__)


class ReservationListener(metaclass=ABCMeta):

    @abstractmethod
    def on_reservation_start(self, reservation):
        """
            Executed when reservation starts
            :param athena_bot.reservation.SilfReservation reservation:
        """

    @abstractmethod
    def on_reservation_end(self, reservation):
        """
            Executed when reservation is finished
        """

REPOSITORY_SCHEDULER_DELAY = 5
"""
    delay in seconds for repeating ReservationRepository update operation
"""

REPOSITORY_SECTION = 'Athena'
"""
section in configuration with config for repository
"""


def utcnow():
    utcmoment_unaware = datetime.utcnow()
    utcmoment = utcmoment_unaware.replace(tzinfo=pytz.utc)
    return utcmoment


class SilfReservation(object):

    def __init__(self, start, end, ownerJid):
        """

        :param start: start time of reservation
        :param end:  end time of reservation
        :param str ownerJid: string representing jid of reservation maker
        :return:
        """
        self.start_time = start
        self.end_time = end
        self._owner_jid = ownerJid.lower()

    @property
    def owner_jid(self):
        """ Get reservation owner - maker.

        :return: string value of reservation owner
        :rtype: str
        """
        return self._owner_jid

    def is_finished(self):
        return self.end_time < utcnow()

    def is_in_progress(self):
        return self.start_time <= utcnow() < self.end_time

    def __str__(self):
        return 'reservation for {} from {} to {}'.format(self._owner_jid, self.start_time, self.end_time)


class ReservationRepositoryModule(AthenaModule):

    def add_reservation_listener(self, reservation_listener):
        pass

    @property
    def reservation_list(self):
        """
        Returns a list of ``SilfReservation``.
        """
        return None

    def is_reservation_in_progress(self, user_jid):
        """
        Check if for given user reservation is in progress
        :param str user_jid: String representation of jid for checked user
        :return: Returns true if for given user reservation is in progress
        """
        return None

    def close(self):
        """
        Destroys this instance, must be called to disable background thread.
        """
        pass


@AthenaCapabilities.register_capability("reservation", CapabilityType.Disabled)
class SimpleDisabledReservationRepository(ReservationRepositoryModule):

    @property
    def reservation_list(self):
        return []

    def is_reservation_in_progress(self, user_jid):
        return True

    def close(self):
        pass


class BaseEnabledReservationRepository(ReservationRepositoryModule):

    def __init__(self, athena):
        """

        :param exp_name: experiment codename used in REST requests
        :param reservationListener: Listener for reservation events
        :type reservationListener: :class:`ReservationListener`
        :param conf: configuration for reservationRepo
        :type conf: :class:`configparser.ConfigParser`
        :return:
        """
        LOGGER.info('Creating ReservationRepository for %s', athena.expName)

        super().__init__(athena)

        self.do_work = True
        self.listeners = []
        self._reservation_list = []
        self.current_reservation = None
        self.current_lock = RLock()
        self.scheduler_delay = self.athena.conf.getfloat(
            REPOSITORY_SECTION, 'reservationRefreshPeriod',
            fallback=REPOSITORY_SCHEDULER_DELAY)

        self.background_thread = Thread(
            target=self.__run_thread,
            name="[{}] ReservationRepository".format(self.exp_name)
        )

        self.add_reservation_listener(AthenaReservationListener(athena))

        self.__user_roles = {}

        self._update_repository()
        self.background_thread.start()

    def __run_thread(self):
        while True:
            with self.current_lock:
                self._update_repository()
                if not self.do_work:
                    return
            time.sleep(self.scheduler_delay)

    def _update_repository(self):
        LOGGER.debug('updating ReservationRepository - {}'.format(self.exp_name))
        self._update_reservation_list()

        self._check_current_reservation()
        self._start_new_reservation()


    @abc.abstractmethod
    def _update_reservation_list(self):
        self._reservation_list = []

    def _check_current_reservation(self):
        if self.current_reservation is None:
            return

        # TODO dodaj sprawdzenie czy rezerwacja nie zostala przedluzona

        if self.current_reservation.is_finished():
            LOGGER.debug('[%s] %s is finished', self.exp_name, self.current_reservation)
            for l in self.listeners:
                l.on_reservation_end(self.current_reservation)
            self.current_reservation = None

    def _start_new_reservation(self):
        if self.current_reservation is not None or not self.reservation_list:
            return
        if self.reservation_list[0].is_in_progress():
            self.current_reservation = self.reservation_list[0]
            for l in self.listeners:
                l.on_reservation_start(self.current_reservation)
            LOGGER.debug('[%s] %s started', self.exp_name, self.current_reservation)

    def add_reservation_listener(self, reservation_listener):
        with self.current_lock:
            self.listeners.append(reservation_listener)

    def close(self):
        with self.current_lock:
            self.do_work = False

    @property
    def reservation_list(self):
        with self.current_lock:
            return tuple(self._reservation_list)

    def is_reservation_in_progress(self, user_jid):
        """
        Check if for given user reservation is in progress
        :param str user_jid: String representation of jid for checked user
        :return: Returns true if for given user reservation is in progress
        """
        with self.current_lock:
            return self.current_reservation is not None and self.current_reservation.owner_jid == user_jid


@AthenaCapabilities.register_capability("reservation", CapabilityType.Enabled)
class EnabledReservationRepository(BaseEnabledReservationRepository):

    def __init__(self, athena):
        """

        :param exp_name: experiment codename used in REST requests
        :param reservationListener: Listener for reservation events
        :type reservationListener: :class:`ReservationListener`
        :param conf: configuration for reservationRepo
        :type conf: :class:`configparser.ConfigParser`
        :return:
        """
        LOGGER.info('Creating ReservationRepository for %s', athena.expName)

        self.recent_reservation_url = athena.urlconf.reservation_url_for(athena.roomDesc.name)

        super().__init__(athena)

    def _parse_time(self, time_string):
        time = datetime.strptime(time_string, '%Y-%m-%dT%H:%M:%S%z')
        return time

    def _update_reservation_list(self):
        self._reservation_list = []
        try:
            # TODO: When we update silf-app in masyter branch to suppot
            # token this can be replaced.
            #r = self.athena.create_authorized_http_session().get()
            r = requests.get(self.recent_reservation_url)
        except ConnectionError:
            LOGGER.exception('Error while trying to get reservation list from: %s', self.recent_reservation_url)
        except Timeout:
            LOGGER.exception('Timeout while trying to get reservation list from: %s', self.recent_reservation_url)
        else:
            if r.status_code == requests.codes.ok:
                rj = r.json()
                for res in rj:
                    start = self._parse_time(res['start_date'])
                    end = self._parse_time(res['end_date'])
                    new_reservation = SilfReservation(start, end, res['jid'])
                    if not new_reservation.is_finished():
                        self._reservation_list.append(new_reservation)
        LOGGER.debug('amount of recent reservations: {}'.format(len(self.reservation_list)))


class AthenaReservationListener(ReservationListener):

    def __init__(self, athena_bot):
        self.athena_bot = athena_bot

    def on_reservation_start(self, reservation):
        """
        Event occurs when reservation starts.
        :param SilfReservation reservation: reservation which is starting
        :return:
        """
        self.athena_bot.set_user_as_operator(reservation.owner_jid)

    def on_reservation_end(self, reservation):
        """
        Event occurs when reservation ends.
        :param SilfReservation reservation: reservation which just finished.
        :return:
        """
        self.athena_bot.set_user_as_visitor(reservation.owner_jid)
        # send experiment:stop stanza so that everything is finished after reservation has ended
        self.athena_bot.exp_state.stop_experiment()
