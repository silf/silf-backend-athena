# -*- coding: utf-8 -*-


import time
from threading import Lock

from sleek_facade.facade import XmppFacade, RoomDescription
from athena_bot.modules.api import AthenaModule

from .capabilities import CapabilityType, AthenaCapabilities


class CreateRoomsModule(object):

    def __init__(self, spawner):
        self.spawner = spawner


    def create_additional_rooms(self):
        pass

@AthenaCapabilities.register_capability("create_additional_rooms", CapabilityType.Disabled)
class DisabledCreateRoomsModule(CreateRoomsModule):

    def create_additional_rooms(self):
        pass


@AthenaCapabilities.register_capability("create_additional_rooms", CapabilityType.Enabled)
class EnabledCreateRoomsModule(CreateRoomsModule):

    def create_additional_rooms(self):
        worker = RoomWorker(self.spawner.conf_paths)
        worker.create_rooms(self.spawner.section)


class RoomWorker(object):

    """
        Worker to create additional rooms expected to be present in SILF environment
    """

    def __init__(self, config_paths=('config/default.ini', )):

        from athena_bot.athena import Athena
        self.conf = Athena._open_configfile(config_paths)
        self.facade = XmppFacade(self.conf, resource='roomConfig')
        self.lock = Lock()
        self.rooms_to_configure = 0

    def create_rooms(self, section='Athena'):
        """
        Based on configuration create additional rooms in xmpp server
        :param section: Section with additional rooms
        :return: None
        """
        if not self.conf.has_option(section, 'additionalRooms'):
            return
        room_list = self.conf[section]['additionalRooms'].split(',')
        self.rooms_to_configure = len(room_list)
        if self.rooms_to_configure == 0 or room_list[0] == '':
            # with empty value for additionalRooms option we get [''] array after split
            return

        try:
            for room in room_list:
                room_desc = RoomDescription(self.conf[room])
                room_desc.on_room_created = lambda: self.room_is_configured()
                self.facade.join_room(room_desc)

            self.facade.connect(block=False)
            #!!TODO brak obslugi jesli pokoje juz istnieja
            # wait till all rooms are configured
            while not self.is_work_done():
                print("Creating additional rooms")
                time.sleep(1)
        finally:
            # disconect from server after room creation
            self.facade.disconnect()

    def room_is_configured(self):
        """
        After room is created decrease number of rooms to be configured.
        So that after all rooms are created this worker can finish.
        :return:
        """
        with self.lock:
            self.rooms_to_configure -= 1

    def is_work_done(self):
        """
        :return: True if all rooms were configured
        """
        self.lock.acquire()
        finished = self.rooms_to_configure == 0
        self.lock.release()
        return finished
