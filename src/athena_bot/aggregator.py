import collections
from itertools import chain
import types


def _append_result(old_result, new_result, has_new_result):
    result = {}
    new_value = new_result.get('value')
    if new_value is None:
        return old_result
    if not isinstance(new_value, collections.Iterable):
        new_value = [new_value]
    result['value'] = old_result.get('value', []) + new_value
    return result


def _replace_result(old_result, new_result, has_new_result):
    if not has_new_result:
        return old_result
    result = {}
    result['value'] = new_result.get('value')
    return result


def _skip_result(old_result, new_result, has_new_result):
    # This actually makes sense --- if this is none kwy will be omitted
    # from results altogether
    return None


PRAGMA_FUNCTIONS = {
    'append': _append_result,
    'replace': _replace_result,
    'transient': _skip_result,
}


def aggregate_results(old_results, new_results):

    """
    >>> def mk_res(val, pragma):
    ...    return {"value": val, "pragma": pragma}

    Test whether for all pragmas we can cope with empty resuls

    >>> aggregate_results({"foo": mk_res([1, 2, 3], "append")}, {}) == {'foo': {'pragma': 'append', 'value': [1, 2, 3]}}
    True
    >>> aggregate_results({}, {"foo": mk_res([1, 2, 3], "append")}) == {'foo': {'pragma': 'append', 'value': [1, 2, 3]}}
    True

    In this case empty dict replaces old on
    >>> aggregate_results({"foo": mk_res([1, 2, 3], "replace")}, {}) == {'foo': {'pragma': 'replace', 'value': None}}
    True
    >>> aggregate_results({}, {"foo": mk_res([1, 2, 3], "replace")}) == {'foo': {'pragma': 'replace', 'value': [1, 2, 3]}}
    True


    >>> aggregate_results({"foo": mk_res([1, 2, 3], "transient")}, {}) == {}
    True
    >>> aggregate_results({}, {"foo": mk_res([1, 2, 3], "transient")}) == {}
    True

    """

    results = {}
    for curr_result in set(chain(new_results.keys(), old_results.keys())):
        old = old_results.get(curr_result, {})
        new = new_results.get(curr_result, {})
        pragma = new.get('pragma', old.get('pragma', None))
        if pragma is None:
            raise ValueError("Pragma is unset in results")
        value = PRAGMA_FUNCTIONS[pragma](old, new, curr_result in new_results)
        if value is not None:
            results[curr_result] = value
            results[curr_result]['pragma'] = pragma
    return results