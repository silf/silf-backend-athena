
import sys
import os
from athena_bot.spawner import AthenaSpawner
import logging


def main():

    file = sys.argv[1]

    logging.basicConfig(level=logging.INFO)

    if not os.path.isabs(file):
        path_str = os.path.abspath(__file__)
        path_str = os.path.split(path_str)[0]
        path_str = os.path.split(path_str)[0]
        file = os.path.join(path_str, file)

    asv = AthenaSpawner(file)
    asv.spawn()


if __name__ == '__main__':
    main()
