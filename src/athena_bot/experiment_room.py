from sleek_facade import facade

__author__ = 'santas'

from sleek_facade.facade import RoomDescription


class ExperimentRoom(RoomDescription):
    def __init__(self, roomConfig):
        super().__init__(roomConfig)
        self.expJid = roomConfig['expServerJid']
        # experiment for which room was created is a member of the room - so can write to it
        self.room_members.append(self.expJid)
        self._chat_room = None

    @property
    def experiment_codename(self):
        return self.name

    @property
    def chat_room(self):
        return self._chat_room

    @chat_room.setter
    def chat_room(self, chatRoom):
        self._chat_room = chatRoom

    def is_visitor(self, user_jid):
        """
        Check if user is a visitor
        :param user_jid: string value of jid or JID object of user to check
        :return: True if user is not admin, owner or member of the room
        :rtype: bool
        """
        user_jid = facade.jid_to_bare_str(user_jid)
        return not (user_jid in self.room_admins
                    or user_jid in self.room_owners
                    or user_jid in self.room_members)
