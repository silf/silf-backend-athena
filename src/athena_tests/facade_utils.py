# -*- coding: utf-8 -*-
from collections import namedtuple
from silf.backend.client.labdata import format_labdata
from sleek_facade.facade import XmppFacade


StoredMessage = namedtuple("StoredMessage", ['room_jid', 'labdata', 'is_private'])


class MockFacade(XmppFacade):

    def __init__(self, configData=None, configPath='config/default.ini',
                 section='Xmpp', resource=None):
        super().__init__(configData, configPath, section, resource)
        self.messages = []

    def connect(self, block=True):
        pass

    def _start_session(self, event):
        pass

    def send_labdata_msg_to_room(self, room_jid, labdata, nick=None):
        self.messages.append(StoredMessage(room_jid, labdata, False))

    #TODO to ponizej bedzie usuniete z orginalnej fasady
    def send_msg_to_room(self, room_jid, labdata):
        self.messages.append(StoredMessage(room_jid, labdata, False))

    def set_role(self, room, role, nick, ifrom=None):
        return True

    def leave_room(self, roomDesc):
        pass

    def disconnect(self):
        pass

    def send_private_msg_to_room(self, room_jid, nick, labdata):
        self.messages.append(StoredMessage(room_jid, labdata, True))

    def __send_msg_to_room_impl(self, room_jid, *, labdata=None, body=None, nick=None):
        # send_labdata_msg_to_room
        # send_chatmsg_to_room
        if labdata is not None:
            self.messages.append(StoredMessage(room_jid, labdata, False))
        elif body is not None:
            self.messages.append(StoredMessage(room_jid, body, False))

    def mock_send_labdata(self, room_jid, namespace, id, type, data=None, labdata_metadata=None):
        self.rooms[room_jid]._on_labdata(format_labdata(namespace, type, suite=data, lid=id), labdata_metadata)
