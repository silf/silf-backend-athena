# -*- coding: utf-8 -*-
import uuid
from contextlib import contextmanager
import re
import unittest
from urllib import parse
import datetime
import json
from silf.backend.client.labdata import format_labdata

from sleekxmpp import JID
import responses
import unipath
from unittest import mock

from athena_bot.modules import capabilities
from athena_bot.liseners.monitoring import Monitoring
from athena_bot.athena import Athena
from silf.backend.commons.api.stanza_content._misc import ModeSelected, \
    SettingSuite, SeriesStartedStanza, ModeSelection, ResultSuite, Result
from athena_tests.facade_utils import MockFacade
from sleek_facade.facade import LabdataMetadata


DIR = unipath.Path(__file__).parent
BIRTHDAY = datetime.datetime(1985, 9, 19, 21, 30) # It's always my birthday!
EXPERIMENT_UUID = str(uuid.uuid4())
SERIES_UUID = str(uuid.uuid4())


def reconstruct_request_body(request):
    if request.headers['Content-Type'] == 'application/x-www-form-urlencoded':
        return parse.parse_qsl(request.body)
    raise ValueError()


class AthenaTestCase(unittest.TestCase):

    URL_FILTER = None
    """
    Only urls matching URL_FILTER will be stored
    """

    def setUp(self):

        self.requests = []

        self.athena = Athena("example", DIR.child('botConf.ini'), FacadeType=MockFacade,
                             capabilities=capabilities.AthenaCapabilities({'monitoring', 'publish_results'}))
        self.athena.run()
        self.room = self.athena.roomDesc
        self.room_jid = self.athena.roomDesc.room_jid

        str_room_jid = str(self.room_jid)

        self.owner_jid = JID("user$owner@ilf.edu.pl")
        self.visitor_jid = JID("user$visitor@ilf.edu.pl")
        self.experment_jid = JID("experiment@ilf.edu.pl")
        self.athena.roomDesc.expJid = "experiment@ilf.edu.pl"
        self.athena_jid = JID("athena@ilf.edu.pl")
        self.athena.facade.jid = "athena@ilf.edu.pl"

        self.athena_role = LabdataMetadata(sender_jid="athena@ilf.edu.pl", sender_nick="athena")
        self.owner_role = LabdataMetadata(sender_jid=str_room_jid + "/owner", sender_nick="owner")
        self.visitor_role = LabdataMetadata(sender_jid=str_room_jid + "/visitor", sender_nick="visitor")
        self.experment_role = LabdataMetadata(sender_jid="experiment@ilf.edu.pl", sender_nick="experiment")

        self.facade = self.athena.facade
        self.monitoring = self.athena.monitoring
        self.results = self.athena.exp_results

        self.__set_up_preconditions()
        self.__set_up_now_mock()
        self.__set_up_responses_mock()

    def __set_up_preconditions(self):

        self.assertFalse(self.athena.monitoring.experiment_is_running)
        self.assertIsNone(self.athena.monitoring.current_series_uuid)
        self.assertIsNone(self.athena.monitoring.current_experiment_uuid)

    def __set_up_responses_mock(self):

        responses.start()
        responses.add_callback(responses.GET, re.compile(".*"), callback=self.__request_callback)
        responses.add_callback(responses.POST, re.compile(".*"), callback=self.__request_callback)
        responses.add_callback(responses.PATCH, re.compile(".*"), callback=self.__request_callback)

    def __set_up_now_mock(self):

        self.now_patch = mock.patch.object(Monitoring, 'now')
        self.now = self.now_patch.start()
        self.now.return_value = BIRTHDAY

    def __request_callback(self, request):
        parsed_url = parse.urlparse(request.url)

        if parsed_url.path.startswith(self.URL_FILTER):
            self.requests.append(request)

        return (200, [], '')

    @contextmanager
    def _assert_no_response(self, namespace, type, suite=None):
        yield
        self.facade.mock_send_labdata(
            self.room_jid,
            namespace=namespace,
            type=type,
            id=self.stanza_id,
            data=suite
        )

    @property
    def serialized_birthday(self):
        #TODO: Move this to config
        return BIRTHDAY.strftime('%Y-%m-%dT%H:%M:%S%z')

    @property
    def stanza_id(self):
        return str(uuid.uuid4())

    def send_message(self, namespace, type, id=None, data=None, role=None):

        if id is None:
            id = self.stanza_id

        if role is None:
            role = self.owner_role

        self.facade.mock_send_labdata(
            self.room_jid,
            namespace=namespace, type=type, id=id,
            data=data, labdata_metadata=role)

    def tearDown(self):
        self.now_patch.stop()
        responses.reset()
        responses.stop()
        super().tearDown()

    def __reset_requests(self):
        self.requests.clear()

    def clear_requests(self):
        self.requests.clear()

    def assert_single_request(self, msg="Expected only single request to be made"):
        self.assertEqual(len(self.requests), 1, msg)

    def assert_request_count(self, count):
        self.assertEqual(len(self.requests), count)

    def assert_no_requests(self, msg="Expected no requests will be made"):
        self.assertEqual(len(self.requests), 0, msg)

    def assert_response_contains(self, data_dict, request_idx=0, msg=None):
        self.assertEqual(
            dict(reconstruct_request_body(self.requests[request_idx])),
            data_dict,
            msg
        )

    @property
    def last_request(self):
        return self.requests[-1]

    def _set_mode(self):

        self.send_message(
            namespace="silf:mode:set", type="result",
            role=self.owner_role,
            data=ModeSelected(mode="foo", experimentId=EXPERIMENT_UUID)
        )

    def _start_series(self):

        self._set_mode()
        self.clear_requests()

        self.series_label = "Series label"
        self.send_message(
            namespace="silf:series:start", type="result", id=self.stanza_id,
            role=self.experment_role,
            data=SeriesStartedStanza(
                seriesId=SERIES_UUID,
                initialSettings=SettingSuite(),
                label=self.series_label))
        
    def _send_results(self, **data):
        self.send_message(
            namespace="silf:results",
            type="data",
            role=self.owner_role,
            data=ResultSuite(**data)
        )

    def _stop_experiment(self):
        self.send_message(
            namespace="silf:experiment:stop",
            type="result",
            role=self.owner_role,
            data=None
        )

    def _stop_series(self):
        self.send_message(
            namespace="silf:series:stop",
            type="result",
            role=self.owner_role,
            data=None
        )

    @contextmanager
    def subTest(self):
        """
        make it comatible with python 3.3 - add subtest() that is a noop
        :return:
        """
        yield


class TestMonitoring(AthenaTestCase):

    URL_FILTER = "/silf/api/monitoring"

    def test_experiment_enters_room(self):
        self.room._on_visitor_online(self.experment_jid, 'experiment', 'participant')
        with self.subTest():
            self.assertEqual(dict(reconstruct_request_body(self.last_request)), dict([('event_type', 'server_in'), ('date', '1985-09-19T21:30:00')]))
        with self.subTest():
            self.assert_single_request()

    def test_experiment_exits_room(self):

        self.room._on_visitor_online(self.experment_jid, 'experiment', 'participant')
        self.room._on_visitor_offline(self.experment_jid, 'experiment', 'participant')

        with self.subTest():
            self.assertEqual(dict(reconstruct_request_body(self.last_request)), dict([('event_type', 'server_out'), ('date', '1985-09-19T21:30:00')]))

        with self.subTest():
            self.assert_request_count(2)

    def test_other_user_enters_room(self):

        self.room._on_visitor_online(self.visitor_jid, 'other-user', 'participant')
        self.assert_no_requests()

    def test_other_user_exits_room(self):

        self.room._on_visitor_online(self.visitor_jid, 'other-user', 'participant')
        self.room._on_visitor_offline(self.visitor_jid, 'other-user', 'participant')

        self.assert_no_requests()

    def test_mode_set_query(self):

        self.facade.send_msg_to_room(
            self.room_jid, format_labdata(
                "silf:mode:set", ltype="query",
                suite=ModeSelection(mode="foo")))
        self.assert_no_requests()

    def test_mode_set(self):

        self._set_mode()

        self.assert_single_request()

        self.assert_response_contains({
            'event_type': 'exp_start',
            'session_uuid': EXPERIMENT_UUID,
            'date': self.serialized_birthday
        })

        with self.subTest():
            self.assertTrue(self.athena.monitoring.experiment_is_running)
            self.assertFalse(self.athena.monitoring.series_is_running)

        with self.subTest():
            self.assertEqual(self.monitoring.current_experiment_uuid, EXPERIMENT_UUID)

        with self.subTest():
            self.assertIsNone(self.monitoring.current_series_uuid)

    def test_check_settings_query(self):

        with self._assert_no_response("silf:settings:check", "query", SettingSuite()):
            self._set_mode()
            self.clear_requests()

    def test_check_settings_response(self):

        with self._assert_no_response("silf:settings:check", "result", SettingSuite()):
            self._set_mode()
            self.clear_requests()

    def test_start_series(self):

        with self._assert_no_response("silf:series:start", "query", SettingSuite()):
            self._set_mode()
            self.clear_requests()

    def test_start_series_response(self):

        self._start_series()

        self.assert_single_request()

        with self.subTest():
            self.assertTrue(self.athena.monitoring.experiment_is_running)
            self.assertTrue(self.athena.monitoring.series_is_running)

        with self.subTest():
            self.assertEqual(self.monitoring.current_experiment_uuid, EXPERIMENT_UUID)
            self.assertEqual(self.monitoring.current_series_uuid, SERIES_UUID)

        with self.subTest():
            self.assert_response_contains({
                'event_type': 'series_start',
                'session_uuid': EXPERIMENT_UUID,
                'series_uuid': SERIES_UUID,
                'date': self.serialized_birthday
            })

    def test_end_series_query(self):
        with self._assert_no_response("silf:series:stop", "query"):
            self._start_series()
            self.clear_requests()

    def test_end_series_response_by_user(self):
        self._start_series()
        self.clear_requests()
        self.send_message(
            namespace="silf:series:stop", type="result", role=self.owner_role,
            data=None
        )

        with self.subTest():
            self.assert_single_request()

        with self.subTest():
            self.assertTrue(self.athena.monitoring.experiment_is_running)
            self.assertFalse(self.athena.monitoring.series_is_running)

        with self.subTest():
            self.assert_response_contains({
                'additional_data': 'user_ended',
                'event_type': 'series_end',
                'session_uuid': EXPERIMENT_UUID,
                'series_uuid': SERIES_UUID,
                'date': self.serialized_birthday
            })

    def test_end_series_response_by_experiment(self):
        self._start_series()
        self.clear_requests()
        self.send_message(
            namespace="silf:series:stop", type="result", role=self.experment_role,
            data=None)

        with self.subTest():
            self.assert_single_request()
        with self.subTest():
            self.assertTrue(self.athena.monitoring.experiment_is_running)
            self.assertFalse(self.athena.monitoring.series_is_running)
        with self.subTest():
            self.assertEqual(self.monitoring.current_series_uuid, None)
        with self.subTest():
            self.assert_response_contains({
                'additional_data': 'exp_ended',
                'event_type': 'series_end',
                'session_uuid': EXPERIMENT_UUID,
                'series_uuid': SERIES_UUID,
                'date': self.serialized_birthday
            })

    def test_end_experiment_query(self):
        self._start_series()
        self.clear_requests()
        self.send_message(
            namespace="silf:experiment:stop", type="query", role=self.owner_role,
            data=None
        )
        self.assert_no_requests()

    def __assert_internal_postcoditions_when_experiment_done(self):

        with self.subTest():
            self.assertFalse(self.athena.monitoring.experiment_is_running)
        with self.subTest():
            self.assertFalse(self.athena.monitoring.series_is_running)
        with self.subTest():
            self.assertEqual(self.monitoring.current_series_uuid, None)
        with self.subTest():
            self.assertEqual(self.monitoring.current_experiment_uuid, None)

    def __test_end_experiment(self, role, aditional_data):
        self._start_series()
        self.clear_requests()
        self.send_message(
            namespace="silf:experiment:stop", type="result", role=role,
            data=None
        )
        self.__assert_internal_postcoditions_when_experiment_done()
        with self.subTest():
            self.assert_single_request()
        with self.subTest():
            self.assert_response_contains({
                'additional_data': aditional_data,
                'event_type': 'exp_end',
                'session_uuid': EXPERIMENT_UUID,
                'date': self.serialized_birthday
            })


    def test_end_experiment_by_experiment(self):
        self.__test_end_experiment(self.experment_role, 'sess_ended_exp')

    def test_end_experiment_by_owner(self):
        self.__test_end_experiment(self.owner_role, 'sess_ended_usr')

    def test_end_experiment_by_athena(self):
        self.__test_end_experiment(self.athena_role, 'sess_ended_ath')

    def test_end_experiment_response_no_series_started(self):
        self._set_mode()
        self.clear_requests()
        self.send_message(
            namespace="silf:experiment:stop", type="result", role=self.experment_role,
            data=None
        )
        self.__assert_internal_postcoditions_when_experiment_done()
        with self.subTest():
            self.assert_single_request()
        with self.subTest():
            self.assert_response_contains({
                'additional_data': 'sess_ended_exp',
                'event_type': 'exp_end',
                'session_uuid': EXPERIMENT_UUID,
                'date': self.serialized_birthday
            })

    def test_experiment_exits_when_session_is_online(self):
        self.room._on_visitor_online(self.experment_jid, "experiment", "participant")
        self._set_mode()
        self.clear_requests()
        self.room._on_visitor_offline(self.experment_jid, "experiment", "participant")
        self.__assert_internal_postcoditions_when_experiment_done()

        self.assertEqual(len(self.requests), 2)

        self.assert_response_contains({
            'event_type': 'exp_end',
            'session_uuid': EXPERIMENT_UUID,
            'date': self.serialized_birthday,
            'additional_data': 'experiment_disconnected'
        })

    def test_experiment_exits_when_series_is_online(self):
        self.room._on_visitor_online(self.experment_jid, "experiment", "participant")
        self._start_series()
        self.clear_requests()
        self.room._on_visitor_offline(self.experment_jid, "experiment", "participant")
        self.__assert_internal_postcoditions_when_experiment_done()

        self.assertEqual(len(self.requests), 3)

        self.assert_response_contains({
            'event_type': 'series_end',
            'session_uuid': EXPERIMENT_UUID,
            'series_uuid': SERIES_UUID,
            'date': self.serialized_birthday,
            'additional_data': 'experiment_disconnected'
        }, request_idx=0)

        self.assert_response_contains({
            'event_type': 'exp_end',
            'session_uuid': EXPERIMENT_UUID,
            'date': self.serialized_birthday,
            'additional_data': 'experiment_disconnected'
        }, request_idx=1)
#     #TODO: Test na SILF-749


class TestResults(AthenaTestCase):

    URL_FILTER = "/silf/api/experiment_results"

    def setUp(self):
        super().setUp()
        self._start_series()

    def test_user_is_saved_when_joins_the_experiment(self):
        self.room._on_visitor_online(self.visitor_jid, 'visitor', 'participant')
        self.assertIn(self.visitor_jid, self.results.session_users)

    def test_user_is_saved_even_when_he_left_the_experiment(self):
        self.room._on_visitor_online(self.visitor_jid, 'visitor', 'participant')
        self.room._on_visitor_offline(self.visitor_jid, 'visitor', 'participant')
        with self.subTest():
            self.assertIn(self.visitor_jid, self.results.session_users)
            self.assertNotIn(self.visitor_jid, self.results.online_users)

    def test_user_exits_the_room(self):
        self.room._on_visitor_offline(self.room_jid, 'experiment', 'visitor')

    def test_results_joined(self):
        self._start_series()
        self._send_results(foo=Result(value=[1, 2], pragma="append"))
        self._send_results(foo=Result(value=[3, 4], pragma="append"))
        self._stop_series()
        self._stop_experiment()
        body = dict(reconstruct_request_body(self.requests[0]))
        with self.subTest():
            self.assertEqual({'data', 'experiment_session_id', 'series_id', 'name'}, body.keys())
        with self.subTest():
            self.assertEqual(body['experiment_session_id'], EXPERIMENT_UUID)
            self.assertEqual(body['series_id'], SERIES_UUID)
        with self.subTest():
            self.assertEqual(json.loads(body['data']), {"foo": {"value": [1, 2, 3, 4], "pragma": "append"}})

    def test_results_sent_even_if_series_wasnt_stopped_explicitly(self):
        self._start_series()
        self._send_results(foo=Result(value=[1, 2], pragma="append"))
        self._send_results(foo=Result(value=[3, 4], pragma="append"))
        # self._stop_series() <- series is not stopped explicitly
        self._stop_experiment()
        body = dict(reconstruct_request_body(self.requests[0]))
        with self.subTest():
            self.assertEqual({'data', 'experiment_session_id', 'series_id', 'name'}, body.keys())
        with self.subTest():
            self.assertEqual(body['experiment_session_id'], EXPERIMENT_UUID)
            self.assertEqual(body['series_id'], SERIES_UUID)
        with self.subTest():
            self.assertEqual(json.loads(body['data']), {"foo": {"value": [1, 2, 3, 4], "pragma": "append"}})