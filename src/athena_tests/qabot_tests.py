# -*- coding: utf-8 -*-
import pathlib
import unittest
import yaml
from athena_bot.chatbot.api.chatbot import DATA_PATH
from qabot.bot import QaBot
from qabot.input_data import QaBotData
from qabot.test_utils import QabotTester


class QabotTestsBaseClass(QabotTester):

    FILE = None

    EXPECTED_WARNINGS = 0

    @classmethod
    def setUpClass(cls):
        data = QaBotData(
            cls.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        cls.warnings = data.parse()
        cls.qabot = QaBot(data)
        cls.qabot.initialize()

    def create_qabot(self) -> QaBot:
        return self.qabot

    def test_print_warnings(self):
        if len(self.warnings) > self.EXPECTED_WARNINGS:
            self.fail("\n".join(self.warnings))

    def assert_question(self, query, expected_question_id):
        questions = self.qabot.on_question(query)
        self.assertTrue(len(questions)>0)
        self.assertEqual(questions[0][1].id, expected_question_id)

class TestDopplerBot(QabotTestsBaseClass, unittest.TestCase):

    EXPECTED_WARNINGS = 4

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'doppler' / 'qa.yaml'

    def testDopplerColor(self):
        self.assert_question("Czym jest Doppler Color", 'doppler-color')

    def testDopplerLight(self):
        self.assert_question("Czy efekt Dopplera zachodzi także dla fal elektromagnetycznych?", 'doppler-light')
        self.assert_question("Czy zjawisko zachodzi także dla światła widzialnego?", 'doppler-light')
        self.assert_question("Czy zjawisko zachodzi także dla fotonów?", 'doppler-light')

    def testDopplerApplications(self):
        self.assert_question("Podaj przykłady zastosowania efektu Dopplera", 'doppler-zastosowania')
        self.assert_question("Jak można wykorzystać efekt (zjawisko) Dopplera?", 'doppler-zastosowania')
        self.assert_question("JCzy efekt (zjawisko) Dopplera można wykorzystać w praktyce", 'doppler-zastosowania')

    def testDopplerSpeed(self):
        self.assert_question("Czy efekt Dopplera zachodzi dla każdej prędkości źródła dźwięku?", 'doppler-speed')

    def testDopplerHypoersonic(self):
        self.assert_question("Czy efekt Dopplera zachodzi dla prędkości ponaddźwiękowych?", 'doppler-hypersonic')
        self.assert_question("Czy efekt Dopplera zachodzi dla prędkości ponaddźwiękowej?", 'doppler-hypersonic')
        self.assert_question("Czy efekt Dopplera zachodzi dla prędkości większej niż prędkość dźwięku?", 'doppler-hypersonic')


class TestPhotoQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'fotoefekt' / 'qa.yaml'

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot

class TestHallQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'hall' / 'qa.yaml'

    EXPECTED_WARNINGS = 5

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot

class TestOHMQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'ohm' / 'qa.yaml'

    EXPECTED_WARNINGS = 8

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot

class TestSnelliusQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'snellius' / 'qa.yaml'

    EXPECTED_WARNINGS = 2

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot

class TestMaxQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'maxwell' / 'qa.yaml'

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot
    
class TestDiodeQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'diode' / 'qa.yaml'

    EXPECTED_WARNINGS = 4

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot
    
class TestCentrQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'centrifugal' / 'qa.yaml'

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot
    
class TestMichelsonQabot(QabotTestsBaseClass, unittest.TestCase, ):

    FILE = pathlib.Path(__file__).parents[1] / 'athena_bot' / 'chatbot' / 'experiments' / 'michelson' / 'qa.yaml'

    EXPECTED_WARNINGS = 6

    # @classmethod
    def create_qabot(self) -> QaBot:
        data = QaBotData(
            self.FILE,
            DATA_PATH / 'paths.yaml',
            DATA_PATH / 'stopwords.yaml'
        )
        warnings = data.parse()
        qabot = QaBot(data)
        qabot.initialize()
        return qabot
