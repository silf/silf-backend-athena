# -*- coding: utf-8 -*-
import configparser

import unittest
from athena_bot.modules.capabilities import AthenaCapabilities
from silf.backend.commons.util.config import configure_logging

import io

CONFIG_FILE = """
[Xmpp]
host=ilf.fizyka.pw.edu.pl
port=5222
mucHost=muc.${host}
botJid=athena@${host}/head
botNick=athena
adminJid=admin@${host}
password=dupa

[Logging]
config_type = basic

[Athena]
# names of sections where each experiment is configured, only experiments from this list will be configured
experimentList=
# url for REST api to get latest reservation for given experiment
recentReservationUrl=http://ilf.fizyka.pw.edu.pl/silf/api/reservation/experiment/EXP_CODENAME/recent.json
# delay time in seconds to refresh reservation list
reservationRefreshPeriod=0.1
# list of additional rooms to configure in XMPP server
;additionalRooms=general
remoteConfigUrl=http://ilf.fizyka.pw.edu.pl/silf/api/experiment.json
capabilities=chatbot,expstate
"""


class TestCapabilitiesCreation(unittest.TestCase):

    def test_default_capabilities(self):
        capa = AthenaCapabilities.default()
        self.assertEqual(capa.capabilities, {'reservation', 'expstate'})

    def test_capabilities_from_file(self):
        cp = configparser.ConfigParser()
        cp.read_file(io.StringIO(CONFIG_FILE))
        capa = AthenaCapabilities.from_config(cp)
        self.assertEqual(capa.capabilities, {"chatbot", "expstate"})
