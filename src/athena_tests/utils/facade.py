# -*- coding: utf-8 -*-
from collections import namedtuple
import uuid
from sleekxmpp import JID
from silf.backend.client.labdata import format_labdata

from sleek_facade import facade

StoredLabdataMessage = namedtuple("StoredMessage", ['room_jid', 'labdata', 'target_nick'])
StoredChatMessage = namedtuple("StoredChatMessage", ['room_jid', 'body', 'target_nick'])


class TestFacade(facade.BaseFacade):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.labdata_messages = []
        self.chat_messages = []

    def mock_user_online(self, room_jid, user_jid, user_nick, user_role):
        room_jid = JID(room_jid)
        user_jid = JID(user_jid)
        self.rooms[room_jid].add_user_to_room(user_jid, user_nick)
        self.rooms[room_jid]._on_visitor_online(user_jid, user_nick, user_role)

    def mock_user_offline(self, room_jid, user_jid, user_nick, user_role):
        self.rooms[room_jid].remove_user(user_jid)
        self.rooms[room_jid]._on_visitor_offline(user_jid, user_nick, user_role)

    def return_labdata_matching_sent_by_athena(self, namespace):
        result = []
        for lm in self.labdata_messages:
            if lm.labdata.namespace == namespace:
                result.append(lm.labdata)
        return result

    def get_user_role(self, room_jid, user_nick):
        room = self.rooms[room_jid]
        if user_nick in room.roles:
            return room.roles[user_nick]
        if user_nick in room._user_nick_to_jid:
            return 'visitor'
        raise ValueError("Unknown user '{}'".format(user_nick))

    def set_user_role_in_room(self, room_jid, user_jid, role, user_nick=None):
        if not user_nick:
            room_desc = self.rooms[room_jid]
            user_nick = room_desc.get_user_nick(user_jid)
        self.rooms[room_jid].roles[user_nick] = role

    def send_labdata_msg_to_room(self, room_jid, labdata, nick=None):
        self.labdata_messages.append(
            StoredLabdataMessage(room_jid, labdata, nick)
        )

    def send_chatmsg_to_room(self, room_jid, body, nick=None):
        self.chat_messages.append(
            StoredChatMessage(room_jid, body, nick)
        )

    def mock_send_labdata(
            self, room_jid, namespace, type, id=None, data=None):
        if id is None:
            id = str(uuid.uuid4())
        self.rooms[room_jid]._on_labdata(format_labdata(namespace, type, suite=data, lid=id),
                                         facade.LabdataMetadata('to', 'nick'))
        # self.rooms[room_jid].

    def join_room(self, roomDesc):
        super().join_room(roomDesc)
        self.rooms[roomDesc.room_jid].roles = {}

    def leave_room(self, roomDesc):
        pass

    def connect(self, block=True):
        pass

    def disconnect(self):
        super().disconnect()