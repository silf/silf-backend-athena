# -*- coding: utf-8 -*-
import enum
from athena_bot.modules.capabilities import AthenaCapabilities


class MockCapabilityType(enum.IntEnum):
    Enabled = 1
    Disabled = 2
    Mock = 3


class MockCapabilities(AthenaCapabilities):

    def __init__(self, capabilities):
        self.capability_patch = {}
        super().__init__(capabilities)

    def reset_patch(self):
        self.CAPABILITIES_REGISTRY.clear()

    def override_capability(self, capability, Type):
        self.capability_patch[capability] = Type

    def _get_capability(self, capability, type):
        if capability in self.capability_patch:
            return self.capability_patch[capability]
        return super()._get_capability(capability, type)
