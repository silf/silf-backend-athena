# -*- coding: utf-8 -*-

import pathlib
from athena_bot.athena import Athena
from athena_tests.utils.facade import TestFacade

from .capabilities import MockCapabilities

def create_athena(test_file, capabilities, **kwargs):

    initial_kwargs = {
        "FacadeType": TestFacade
    }

    initial_kwargs.update(kwargs)

    DIRNAME = pathlib.Path(test_file).absolute().parent
    return Athena("attenuation", [str(DIRNAME / 'botTestConf.ini')], capabilities=capabilities, **initial_kwargs)
