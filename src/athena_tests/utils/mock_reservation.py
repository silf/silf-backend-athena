# -*- coding: utf-8 -*-

from athena_bot.modules import AthenaCapabilities
from athena_bot.modules.capabilities import CapabilityType
from athena_bot.modules.reservation import BaseEnabledReservationRepository

class MockReservationRepository(BaseEnabledReservationRepository):

    def __init__(self, athena):

        self.__new_reservations = None
        super().__init__(athena)

    def _update_reservation_list(self):
        with self.current_lock:
            if self.__new_reservations is not None:
                self._reservation_list = self.__new_reservations
                self.__new_reservations = None

    def mock_set_reservations(self, reservation_list, update=False):
        with self.current_lock:
            self.__new_reservations = reservation_list
            if update:
                self._update_repository()

    def update_repository(self):
        self._update_repository()



class EveryoneIsAnOperatorRepository(BaseEnabledReservationRepository):

    @property
    def reservation_list(self):
        return []

    def is_reservation_in_progress(self, user_jid):
        return True
