# -*- coding: utf-8 -*-
import unittest
import types
from athena_bot.modules import AthenaCapabilities
from athena_tests.utils import create_athena
from silf.backend.commons.api import ProtocolVersion, Setting
from silf.backend.commons.api.stanza_content._misc import ModeSelection, \
    SettingSuite, ModeSelected, SeriesStartedStanza


class BaseExperiment():

    def test_labdatas_are_stored(self):
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:protocol:version:set",
            type='result',
            data=ProtocolVersion(version="1.0.0")
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:mode:set",
            type='result',
            data=ModeSelected(mode='foo', experimentId='foo')
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:series:start",
            type='result',
            data=SeriesStartedStanza(seriesId='bar', initialSettings=SettingSuite(foo=Setting(value='bar')))
        )

        lmap = self.athena.exp_state.labdata_map
        self.assertEqual(len(lmap), 3)

        self.assertEqual(lmap["silf:protocol:version:set"].suite, ProtocolVersion(version="1.0.0"))
        self.assertEqual(lmap["silf:mode:set"].suite, ModeSelected(mode='foo', experimentId='foo'))

    def test_experiment_stop_without_version_id(self):
        self.athena.exp_state.stop_experiment()
        # No messages are sent, experiment exits
        self.assertEqual(len(self.athena.facade.labdata_messages), 0)

    def test_experiment_stop(self):
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:protocol:version:set",
            type='result',
            data=ProtocolVersion(version="1.0.0")
        )
        self.athena.exp_state.stop_experiment()
        # No messages are sent, experiment exits
        self.assertEqual(len(self.athena.facade.return_labdata_matching_sent_by_athena('silf:experiment:stop')), 1)


class TestDisabledExpStateTests(unittest.TestCase, BaseExperiment):

    def setUp(self):
        self.athena = create_athena(__file__, AthenaCapabilities(set()))
        self.room_jid = "attenuation@muc.ilf.fizyka.pw.edu.pl"
        self.athena.run()

    def tearDown(self):
        self.athena.on_exit()

    def test_labdatas_are_not_sent(self):
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:protocol:version:set",
            type='result',
            data=ProtocolVersion(version="1.0.0")
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:mode:set",
            type='result',
            data=ModeSelected(mode='foo', experimentId='foo')
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:series:start",
            type='result',
            data=SeriesStartedStanza(seriesId='bar', initialSettings=SettingSuite(foo=Setting(value='bar')))
        )

        self.athena.facade.mock_user_online(
            self.room_jid, 'foo@bar.pl', 'foo', 'visitor'
        )

        # No messages are sent:
        self.assertEqual(len(self.athena.facade.labdata_messages), 0)


class TestEnabledExpStateTests(unittest.TestCase, BaseExperiment):

    def setUp(self):
        self.athena = create_athena(__file__, AthenaCapabilities({'expstate'}))

        self.room_jid = "attenuation@muc.ilf.fizyka.pw.edu.pl"
        self.athena.run()

    def tearDown(self):
        self.athena.on_exit()

    def test_labdatas_are_sent(self):
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:protocol:version:set",
            type='result',
            data=ProtocolVersion(version="1.0.0")
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:mode:set",
            type='result',
            data=ModeSelected(mode='foo', experimentId='foo')
        )
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:series:start",
            type='result',
            data=SeriesStartedStanza(seriesId='bar', initialSettings=SettingSuite(foo=Setting(value='bar')))
        )

        self.athena.facade.mock_user_online(
            self.room_jid, 'foo@bar.pl', 'foo', 'visitor'
        )

        lm = self.athena.facade.labdata_messages
        self.assertEqual(len(lm), 3)
        self.assertEqual(lm[0].labdata.namespace, "silf:protocol:version:set")
        self.assertEqual(lm[0].labdata.suite, ProtocolVersion(version="1.0.0"))
        self.assertEqual(lm[1].labdata.namespace, "silf:mode:set")
        self.assertEqual(lm[2].labdata.namespace, "silf:series:start")

