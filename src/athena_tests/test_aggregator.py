# -*- coding: utf-8 -*-

from unittest import TestCase

from athena_bot.aggregator \
    import aggregate_results, _append_result, _replace_result, _skip_result


class PragmasTestCase(TestCase):

    def test_append_list(self):
        old_result = {'value': [[1, 2], [3, 4]], 'pragma': 'append'}
        new_result = {'value': [[5, 6]], 'pragma': 'append'}
        expected_result = {'value': [[1, 2], [3, 4], [5, 6]]}
        self.assertEqual(_append_result(old_result, new_result, None), expected_result)

    def test_append_float(self):
        old_result = {'value': [1.2, 3.4], 'pragma': 'append'}
        new_result = {'value': [5.6], 'pragma': 'append'}
        expected_result = {'value': [1.2, 3.4, 5.6]}
        self.assertEqual(_append_result(old_result, new_result, None), expected_result)

    def test_replace_list(self):
        old_result = {'value': [[1, 2], [3, 4]], 'pragma': 'replace'}
        new_result = {'value': [[5, 6]], 'pragma': 'replace'}
        expected_result = {'value': [[5, 6]]}
        self.assertEqual(_replace_result(old_result, new_result, True), expected_result)

    def test_replace_float(self):
        old_result = {'value': 1.2, 'pragma': 'replace'}
        new_result = {'value': 5.6, 'pragma': 'replace'}
        expected_result = {'value': 5.6}
        self.assertEqual(_replace_result(old_result, new_result, True), expected_result)

    def test_transient(self):
        old_result = {}
        new_result = {'value': [5.6], 'pragma': 'transient'}
        expected_result = None
        self.assertEqual(_skip_result(old_result, new_result, True), expected_result)


class AggregatorTestCase(TestCase):

    def test_append(self):
        old_result = {
            'var1': {'value': [[1, 2], [3, 4]], 'pragma': 'append'},
            'var2': {'value': [1.2, 3.4], 'pragma': 'append'},
        }
        new_result = {
            'var1': {'value': [[5, 6]], 'pragma': 'append'},
            'var2': {'value': [5.6], 'pragma': 'append'},
        }
        expected_result = {
            'var1': {'value': [[1, 2], [3, 4], [5, 6]], 'pragma': 'append'},
            'var2': {'value': [1.2, 3.4, 5.6], 'pragma': 'append'},
        }
        self.assertEqual(aggregate_results(old_result, new_result), expected_result)

    def test_replace(self):
        old_result = {
            'var1': {'value': [[1, 2], [3, 4]], 'pragma': 'replace'},
            'var2': {'value': [1.2, 3.4], 'pragma': 'replace'},
        }
        new_result = {
            'var1': {'value': [[5, 6]], 'pragma': 'replace'},
            'var2': {'value': [5.6], 'pragma': 'replace'},
        }
        expected_result = {
            'var1': {'value': [[5, 6]], 'pragma': 'replace'},
            'var2': {'value': [5.6], 'pragma': 'replace'},
        }
        self.assertEqual(aggregate_results(old_result, new_result), expected_result)

    def test_transient(self):
        old_result = {}
        new_result = {
            'var1': {'value': [[5, 6]], 'pragma': 'transient'},
            'var2': {'value': [5.6], 'pragma': 'transient'},
        }
        expected_result = {}
        self.assertEqual(aggregate_results(old_result, new_result), expected_result)

    def test_all(self):
        old_result = {
            'var1': {'value': [[1, 2], [3, 4]], 'pragma': 'append'},
            'var2': {'value': [1.2, 3.4], 'pragma': 'replace'},
        }
        new_result = {
            'var1': {'value': [[5, 6]], 'pragma': 'append'},
            'var2': {'value': [5.6], 'pragma': 'replace'},
            'var3': {'value': [[5, 6]], 'pragma': 'transient'},
        }
        expected_result = {
            'var1': {'value': [[1, 2], [3, 4], [5, 6]], 'pragma': 'append'},
            'var2': {'value': [5.6], 'pragma': 'replace'},
        }
        self.assertEqual(aggregate_results(old_result, new_result), expected_result)
