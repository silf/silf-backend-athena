# -*- coding: utf-8 -*-
from datetime import datetime
import unittest
import pathlib
from unittest.mock import patch
import pytz
from athena_bot.athena import Athena
from athena_bot.modules.capabilities import AthenaCapabilities
from athena_bot.modules.reservation import SilfReservation, \
    SimpleDisabledReservationRepository
from athena_tests.utils import create_athena
from athena_tests.utils import MockCapabilities
from athena_tests.utils.mock_reservation import MockReservationRepository, \
    EveryoneIsAnOperatorRepository
from silf.backend.client.labdata import LabdataVersionSet
from silf.backend.commons.api.stanza_content._misc import ProtocolVersion



DIRNAME = pathlib.Path(__file__).parent


class TestDisabledReservationBase(unittest.TestCase):

    def tearDown(self):
        self.athena.on_exit()

    def setUp(self):
        self.athena = create_athena(__file__, AthenaCapabilities(set()))
        self.athena.run()

    def test_sanity(self):
        pass

    def test_reservation_module_present(self):
        self.assertIsNotNone(getattr(self.athena, 'reservation_repo', None))
        self.assertIsInstance(self.athena.reservation_repo, SimpleDisabledReservationRepository)

    def test_reservation_module_behaviour(self):
        self.assertTrue(self.athena.reservation_repo.is_reservation_in_progress("foo_nick"))
        self.assertEqual(len(self.athena.reservation_repo.reservation_list), 0)


class TestDisabledReservationBehaviours(unittest.TestCase):

    def tearDown(self):
        self.athena.on_exit()

    def setUp(self):

        self.athena = create_athena(__file__, MockCapabilities(set()))
        self.athena.capabilities.override_capability("reservation", EveryoneIsAnOperatorRepository)
        self.room_jid = "attenuation@muc.ilf.fizyka.pw.edu.pl"
        self.athena.run()
        self.athena.facade.mock_user_online(
            self.room_jid, 'foo@bar.pl', 'foo', 'visitor')
        self.athena.reservation_repo._update_repository()
        from athena_bot.modules import reservation

    def test_sanity(self):
        pass

    def test_user_got_proper_role(self):
        room = self.athena.facade.rooms[self.room_jid]
        self.assertEqual(room.roles['foo'], 'participant')


class ReservationMockHelper(object):

    def setUp(self):
        self.datetime_patcher = patch("athena_bot.modules.reservation.datetime")
        self.datetime_patch = self.datetime_patcher.start()
        self.datetime_patch.utcnow.return_value = "foo"

    def tearDown(self):
        self.datetime_patcher.stop()

    def set_datetime(self, *largs):
        self.datetime_patch.utcnow.return_value = datetime(*largs)

    def __call__(self, *args):
        date_unaware = datetime(*args)
        return date_unaware.replace(tzinfo=pytz.utc)


class TestMockRepository(unittest.TestCase):

    def tearDown(self):
        self.athena.on_exit()
        self.helper.tearDown()

    def setUp(self):
        self.athena = create_athena(__file__, MockCapabilities(set()))
        self.athena.capabilities.override_capability("reservation", MockReservationRepository)
        self.room_jid = "attenuation@muc.ilf.fizyka.pw.edu.pl"
        self.athena.run()
        self.athena.reservation_repo.close()
        self.athena.facade.mock_user_online(
            self.room_jid, 'foo@bar.pl', 'foo', 'visitor')
        self.athena.facade.mock_user_online(
            self.room_jid, 'bar@bar.pl', 'bar', 'visitor')
        self.athena.reservation_repo._update_repository()
        self.helper = ReservationMockHelper()
        self.helper.setUp()

    def test_sanity(self):
        pass

    def assert_has_role(self, user_nick, role):
        self.assertEqual(self.athena.facade.get_user_role(self.room_jid, user_nick), role)

    def test_user_got_proper_role(self):

        self.assert_has_role('foo', 'visitor')
        self.assert_has_role('bar', 'visitor')

    def test_operator_got_role(self):
        self.helper.set_datetime(1985, 9, 19, 12)
        self.athena.reservation_repo.mock_set_reservations(
            [SilfReservation(self.helper(1985, 9, 19), self.helper(1985, 9, 20), 'foo@bar.pl')],
        update=True)
        self.assert_has_role('foo', 'participant')
        self.assert_has_role('bar', 'visitor')

    def test_operator_gets_demoted(self):
        self.helper.set_datetime(1985, 9, 19, 12)
        self.athena.reservation_repo.mock_set_reservations(
            [SilfReservation(self.helper(1985, 9, 19), self.helper(1985, 9, 20), 'foo@bar.pl')],
        update=True)
        self.assert_has_role('foo', 'participant')
        self.assert_has_role('bar', 'visitor')
        self.helper.set_datetime(1985, 9, 20, 12)
        self.athena.reservation_repo.update_repository()
        self.assert_has_role('foo', 'visitor')
        self.assert_has_role('bar', 'visitor')


    def test_op_gets_role_if_joins_in_middle_of_reservation(self):
        self.helper.set_datetime(1985, 9, 19, 12)
        self.athena.reservation_repo.mock_set_reservations(
            [SilfReservation(self.helper(1985, 9, 19), self.helper(1985, 9, 20), 'baz@bar.pl')],
        update=True)
        self.athena.facade.mock_user_online(
            self.room_jid, 'baz@bar.pl', 'baz', 'visitor')
        self.assert_has_role('foo', 'visitor')
        self.assert_has_role('bar', 'visitor')
        self.assert_has_role('baz', 'participant')
        self.helper.set_datetime(1985, 9, 20, 12)
        self.athena.reservation_repo.update_repository()
        self.assert_has_role('foo', 'visitor')
        self.assert_has_role('bar', 'visitor')
        self.assert_has_role('baz', 'visitor')

    def test_participant_gets_demoted(self):
        self.athena.facade.mock_user_online(
            self.room_jid, 'baz@bar.pl', 'baz', 'participant')
        self.assert_has_role('baz', 'visitor')

    @unittest.expectedFailure
    def test_reservation_prolongation(self):
        self.athena.facade.mock_send_labdata(
            self.room_jid,
            "silf:protocol:version:set",
            type='result',
            data=ProtocolVersion(version="1.0.0")
        )
        self.helper.set_datetime(1985, 9, 19, 12)
        self.athena.reservation_repo.mock_set_reservations(
            [SilfReservation(self.helper(1985, 9, 19), self.helper(1985, 9, 20), 'foo@bar.pl')],
            update=True)
        self.assert_has_role('foo', 'participant')
        self.athena.reservation_repo.mock_set_reservations(
            [SilfReservation(self.helper(1985, 9, 19), self.helper(1985, 9, 21), 'foo@bar.pl')],
            update=True)
        self.helper.set_datetime(1985, 9, 20, 12)
        self.athena.reservation_repo.update_repository()
        self.assertEqual(len(self.athena.facade.return_labdata_matching_sent_by_athena("silf:experiment:stop")), 0)
        self.assert_has_role('foo', 'participant')


# FIXME: Test reservations with new WEB API (mocing requests --- jak przy dziewczynach)