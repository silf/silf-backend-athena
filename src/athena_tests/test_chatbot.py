# -*- coding: utf-8 -*-
import pathlib

import unittest
import unipath
from athena_bot.athena import Athena
from athena_bot.chatbot.api import BotIdle
from athena_bot.chatbot.api.chatbot import ChatBot
from athena_bot.chatbot.api.command_parser import ListAllCommandsCommand, \
    ListQuestions
from athena_bot.modules import capabilities
from athena_tests.facade_utils import MockFacade

DIR = unipath.Path(__file__).parent

class MockChatbot(ChatBot):

    QABOT_DATA_PATH = pathlib.Path(DIR.child("qa-cdc.yaml").absolute())
    def get_command_classes(self):
        return []


class TestChatbot(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.athena = Athena("example", DIR.child('botConf.ini'), FacadeType=MockFacade,
                             capabilities=capabilities.AthenaCapabilities({'monitoring', 'publish_results'}))
        self.athena.run()
        self.chatbot = MockChatbot(self.athena)
        self.chatbot.initialize()
        self.chatbot.state = BotIdle

    def test_command_description(self):

        expected = """
**KOMENDA** `\czesio` info
 Wyświetla wszystkie komendy
        """.strip()
        cmd = ListAllCommandsCommand(self.chatbot)

        self.assertEqual(
            cmd.get_command_info().strip(),
            expected
        )

    def test_list_all(self):

        cmd = ListAllCommandsCommand(self.chatbot)
        # try:
        # This fails because mock facade does not override all needed methods
        cmd.execute_command('', '', '', True)
        # except Exception as e:
        #     print("ERROR {}".format(e))
        #     print(e)

    def test_list_all_questions(self):

        cmd = ListQuestions(self.chatbot)
        # try:
        # This fails because mock facade does not override all needed methods
        self.fail(cmd.get_response())
        # except Exception as e:
        #     print("ERROR {}".format(e))
        #     print(e)