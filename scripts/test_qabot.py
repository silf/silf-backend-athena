# -*- coding: utf-8 -*-
import pathlib

import sys
import yaml
import os
from athena_bot.chatbot.api import DATA_PATH, ChatBot
# import qabot
from qabot.bot import QaBot
from qabot.input_data import QaBotData

data = QaBotData(
    pathlib.Path(sys.argv[1]),
    DATA_PATH / 'paths.yaml',
    DATA_PATH / 'stopwords.yaml'
)
print(data.parse())

qabot = QaBot(data)
qabot.initialize()

print(ChatBot.render_question(
        qabot.get_question_by_id('cdc-model')))