#!/bin/sh

scripts/j2cli.py conf/docker.ini > conf/docker-ready.ini

cat conf/docker-ready.ini

while true; do
    if curl $SILF_APP_API_URL --silent > /dev/null; then
        break
    else
        echo "Wating for API to become avilable"
        sleep 1;
    fi
done

set -e

python3 runner.py /athena/conf/docker-ready.ini
