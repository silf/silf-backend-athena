# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
import pathlib

import sys
import yaml
import os
# import qabot
from athena_bot.chatbot.api.chatbot import DATA_PATH
from athena_bot.chatbot.api.jinjaenv import ENVIORMENT
from qabot.bot import QaBot
from qabot.input_data import QaBotData

data = QaBotData(
    pathlib.Path(sys.argv[1]),
    DATA_PATH / 'paths.yaml',
    DATA_PATH / 'stopwords.yaml'
)
data.parse()
# print(data.parse())
# python scripts/print_questions.py athena_bot/chatbot/experiments/attenuation/input-file-example.yaml > /tmp/att.rst && rst2latex /tmp/att.rst > /tmp/att.tex && pdflatex /tmp/att.tex
template = ENVIORMENT.get_template('qabot_questionaire.rst')

print(template.render(
    questions=data.question_map.values()
))